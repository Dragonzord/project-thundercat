from rest_framework import pagination
from rest_framework.response import Response

# function that verifies if the specified parameter is undefined
def is_undefined(value):
    if value is None:
        return True
    if value == "undefined":
        return True
    if value == "null":
        return True
    return False


# custom pagination class
class CustomPagination(pagination.PageNumberPagination):
    page_size_query_param = "page_size"
    max_page_size = 100

    def get_paginated_response(self, data):

        # checking if a next page number exists
        # if not, set it to 0
        next_page_number = 0
        try:
            next_page_number = self.page.next_page_number()
        except:
            pass

        # checking if a previous page number exists
        # if not, set it to 0
        previous_page_number = 0
        try:
            previous_page_number = self.page.previous_page_number()
        except:
            pass

        return Response(
            {
                "next_link": self.get_next_link(),
                "next_page_number": next_page_number,
                "previous_link": self.get_previous_link(),
                "previous_page_number": previous_page_number,
                "count": self.page.paginator.count,
                "current_page_number": self.page.number,
                "results": data,
            }
        )
