# Generated by Django 2.2.3 on 2020-01-13 17:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_management_models', '0008_user_secondary_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='last_password_change',
            field=models.DateField(blank=True, null=True),
        ),
    ]
