from rest_framework import serializers
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.item_text import ItemText
from backend.custom_models.test_scorer_assignment import TestScorerAssignment
from backend.serializers.assigned_test_serializer import AssignedTestSerializer


# Serializers define the API representation
class GetTestScorerAssignmentSerializer(serializers.ModelSerializer):
    assigned_test_id = serializers.SerializerMethodField()
    assigned_test_status = serializers.SerializerMethodField()
    assigned_test_start_date = serializers.SerializerMethodField()
    assigned_test_test_name = serializers.SerializerMethodField()
    assigned_test_en_name = serializers.SerializerMethodField()
    assigned_test_fr_name = serializers.SerializerMethodField()
    assigned_test_submit_date = serializers.SerializerMethodField()
    assigned_test_submit_time = serializers.SerializerMethodField()
    assigned_test_test_session_language = serializers.SerializerMethodField()

    def get_assigned_test(self, test_scorer_assignment):
        return AssignedTest.objects.get(id=test_scorer_assignment.assigned_test_id)

    def get_assigned_test_id(self, test_scorer_assignment):
        return self.get_assigned_test(test_scorer_assignment).id

    def get_assigned_test_status(self, test_scorer_assignment):
        return self.get_assigned_test(test_scorer_assignment).status

    def get_assigned_test_start_date(self, test_scorer_assignment):
        return self.get_assigned_test(test_scorer_assignment).start_date

    def get_assigned_test_test_name(self, test_scorer_assignment):
        return self.get_assigned_test(test_scorer_assignment).test.test_name

    # getting test en_test_name
    def get_assigned_test_en_name(self, test_scorer_assignment):
        test_item_id = self.get_assigned_test(test_scorer_assignment).test.item_id
        en_test_name = ItemText.objects.get(
            item_id=test_item_id, language_id=1
        ).text_detail
        return en_test_name

    # getting test fr_test_name
    def get_assigned_test_fr_name(self, test_scorer_assignment):
        test_item_id = self.get_assigned_test(test_scorer_assignment).test.item_id
        fr_test_name = ItemText.objects.get(
            item_id=test_item_id, language_id=2
        ).text_detail
        return fr_test_name

    def get_assigned_test_submit_date(self, test_scorer_assignment):
        return self.get_assigned_test(test_scorer_assignment).submit_date.date()

    def get_assigned_test_submit_time(self, test_scorer_assignment):
        return self.get_assigned_test(test_scorer_assignment).submit_date.time()

    def get_assigned_test_test_session_language(self, test_scorer_assignment):
        language = self.get_assigned_test(test_scorer_assignment).test_session_language
        if language is None:
            return None
        return language.ISO_Code_1

    class Meta:
        model = TestScorerAssignment
        fields = [
            "id",
            "assigned_test_id",
            "assigned_test_status",
            "assigned_test_start_date",
            "assigned_test_test_name",
            "assigned_test_en_name",
            "assigned_test_fr_name",
            "assigned_test_submit_date",
            "assigned_test_submit_time",
            "assigned_test_test_session_language",
            "scorer_username",
            "status",
            "score_date",
        ]
