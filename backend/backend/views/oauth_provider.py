from django.conf import settings
import requests


# getting oauth token
def get_oauth_token():
    # dev oauth provider
    # TODO: get the right endpoints depending on the current environment
    endpoint = "http://natux40:7025/oauth-provider/oauth/token"
    headers = {
        "Authorization": settings.OAUTH_PROVIDER_CREDENTIALS,
        "Content-Type": "application/x-www-form-urlencoded",
    }
    body = {"grant_type": "client_credentials"}

    # POST call that is getting the oauth token
    response = requests.post(endpoint, headers=headers, data=body)

    # converting response to json format
    data = response.json()

    # returning oauth access token
    return data["access_token"]
