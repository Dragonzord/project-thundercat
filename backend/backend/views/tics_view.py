from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.tics_data import get_data


class GetTicsData(APIView):
    def get(self, request):
        return get_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
