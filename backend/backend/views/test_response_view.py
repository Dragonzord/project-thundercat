from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from backend.views.retrieve_test_response_view import (
    set_assigned_questions,
    add_test_response,
    edit_test_response,
    delete_test_response,
    get_test_responses,
)


class SetAssignedQuestions(APIView):
    def get(self, request):
        return Response(set_assigned_questions(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class AddTestResponse(APIView):
    def get(self, request):
        return Response(add_test_response(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class EditTestResponse(APIView):
    def get(self, request):
        return Response(edit_test_response(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class DeleteTestResponse(APIView):
    def get(self, request):
        return Response(delete_test_response(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetTestResponses(APIView):
    def get(self, request):
        return Response(get_test_responses(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
