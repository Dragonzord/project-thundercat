from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from backend.views.retrieve_assigned_tests_view import (
    retrieve_assigned_tests,
    assign_test,
    assigned_tests_historical,
)


class AssignedTestsSet(APIView):
    def get(self, request):
        return retrieve_assigned_tests(request.query_params.get("username", None))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class AssignTest(APIView):
    def get(self, request):
        return Response(assign_test(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class HistoricalAssignedTests(APIView):
    def get(self, request):
        return assigned_tests_historical(
            request.query_params.get("username", None),
            request.query_params.get("test_id", None),
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
