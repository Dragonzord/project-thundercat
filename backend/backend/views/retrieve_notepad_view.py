from backend.custom_models.notepad import Notepad
from rest_framework import status
from rest_framework.response import Response
from backend.views.utils import is_undefined


def save_notepad(request):
    assigned_test_id = request.query_params.get("assigned_test_id", None)
    notepad_content = request.query_params.get("notepad", None)
    # verify that all parameters were properly passed in
    if is_undefined(notepad_content):
        return Response(
            {"error": "no 'notepad_content' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(assigned_test_id):
        return Response(
            {"error": "no 'assigned_test_id' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    # see if it exists
    try:
        notepad = Notepad.objects.get(assigned_test=assigned_test_id)
    except Notepad.DoesNotExist:
        # otherwise make a new one
        notepad = Notepad(assigned_test_id=assigned_test_id)
    # update the notepad content and save
    notepad.notepad = notepad_content
    notepad.save()
    return Response(status=status.HTTP_200_OK)


def delete_notepad(assigned_test_id):
    # see if it exists
    try:
        notepad = Notepad.objects.get(assigned_test=assigned_test_id)
    except Notepad.DoesNotExist:
        # if not, do nothing
        return
    # otherwise delete it
    notepad.delete()
    return


def get_notepad(request):
    assigned_test_id = request.query_params.get("assigned_test_id", None)
    # verify that all parameters were properly passed in
    if is_undefined(assigned_test_id):
        return Response(
            {"error": "no 'assigned_test_id' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    # see if it exists
    try:
        notepad = Notepad.objects.get(assigned_test=assigned_test_id).notepad
    except Notepad.DoesNotExist:
        # otherwise return an empty one
        notepad = ""
    return Response({"notepad": notepad}, status=status.HTTP_200_OK)

