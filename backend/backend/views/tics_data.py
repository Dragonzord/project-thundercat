from rest_framework.response import Response
from rest_framework import status
import requests
from backend.views.utils import is_undefined
from backend.views.oauth_provider import get_oauth_token


# getting tics data
def get_data(request):
    order_number = request.query_params.get("order_number", None)
    # verify that all parameters were properly passed in
    if is_undefined(order_number):
        return Response(
            {"error": "no 'order_number' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )

    # TODO: get the right endpoints depending on the current environment
    # (TEMP) use your laptop IP address to connect locally (note that ordering service needs to be running locally)
    endpoint = "http://<your_laptop_ip>:8042/orders/"

    token = get_oauth_token()
    headers = {"Authorization": "bearer " + token}
    params = {"order_number": order_number}

    # GET call that is getting the needed tics data based on the provided order number
    response = requests.get(endpoint, headers=headers, params=params)

    # converting response to json format
    data = response.json()

    # there is data related to provided order number
    if data["responseCode"] == 200:
        # returning data
        return Response(data)
    # there is no data related to provided order number
    elif data["responseCode"] == 204:
        return Response({"info": "no results found"}, status=status.HTTP_204_NO_CONTENT)
    # should never happen
    else:
        return Response(
            {"error": "something happened during getting tics data process"},
            status=status.HTTP_400_BAD_REQUEST,
        )

