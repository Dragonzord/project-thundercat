from rest_framework.response import Response
from rest_framework import status
from backend.custom_models.test_access_code_model import TestAccessCode
from backend.custom_models.assigned_test import AssignedTest
from user_management.user_management_models.user_models import User
from backend.static.assigned_test_status import AssignedTestStatus
from cms.cms_models.test import Test


def check_into_room(test_access_code, username):
    # find DAOs
    room = TestAccessCode.objects.filter(test_access_code=test_access_code).first()
    test_admin = User.objects.get(username=room.ta_username)
    user = User.objects.get(username=username)

    # don't allow a TA to take their own exam
    if user.username == test_admin.username:
        return Response(
            {"error": "You cannot be a candidate in an exam you are administering."},
            status=status.HTTP_400_BAD_REQUEST,
        )

    test_name = room.test.test_name
    ## TODO remove the follwoing line after pilot
    test_name = get_alternating_test_name(test_name)

    test = Test.objects.get(test_name=test_name)

    # check if this rooms test has already been assigned
    if not has_test_for_test_access_code_already_been_assigned(
        test_access_code, username
    ):
        assigned_test = AssignedTest(
            status=AssignedTestStatus.ASSIGNED,
            test=test,
            username=user,
            ta=test_admin,
            test_access_code=test_access_code,
            test_session_language=room.test_session_language,
        )
        assigned_test.save()

    return Response(status=status.HTTP_200_OK)


def has_test_for_test_access_code_already_been_assigned(test_access_code, username):
    return (
        AssignedTest.objects.filter(
            username__username=username, test_access_code=test_access_code
        ).count()
        > 0
    )


# TODO: Remove this code after pilot. This is a hack to alternate tests to assign
# if the test is emib
def get_alternating_test_name(test_name):
    test_a = "emibTestA"
    test_b = "emibTestB"
    if test_name in [test_a, test_b]:
        if (
            AssignedTest.objects.filter(test_id__test_name=test_a).count()
            == AssignedTest.objects.filter(test_id__test_name=test_b).count()
        ):
            return test_a
        else:
            return test_b
    # if not one of the above, just return the original test_name
    return test_name
    ## End of block to remove
