from datetime import datetime
from rest_framework.response import Response
from math import ceil
from backend.custom_models.assigned_test import AssignedTest
from backend.static.assigned_test_status import AssignedTestStatus
from backend.views.retrieve_notepad_view import delete_notepad
from backend.custom_models.test_scorer_assignment import TestScorerAssignment


def update_test_status(test_id, new_status):
    test = AssignedTest.objects.get(id=test_id)
    test.status = new_status
    return save_and_return(test)


def activate_test(test_id, start_date, new_status):
    test = AssignedTest.objects.get(id=test_id)

    if test.status != AssignedTestStatus.ACTIVE:
        # /1000 because js timestamps are in milliseconds and python in seconds
        test.start_date = datetime.fromtimestamp(ceil(float(start_date) / 1000))

    test.status = new_status

    return save_and_return(test)


# create a new instance of assigned_test_status for this test, if it was submitted or timedout
def prepare_test_for_scoring(test_id, new_status):
    if new_status in (AssignedTestStatus.SUBMITTED, AssignedTestStatus.TIMED_OUT):
        check = TestScorerAssignment.objects.filter(assigned_test_id=test_id)
        if not check:
            assignment = TestScorerAssignment(assigned_test_id=test_id)
            assignment.save()


def submit_test(test_id, new_status):
    test = AssignedTest.objects.get(id=test_id)

    if new_status in (
        AssignedTestStatus.SUBMITTED,
        AssignedTestStatus.TIMED_OUT,
        AssignedTestStatus.QUIT,
    ):
        test.submit_date = datetime.now()

    test.status = new_status

    # clean out the notepad
    delete_notepad(test_id)

    prepare_test_for_scoring(test_id, new_status)

    return save_and_return(test)


def save_and_return(test):
    test.save()
    return Response()
