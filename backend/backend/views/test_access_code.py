import random
import string
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from backend.views.utils import is_undefined
from backend.custom_models.test_access_code_model import TestAccessCode
from user_management.user_management_models.user_models import User
from backend.custom_models.language import Language
from cms.cms_models.test import Test


# get a new randomly generated room number
def generate_random_test_access_code():
    allowed_chars = string.ascii_lowercase + string.ascii_uppercase + string.digits
    random_test_access_code = "".join(random.choice(allowed_chars) for i in range(10))
    return random_test_access_code


def access_code_already_exists(random_code):
    return TestAccessCode.objects.filter(test_access_code=random_code).count() > 0


# populate the room number table with the needed parameters, which contains the generated room number
def populate_test_access_code_table(request):
    # making sure that we have the needed parameters
    ta_username = request.query_params.get("ta_username_id", None)
    staffing_process_number = request.query_params.get("staffing_process_number", None)
    test_session_language = request.query_params.get("test_session_language", None)
    test = request.query_params.get("test_id", None)
    if is_undefined(ta_username):
        return {"error": "no 'ta_username' parameter"}
    if is_undefined(staffing_process_number):
        return {"error": "no 'staffing_process_number' parameter"}
    if is_undefined(test_session_language):
        return {"error": "no 'test_session_language' parameter"}
    if is_undefined(test):
        return {"error": "no 'test' parameter"}
    # call the function that generates the random room number
    random_test_access_code = generate_random_test_access_code()
    while access_code_already_exists(random_test_access_code):
        random_test_access_code = generate_random_test_access_code()
    # saving the response with the needed parameters
    response = TestAccessCode.objects.create(
        test_access_code=random_test_access_code,
        ta_username=User.objects.get(username=ta_username),
        staffing_process_number=staffing_process_number,
        test_session_language=Language.objects.get(language_id=test_session_language),
        test=Test.objects.get(test_name=test),
    )
    response.save()
    # return the rom number
    return random_test_access_code


class GetNewTestAccessCode(APIView):
    def get(self, request):
        return Response(populate_test_access_code_table(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def delete_test_access_code_row(request):
    # making sure that we have the needed parameters
    test_access_code = request.query_params.get("test_access_code", None)
    ta_username = request.query_params.get("ta_username_id", None)
    if is_undefined(test_access_code):
        return {"error": "no 'test_access_code' parameter"}
    if is_undefined(ta_username):
        return {"error": "no 'ta_username' parameter"}
    # get the room number id
    try:
        test_access_code_id = TestAccessCode.objects.get(
            test_access_code=test_access_code
        ).id
    except TestAccessCode.DoesNotExist:
        return {"error": "no room number found"}
    # delete the whole row based on parameters above
    TestAccessCode.objects.get(
        id=test_access_code_id,
        test_access_code=test_access_code,
        ta_username_id=ta_username,
    ).delete()
    return {"succeed"}


class DeleteTestAccessCode(APIView):
    def get(self, request):
        return Response(delete_test_access_code_row(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class getActiveTestAccessCodeData(APIView):
    def get(self, request):
        # making sure that we have the needed parameters
        ta_username = request.query_params.get("ta_username_id", None)
        if is_undefined(ta_username):
            return Response({"error": "no 'ta_username' parameter"})
        try:
            ta_username = TestAccessCode.objects.get(
                ta_username_id=ta_username
            ).ta_username_id
        except TestAccessCode.DoesNotExist:
            return Response(
                {
                    "error": "no room number found associated to the specified TA username"
                }
            )
        return Response(
            TestAccessCode.objects.filter(
                ta_username_id=User.objects.get(username=ta_username)
            ).values()
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
