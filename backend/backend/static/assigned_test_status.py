
# This file is a collection of the valid statuses for assigned tests.
# Tests can be any of the following.


class AssignedTestStatus:
    SUBMITTED = 1  # "Submitted"
    INACTIVITY = 2  # "Expired by Inactivity"
    QUIT = 3  # , "User Quit"
    ASSIGNED = 4  # "Assigned to a Candidate"
    ACTIVE = 5  # "Active"
    NEVER_STARTED = 6  # "Never Started"
    TIMED_OUT = 7  # User timed out, still counts as submitted
