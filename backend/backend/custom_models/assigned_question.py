from django.db import models
from cms.cms_models.question import Question
from backend.custom_models.assigned_test import AssignedTest

# Assigned Question (data for the assigned questions related to an assigned test)
# Stores the id, id of the assigned test,
# and the id of the question


class AssignedQuestion(models.Model):
    id = models.AutoField(primary_key=True)
    assigned_test = models.ForeignKey(AssignedTest, on_delete=models.DO_NOTHING)
    question = models.ForeignKey(Question, on_delete=models.DO_NOTHING)
