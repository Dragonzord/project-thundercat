# Generated by Django 2.2.3 on 2020-02-11 13:17

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("custom_models", "0053_notepad"),
    ]

    operations = [
        migrations.CreateModel(
            name="TestScorerAssignment",
            fields=[
                ("id", models.AutoField(primary_key=True, serialize=False)),
                ("status", models.IntegerField(default=0)),
                ("score_date", models.DateTimeField(blank=True, null=True)),
                (
                    "assigned_test",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="custom_models.AssignedTest",
                    ),
                ),
                (
                    "scorer_username",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to=settings.AUTH_USER_MODEL,
                        to_field="username",
                    ),
                ),
            ],
        )
    ]
