# Generated by Django 2.1.7 on 2019-05-30 14:07
# Edited by Michael Cherry to fix a typos in the sample test

from django.db import migrations

EMAIL_3_BODY_ORIGINAL_TEXT_EN = "Hello T.C.,\n\nI am working with Clara Farewell from the Research and Innovation unit to evaluate the quality of a training approach, and I am having a hard time getting a hold of her. I am starting to be concerned because I am waiting for her part of the project to complete the evaluation report.\n\nFor the past three weeks, we had scheduled working meetings on Friday afternoons. Although she did cancel the first one, she was absent for the past two without notice. She did not answer my attempts to contact her by phone or email. I am worried that I will not be able to complete the report by the end of next Friday without her content.\n\nOn another note, I was told by one of my colleagues from the Program Development unit that his director, Bartosz Greco, would invite employees from other units to help them develop a new training program. They want to take a multiple perspectives approach. I’m very much interested in participating in this process. As usual, manager permission is required for participation. I am wondering what you think.\n\nThank you.\nCharlie"
EMAIL_3_BODY_NEW_TEXT_EN = "Hello T.C.,\n\nI am working with Clara Farewell from the Research and Innovation unit to evaluate the quality of a training approach, and I am having a hard time getting a hold of her. I am starting to be concerned because I am waiting for her part of the project to complete the evaluation report.\n\nFor the past three weeks, we had scheduled working meetings on Friday afternoons. Although she did cancel the first one, she was absent for the past two without notice. She did not answer my attempts to contact her by phone or email. I am worried that I will not be able to complete the report by the end of next Friday without her content.\n\nOn another note, I was told by one of my colleagues from the Program Development unit that his director, Bartosz Greco, would invite employees from other units to help them develop a new training program. They want to take a multiple perspectives approach. I’m very much interested in participating in this process. As usual, manager permission is required for participation. I am wondering what you think.\n\nThank you.\n\nCharlie"
EMAIL_3_BODY_ORIGINAL_TEXT_FR = "Bonjour T.C.,\n\nJe travaille avec Clara Farewell de l’Unité de recherche et innovation sur l’évaluation de la qualité d’une approche de formation, et j’ai de la difficulté à la joindre. Je commence à m’inquiéter, parce que j’attends qu’elle termine sa partie du travail pour achever le rapport d’évaluation.\n\nAu cours des trois dernières semaines, nous avions prévu des rencontres de travail les vendredis après-midi et, après avoir annulé la première rencontre, elle était absente aux deux dernières, sans donner de préavis. Elle n’a pas non plus répondu à mes tentatives de communiquer avec elle par téléphone ou par courriel. Je m’inquiète de ne pas pouvoir terminer le rapport d’ici vendredi prochain si elle ne remet pas sa part du travail.\n\nDans un autre ordre d’idées, un de mes collègues de l’Unité de l’élaboration des programmes m’a dit que son directeur, Bartosz Greco, inviterait des employés d’autres unités à les aider à créer un nouveau programme de formation. Ils veulent adopter une approche qui inclut des perspectives multiples. J’aimerais bien participer à ce processus. Comme d’habitude, la permission du gestionnaire est requise pour y participer. Je me demande ce que tu en penses.\n\nMerci,\nCharlie"
EMAIL_3_BODY_NEW_TEXT_FR = "Bonjour T.C.,\n\nJe travaille avec Clara Farewell de l’Unité de recherche et innovation sur l’évaluation de la qualité d’une approche de formation, et j’ai de la difficulté à la joindre. Je commence à m’inquiéter, parce que j’attends qu’elle termine sa partie du travail pour achever le rapport d’évaluation.\n\nAu cours des trois dernières semaines, nous avions prévu des rencontres de travail les vendredis après-midi et, après avoir annulé la première rencontre, elle était absente aux deux dernières, sans donner de préavis. Elle n’a pas non plus répondu à mes tentatives de communiquer avec elle par téléphone ou par courriel. Je m’inquiète de ne pas pouvoir terminer le rapport d’ici vendredi prochain si elle ne remet pas sa part du travail.\n\nDans un autre ordre d’idées, un de mes collègues de l’Unité de l’élaboration des programmes m’a dit que son directeur, Bartosz Greco, inviterait des employés d’autres unités à les aider à créer un nouveau programme de formation. Ils veulent adopter une approche qui inclut des perspectives multiples. J’aimerais bien participer à ce processus. Comme d’habitude, la permission du gestionnaire est requise pour y participer. Je me demande ce que tu en penses.\n\nMerci,\n\nCharlie"


def update_missing_french_wording(apps, schema_editor):
    # get models
    language = apps.get_model("custom_models", "Language")
    item_text = apps.get_model("custom_models", "ItemText")
    # get db alias
    db_alias = schema_editor.connection.alias
    # get language
    l_english = (
        language.objects.using(db_alias)
        .filter(ISO_Code_1="en", ISO_Code_2="en-ca")
        .last()
    )

    l_french = (
        language.objects.using(db_alias)
        .filter(ISO_Code_1="fr", ISO_Code_2="fr-ca")
        .last()
    )

    # get the french "Your Organization" side navigation title
    email_3_body_en_item_text = (
        item_text.objects.using(db_alias)
        .filter(text_detail=EMAIL_3_BODY_ORIGINAL_TEXT_EN, language=l_english)
        .last()
    )

    email_3_body_fr_item_text = (
        item_text.objects.using(db_alias)
        .filter(text_detail=EMAIL_3_BODY_ORIGINAL_TEXT_FR, language=l_french)
        .last()
    )

    # replace the text and save
    email_3_body_en_item_text.text_detail = EMAIL_3_BODY_NEW_TEXT_EN
    email_3_body_en_item_text.save()
    email_3_body_fr_item_text.text_detail = EMAIL_3_BODY_NEW_TEXT_FR
    email_3_body_fr_item_text.save()


def rollback_translation_updates(apps, schema_editor):
    # get models
    language = apps.get_model("custom_models", "Language")
    item_text = apps.get_model("custom_models", "ItemText")
    # get db alias
    db_alias = schema_editor.connection.alias
    # get language
    l_english = (
        language.objects.using(db_alias)
        .filter(ISO_Code_1="en", ISO_Code_2="en-ca")
        .last()
    )

    l_french = (
        language.objects.using(db_alias)
        .filter(ISO_Code_1="fr", ISO_Code_2="fr-ca")
        .last()
    )

    # get the french "Your Organization" side navigation title
    email_3_body_en_item_text = (
        item_text.objects.using(db_alias)
        .filter(text_detail=EMAIL_3_BODY_NEW_TEXT_EN, language=l_english)
        .last()
    )

    email_3_body_fr_item_text = (
        item_text.objects.using(db_alias)
        .filter(text_detail=EMAIL_3_BODY_NEW_TEXT_FR, language=l_french)
        .last()
    )

    # replace the text and save
    email_3_body_en_item_text.text_detail = EMAIL_3_BODY_ORIGINAL_TEXT_EN
    email_3_body_en_item_text.save()
    email_3_body_fr_item_text.text_detail = EMAIL_3_BODY_ORIGINAL_TEXT_FR
    email_3_body_fr_item_text.save()


class Migration(migrations.Migration):

    dependencies = [("custom_models", "0041_updating_qa_org_chart_location")]

    operations = [
        migrations.RunPython(
            update_missing_french_wording, rollback_translation_updates
        )
    ]
