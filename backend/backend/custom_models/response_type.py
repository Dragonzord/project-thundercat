from django.db import models

MAX_CHAR_LEN = 100

# Response Type model, containing the various response types (email, task, etc)
# Used to help the application know how to store a canidate's response


class ResponseType(models.Model):
    id = models.AutoField(primary_key=True)
    response_desc = models.CharField(max_length=MAX_CHAR_LEN)
    date_created = models.DateTimeField(auto_now_add=True, blank=True)
    date_from = models.DateTimeField(auto_now_add=True, blank=True)
    date_to = models.DateTimeField(null=True, blank=True)
