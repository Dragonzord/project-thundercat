from django.db import models
from .assigned_test import AssignedTest

# Notepad
# Stores the text from the notepad
# associated to an assigned test via the id


class Notepad(models.Model):
    assigned_test = models.OneToOneField(
        AssignedTest, on_delete=models.DO_NOTHING, primary_key=True
    )
    notepad = models.CharField(max_length=20000, null=True, blank=True)

