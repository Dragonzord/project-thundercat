from django.db import models
from .candidate_reponse import CandidateResponse

MAX_CHAR_LEN = 1000


class TaskResponse(models.Model):
    id = models.AutoField(primary_key=True)
    candidate_response = models.ForeignKey(
        CandidateResponse, to_field="id", on_delete=models.DO_NOTHING
    )
    task = models.CharField(max_length=MAX_CHAR_LEN)
    reason = models.CharField(max_length=MAX_CHAR_LEN)
