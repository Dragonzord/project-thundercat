from django.db import models
from cms.cms_models.test import Test
from user_management.user_management_models.user_models import User
from backend.custom_models.language import Language

# Assigned Test Model (data for a test assigned to a candidate)
# Stores the id, user it is assigned to, start of test date,
# scheduled test date, last modification date (last question saved),
# submit date, date the test was ordered, and a test id foreign key


class AssignedTest(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.ForeignKey(User, to_field="username", on_delete=models.DO_NOTHING)
    status = models.IntegerField(blank=True, null=False)
    scheduled_date = models.DateField(blank=True, null=True)
    start_date = models.DateTimeField(blank=True, null=True)
    modify_date = models.DateTimeField(auto_now=True)
    submit_date = models.DateTimeField(blank=True, null=True)
    test = models.ForeignKey(Test, to_field="test_name", on_delete=models.DO_NOTHING)
    test_access_code = models.CharField(max_length=10, null=True, blank=True)
    test_session_language = models.ForeignKey(
        Language,
        to_field="language_id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
    )
    ta = models.ForeignKey(
        User,
        to_field="username",
        related_name="ta_username",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
    )
