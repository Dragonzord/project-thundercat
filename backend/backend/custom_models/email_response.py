from django.db import models
from .candidate_reponse import CandidateResponse

MAX_CHAR_LEN = 4000


class EmailResponse(models.Model):
    id = models.AutoField(primary_key=True)
    candidate_response = models.ForeignKey(
        CandidateResponse, to_field="id", on_delete=models.DO_NOTHING
    )
    to = models.CharField(max_length=200)
    cc = models.CharField(max_length=200)
    response = models.CharField(max_length=4000)
    reason = models.CharField(max_length=1000)
