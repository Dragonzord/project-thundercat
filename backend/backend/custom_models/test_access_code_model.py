from django.db import models
from cms.cms_models.test import Test
from user_management.user_management_models.user_models import User
from backend.custom_models.language import Language

# Room Number Model, containing the randomly generated room numbers
# Used to make sure that only authorized candidate get their test assigned through this room number


class TestAccessCode(models.Model):
    id = models.AutoField(primary_key=True)
    test_access_code = models.CharField(max_length=10, unique=True)
    staffing_process_number = models.CharField(max_length=50)
    test_session_language = models.ForeignKey(
        Language, to_field="language_id", on_delete=models.DO_NOTHING, null=False
    )
    test = models.ForeignKey(
        Test, to_field="test_name", on_delete=models.DO_NOTHING, null=True
    )
    ta_username = models.ForeignKey(
        User, to_field="username", on_delete=models.DO_NOTHING
    )
