from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from backend.custom_models.item_text import ItemText
from cms.serializers.get_test_information_serializer import (
    GetNonPublicTestsSerializer,
    GetTestNameSerializer,
    GetTestInternalNameSerializer,
)
from cms.cms_models.test import Test
from cms.views.utils import is_undefined


# getting non public tests (where is_public is set to false)
class GetNonPublicTests(APIView):
    def get(self, request):
        non_public_test_name = Test.objects.all()
        serialized = GetNonPublicTestsSerializer(non_public_test_name, many=True)
        return Response(serialized.data)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# getting test name (English and French) based on the given test internal name
class GetTestName(APIView):
    def get(self, request):
        return Response(get_test_name(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_test_name(request):
    # making sure that we have the needed parameters
    test_internal_name = request.query_params.get("test_name", None)
    if is_undefined(test_internal_name):
        return {"error": "no 'test_name' parameter"}
    try:
        item_id = Test.objects.get(test_name=test_internal_name).item_id
        test_name = ItemText.objects.filter(item_id=item_id)
        serialized = GetTestNameSerializer(test_name, many=True)
        return serialized.data
    except ObjectDoesNotExist:
        return {"The specified test name does not exist"}


# getting test internal name based on the given test name (English or French name)
class GetTestInternalName(APIView):
    def get(self, request):
        return Response(get_test_internal_name(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_test_internal_name(request):
    # making sure that we have the needed parameters
    test_name = request.query_params.get("text_detail", None)
    if is_undefined(test_name):
        return {"error": "no 'text_detail' parameter"}
    try:
        item_id = ItemText.objects.get(text_detail=test_name).item_id
        test_internal_name = Test.objects.get(item_id=item_id)
        serialized = GetTestInternalNameSerializer(test_internal_name, many=False)
        return serialized.data
    except ObjectDoesNotExist:
        return {"The specified test name does not exist"}
