from operator import or_, and_
from functools import reduce
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from backend.custom_models.item_text import ItemText
from user_management.user_management_models.user_models import User
from user_management.views.utils import CustomPagination
from cms.cms_models.test_permissions_model import TestPermissions
from cms.cms_models.test import Test
from cms.views.utils import is_undefined
from cms.serializers.get_test_information_serializer import GetTestPermissionsSerializer

# getting test permissions data for a specified user
def get_test_permissions(request):
    # making sure that we have the needed parameters
    username_id = request.query_params.get("username_id", None)
    if is_undefined(username_id):
        return Response(
            {"error": "no 'username_id' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    try:
        test_permissions = TestPermissions.objects.filter(
            Q(username=User.objects.get(username=username_id))
        )
        serializer = GetTestPermissionsSerializer(test_permissions, many=True)
        return Response(serializer.data)
    except User.DoesNotExist:
        return Response(
            {"error": "the specified username does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


# getting all active test permissions
def get_all_active_test_permissions(request):
    page = request.query_params.get("page", None)
    page_size = request.query_params.get("page_size", None)
    # making sure that we have the needed parameters
    if is_undefined(page):
        return Response(
            {"error": "no 'page' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(page_size):
        return Response(
            {"error": "no 'page_size' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # get all active test permissions
    active_test_permissions = TestPermissions.objects.all()
    # serializer = GetTestPermissionsSerializer(active_test_permissions, many=True)

    # ordering active test permissions by last_name
    # getting serialized active test permissions data
    serialized_active_test_permissions_data = GetTestPermissionsSerializer(
        active_test_permissions, many=True
    ).data
    # getting sorted serialized data by last_name (ascending)
    ordered_active_test_permissions = sorted(
        serialized_active_test_permissions_data,
        key=lambda k: k["last_name"],
        reverse=False,
    )
    # initializing user permissions ids array (the user_permission_id order here represents the order)
    test_permission_ids_array = []
    # looping in ordered active test permissions
    for i in ordered_active_test_permissions:
        # inserting user permissions ids (ordered respectively by last name) in an array
        test_permission_ids_array.insert(len(test_permission_ids_array), i["id"])

    # sorting active test permissions queryset based on ordered (by last name) user permission ids
    new_active_test_permissions = list(
        TestPermissions.objects.filter(id__in=test_permission_ids_array)
    )
    new_active_test_permissions.sort(
        key=lambda t: test_permission_ids_array.index(t.id)
    )

    # getting page results based on queryset (new_active_test_permissions)
    page = paginator.paginate_queryset(new_active_test_permissions, request)
    # serializing the data
    serialized = GetTestPermissionsSerializer(page, many=True)
    # retuning final data using pagination library
    return paginator.get_paginated_response(serialized.data)


def get_found_active_test_permissions(request):
    keyword = request.query_params.get("keyword", None)
    current_language = request.query_params.get("current_language", None)
    page = request.query_params.get("page", None)
    page_size = request.query_params.get("page_size", None)
    # making sure that we have the needed parameters
    if is_undefined(keyword):
        return Response(
            {"error": "no 'keyword' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(current_language):
        return Response(
            {"error": "no 'current_language' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(page):
        return Response(
            {"error": "no 'page' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(page_size):
        return Response(
            {"error": "no 'page_size' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # ==================== TEST NAME SEARCH (BEGIN) ====================
    # getting all tests data (need item_ids of each test)
    tests = Test.objects.all()

    # search while interface is in English
    if current_language == "en":
        # getting all text_detail items that contains the keyword string (compared with English text_detail items only)
        # and at least one of the item_ids from tests object
        text_detail_items = ItemText.objects.filter(
            reduce(
                or_,
                [
                    Q(
                        text_detail__icontains=keyword,
                        language_id=1,
                        item_id=item.item_id,
                    )
                    for item in tests
                ],
            )
        )
    # search while interface is in French
    else:
        # getting all text_detail items that contains the keyword string (compared with French text_detail items only)
        # and at least one of the item_ids from tests object
        text_detail_items = ItemText.objects.filter(
            reduce(
                or_,
                [
                    Q(
                        text_detail__icontains=keyword,
                        language_id=2,
                        item_id=item.item_id,
                    )
                    for item in tests
                ],
            )
        )

    # there is at least one matching text_detail_item
    if text_detail_items:
        # getting all test_names based on text_detail_items object (using item_ids to get the needed data)
        test_names = Test.objects.filter(
            reduce(or_, [Q(item_id=item.item_id) for item in text_detail_items])
        )
        # getting all test_permission_ids based on test_names object (using test_id/test_name to get the needed data)
        test_permission_ids_based_on_found_tests = TestPermissions.objects.filter(
            reduce(or_, [Q(test_id=test.test_name) for test in test_names])
        )
    # there are no matching text_detail_items
    else:
        test_permission_ids_based_on_found_tests = []
    # ==================== TEST NAME SEARCH (END) ====================

    # ==================== USER NAMES SEARCH (BEGIN) ====================
    # since there is a comma between last name and first name in active test permissions table, if the user put
    # a comma in one of the search keyword words, remove it
    keyword_without_comma = keyword.replace(",", "")
    # splitting keyword string
    split_keyword = keyword_without_comma.split()

    # if keyword contains more than one word
    if len(split_keyword) > 1:
        # getting all usernames based on matching first and last names
        usernames = User.objects.filter(
            reduce(
                and_,
                [
                    Q(first_name__icontains=splitted_keyword)
                    | Q(last_name__icontains=splitted_keyword)
                    for splitted_keyword in split_keyword
                ],
            )
        )
    # keyword is empty or contains only one word
    else:
        # getting all usernames based on matching first name and/or last name
        usernames = User.objects.filter(
            Q(first_name__icontains=keyword) | Q(last_name__icontains=keyword)
        )

    # there is at least one matching name
    if usernames:
        test_permission_ids_based_on_found_names = TestPermissions.objects.filter(
            reduce(or_, [Q(username_id=user.username) for user in usernames])
        )
        # if queryset is empty (meaning that the username(s) exists, but doesn't have any test permissions)
        if not bool(test_permission_ids_based_on_found_names):
            test_permission_ids_based_on_found_names = []
    # there are no matching names
    else:
        test_permission_ids_based_on_found_names = []
    # ==================== USER NAMES SEARCH (END) ====================

    # ==================== TEST ORDER NUMBER SEARCH (BEGIN) ====================
    # getting all test order numbers that contains keyword string
    order_numbers = TestPermissions.objects.filter(test_order_number__icontains=keyword)
    # if there is at least one matching order number
    if order_numbers:
        test_permission_ids_based_on_found_order_nbr = order_numbers
    # there are no matching order numbers
    else:
        test_permission_ids_based_on_found_order_nbr = []
    # ==================== TEST ORDER NUMBER SEARCH (END) ====================

    # if at least one result has been found based on the keyword provided
    if (
        test_permission_ids_based_on_found_tests != []
        or test_permission_ids_based_on_found_names != []
        or test_permission_ids_based_on_found_order_nbr != []
    ):

        # results have been found based on test names
        if test_permission_ids_based_on_found_tests != []:
            found_active_test_permissions_tests = TestPermissions.objects.filter(
                reduce(
                    or_,
                    [
                        Q(id=test.id)
                        for test in test_permission_ids_based_on_found_tests
                    ],
                )
            )
        # no results have been found based on test names
        else:
            found_active_test_permissions_tests = TestPermissions.objects.none()

        # results have been found based on user names (first and/or last name)
        if test_permission_ids_based_on_found_names != []:
            found_active_test_permissions_names = TestPermissions.objects.filter(
                reduce(
                    or_,
                    [
                        Q(id=name.id)
                        for name in test_permission_ids_based_on_found_names
                    ],
                )
            )
        # no results have been found based on user names (first and/or last name)
        else:
            found_active_test_permissions_names = TestPermissions.objects.none()

        # results have been found based on test order number
        if test_permission_ids_based_on_found_order_nbr != []:
            found_active_test_permissions_order_nbrs = TestPermissions.objects.filter(
                reduce(
                    or_,
                    [
                        Q(id=order_number.id)
                        for order_number in test_permission_ids_based_on_found_order_nbr
                    ],
                )
            )
        # no results have been found based on test order number
        else:
            found_active_test_permissions_order_nbrs = TestPermissions.objects.none()

        # merging all found results together
        found_active_test_permissions = found_active_test_permissions_names.union(
            found_active_test_permissions_tests,
            found_active_test_permissions_order_nbrs,
        )

    else:
        return Response({"no results found"}, status=status.HTTP_200_OK)

    # ordering found active test permissions by last_name
    # getting serialized active test permissions data
    serialized_found_active_test_permissions_data = GetTestPermissionsSerializer(
        found_active_test_permissions, many=True
    ).data
    # getting sorted serialized data by last_name (ascending)
    ordered_found_active_test_permissions = sorted(
        serialized_found_active_test_permissions_data,
        key=lambda k: k["last_name"],
        reverse=False,
    )
    # initializing test permissions ids array
    test_permission_ids_array = []
    # looping in ordered active test permissions
    for i in ordered_found_active_test_permissions:
        # inserting test permissions ids (ordered respectively by last name) in an array
        test_permission_ids_array.insert(len(test_permission_ids_array), i["id"])

    # sorting active test permissions queryset based on ordered (by last name) user permission ids
    new_found_active_test_permissions = list(
        TestPermissions.objects.filter(id__in=test_permission_ids_array)
    )
    new_found_active_test_permissions.sort(
        key=lambda t: test_permission_ids_array.index(t.id)
    )

    # getting page results based on queryset (new_found_active_test_permissions)
    page = paginator.paginate_queryset(new_found_active_test_permissions, request)
    # serializing the data
    serialized = GetTestPermissionsSerializer(page, many=True)
    # retuning final data using pagination library
    return paginator.get_paginated_response(serialized.data)

