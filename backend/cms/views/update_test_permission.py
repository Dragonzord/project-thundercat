from rest_framework.response import Response
from rest_framework import status
from cms.cms_models.test_permissions_model import TestPermissions
from cms.views.utils import is_undefined

# updating test permission
def update_test_permission(request):
    # making sure that we have the needed parameters
    test_permission_id = request.query_params.get("test_permission_id", None)
    expiry_date = request.query_params.get("expiry_date", None)
    if is_undefined(test_permission_id):
        return Response(
            {"error": "no 'test_permission_id' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(expiry_date):
        return Response(
            {"error": "no 'expiry_date' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )

    try:
        # getting specified test permission
        test_permission = TestPermissions.objects.get(id=test_permission_id)
        # updating specified test permission
        test_permission.expiry_date = expiry_date
        test_permission.save()
        return Response(status=status.HTTP_200_OK)
    except TestPermissions.DoesNotExist:
        return Response(
            {"error": "the specified test permission does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )
