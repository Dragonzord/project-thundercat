from django.utils import timezone
from rest_framework import serializers
from cms.cms_models.test import Test
from cms.cms_models.test_permissions_model import TestPermissions
from backend.custom_models.item_text import ItemText
from user_management.user_management_models.user_models import User


class FilterSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        # Exclude expired items
        query_date_time = timezone.now()
        data = (
            data.filter(is_public=False)
            .exclude(item__date_from__gt=query_date_time)
            .exclude(item__date_to__lt=query_date_time)
        )
        return super(FilterSerializer, self).to_representation(data)


# Serializers define the API representation
class GetNonPublicTestsSerializer(serializers.ModelSerializer):
    en_test_name = serializers.SerializerMethodField()
    fr_test_name = serializers.SerializerMethodField()

    # getting en_test_name
    def get_en_test_name(self, request):
        en_test_name = ItemText.objects.get(
            item_id=request.item_id, language_id=1
        ).text_detail
        return en_test_name

    # getting fr_test_name
    def get_fr_test_name(self, request):
        fr_test_name = ItemText.objects.get(
            item_id=request.item_id, language_id=2
        ).text_detail
        return fr_test_name

    class Meta:
        model = Test
        list_serializer_class = FilterSerializer
        fields = ("test_name", "is_public", "en_test_name", "fr_test_name")


class GetTestNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemText
        fields = ("text_detail", "language_id")


class GetTestInternalNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Test
        fields = "__all__"


class GetTestPermissionsSerializer(serializers.ModelSerializer):
    en_test_name = serializers.SerializerMethodField()
    fr_test_name = serializers.SerializerMethodField()
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()

    # getting test en_test_name
    def get_en_test_name(self, request):
        test_item_id = Test.objects.get(test_name=request.test_id).item_id
        en_test_name = ItemText.objects.get(
            item_id=test_item_id, language_id=1
        ).text_detail
        return en_test_name

    # getting test fr_test_name
    def get_fr_test_name(self, request):
        test_item_id = Test.objects.get(test_name=request.test_id).item_id
        fr_test_name = ItemText.objects.get(
            item_id=test_item_id, language_id=2
        ).text_detail
        return fr_test_name

    # getting ta first name
    def get_first_name(self, request):
        first_name = User.objects.get(username=request.username).first_name
        return first_name

    # getting ta last name
    def get_last_name(self, request):
        get_last_name = User.objects.get(username=request.username).last_name
        return get_last_name

    class Meta:
        model = TestPermissions
        fields = "__all__"
