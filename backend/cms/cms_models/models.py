from cms.cms_models.question_type import QuestionType
from cms.cms_models.question import Question
from cms.cms_models.test import Test
from cms.cms_models.test_permissions_model import TestPermissions

# These imports help to auto discover the models
