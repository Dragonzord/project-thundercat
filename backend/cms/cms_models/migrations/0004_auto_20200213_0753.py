# Generated by Django 2.2.3 on 2020-02-13 12:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms_models', '0003_auto_20200212_0830'),
    ]

    operations = [
        migrations.AddField(
            model_name='testpermissions',
            name='billing_contact',
            field=models.CharField(default='default', max_length=180),
        ),
        migrations.AddField(
            model_name='testpermissions',
            name='billing_contact_info',
            field=models.CharField(default='default', max_length=255),
        ),
        migrations.AddField(
            model_name='testpermissions',
            name='department_ministry_code',
            field=models.CharField(default='default', max_length=10),
        ),
        migrations.AddField(
            model_name='testpermissions',
            name='is_org',
            field=models.CharField(default='default', max_length=16),
        ),
        migrations.AddField(
            model_name='testpermissions',
            name='is_ref',
            field=models.CharField(default='default', max_length=20),
        ),
        migrations.AddField(
            model_name='testpermissions',
            name='staffing_process_number',
            field=models.CharField(default='default', max_length=50),
        ),
        migrations.AddField(
            model_name='testpermissions',
            name='test_order_number',
            field=models.CharField(default='default', max_length=12),
        ),
    ]
