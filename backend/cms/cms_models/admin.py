from django.contrib import admin
from cms.cms_models.test_permissions_model import TestPermissions

admin.site.register(TestPermissions)
