import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Router, Route, Redirect, Switch } from "react-router-dom";
import { authenticateAction, logoutAction, isTokenStillValid } from "./modules/LoginRedux";
import "./css/lib/aurora.min.css";
import "./css/cat-theme.css";
import { Helmet } from "react-helmet";
import Status from "./Status";
import Home from "./Home";
import TestAdministration from "./TestAdministration";
import Emib from "./components/eMIB/Emib";
import history from "./components/authentication/history";
import SelectLanguage from "./SelectLanguage";
import SiteNavBar from "./SiteNavBar";
import { PATH } from "./components/commons/Constants";
import { refreshAuthToken } from "./modules/LoginRedux";
import SampleTestsRoutes from "./components/samples/SampleTestRoutes";
import {
  getUserPermissions,
  updateCurrentHomePageState,
  updatePermissionsState
} from "./modules/PermissionsRedux";
import { PrivateRoute } from "./components/commons/PrivateRoute";
import Dashboard from "./Dashboard";
import Profile from "./components/profile/Profile";
import IncidentReport from "./IncidentReport";
import MyTests from "./MyTests";
import ContactUs from "./ContactUs";
import SystemAdministration from "./components/etta/SystemAdministration";
import PpcAdministration from "./PpcAdministration";
import Scorer from "./components/scorer/Scorer";
import { resetInboxState } from "./modules/EmibInboxRedux";
import { resetMetaDataState } from "./modules/LoadTestContentRedux";
import { resetTestStatusState } from "./modules/TestStatusRedux";
import { resetNotepadState } from "./modules/NotepadRedux";
import { resetTestStatusState as resetSampleTestStatusState } from "./modules/SampleTestStatusRedux";
import { resetPermissionsState } from "./modules/PermissionsRedux";
import { getAssignedTests } from "./modules/AssignedTestsRedux";
import ExpiredTokenPopop from "./ExpiredTokenPopup";

class App extends Component {
  static propTypes = {
    // Props from Redux
    authenticateAction: PropTypes.func,
    logoutAction: PropTypes.func,
    currentTestId: PropTypes.string,
    refreshAuthToken: PropTypes.func,
    isTestActive: PropTypes.bool,
    getUserPermissions: PropTypes.func,
    updatePermissionsState: PropTypes.func,
    resetInboxState: PropTypes.func,
    resetMetaDataState: PropTypes.func,
    resetTestStatusState: PropTypes.func,
    resetSampleTestStatusState: PropTypes.func,
    resetNotepadState: PropTypes.func,
    resetPermissionsState: PropTypes.func,
    isTokenStillValid: PropTypes.func,
    getAssignedTests: PropTypes.func,
    updateCurrentHomePageState: PropTypes.func
  };

  state = {
    setFocusOnQuitTestButton: false,
    isLanguageSelected: localStorage.getItem("catLanguage"),
    showTokenExpiredDialog: false
  };

  handleExpiredTokenAction = () => {
    // reset all states in case of token expiration
    this.props.logoutAction();
    this.props.resetInboxState();
    this.props.resetMetaDataState();
    this.props.resetTestStatusState();
    this.props.resetSampleTestStatusState();
    this.props.resetNotepadState();
    this.props.resetPermissionsState();
    // push to select language page (application root)
    history.push("/");
    // display token expired popup
    this.setState({ showTokenExpiredDialog: true });
  };

  closePopup = () => {
    this.setState({ showTokenExpiredDialog: false });
    // reloading page to make sure that all states are reseted
    window.location.reload();
  };

  initializeLanguage = () => {
    this.setState({ isLanguageSelected: true });
  };

  componentDidMount = () => {
    // set focus on quit test button on component load
    this.setState({ setFocusOnQuitTestButton: true });
    // getting the authentication token from the local storage
    const auth_token = localStorage.auth_token;

    // if there is no token, then there is no point in trying to verify it
    if (auth_token === undefined) {
      // update authenticated state to false
      this.props.authenticateAction(false);
      return;
    }
    // refresh the token
    else {
      this.refreshToken();
    }
  };

  componentDidUpdate = prevProps => {
    // if the user is authenticated and if the user is not yet defined (avoid to constantly call this function)
    // as soon as the user is defined, this function will not no longer be called
    if (this.props.authenticated) {
      const auth_token = localStorage.auth_token;
      // checks if the token is still valid
      this.props.isTokenStillValid(auth_token).then(bool => {
        // token is still valid
        if (bool) {
          // update authenticated state to true
          this.props.authenticateAction(true);
          // initialize permission flags
          this.props.getUserPermissions(auth_token, this.props.username).then(response => {
            let isSuperUser = this.props.isSuperUser;
            let isEtta = false;
            let isPpc = false;
            let isTa = false;
            let isScorer = false;
            // specified user is a super user
            if (isSuperUser) {
              isEtta = true;
              isPpc = true;
              isTa = true;
              isScorer = true;
              // // specified user is not a super user
            } else {
              // looping in user permissions to update the needed permission flags
              for (let i = 0; i < response.length; i++) {
                if (response[i].codename === "is_etta") {
                  isEtta = true;
                } else if (response[i].codename === "is_ppc") {
                  isPpc = true;
                } else if (response[i].codename === "is_scorer") {
                  isScorer = true;
                } else if (response[i].codename === "is_test_administrator") {
                  isTa = true;
                }
              }
            }
            // update permissions states based on the user permission flags
            this.props.updatePermissionsState({
              isEtta: isEtta,
              isPpc: isPpc,
              isTa: isTa,
              isScorer: isScorer
            });
            // update the current home page state based on the permissions
            // is super user or ETTA
            if (isEtta || isSuperUser) {
              this.props.updateCurrentHomePageState(PATH.systemAdministration);
              // is PPC
            } else if (isPpc) {
              this.props.updateCurrentHomePageState(PATH.ppcAdministration);
              // is scorer
            } else if (isScorer) {
              this.props.updateCurrentHomePageState(PATH.scorer);
              // is TA
            } else if (isTa) {
              this.props.updateCurrentHomePageState(PATH.testAdministration);
              //   // is candidate
            } else {
              this.props.updateCurrentHomePageState(PATH.dashboard);
            }
          });
          // getting assigned test for the current user
          this.props.getAssignedTests(this.props.username, auth_token).then(response => {
            // check for each assigned test
            for (let i = 0; i < response.length; i++) {
              // if there is an active test (status 5 = active)
              if (response[i].status === 5) {
                // redirect to dashboard page
                history.push(PATH.dashboard);
              }
            }
          });
          // token is expired
        } else {
          // update authenticated state to false
          this.props.authenticateAction(false);
          this.handleExpiredTokenAction();
        }
      });
      // if home page changes, push new path
      if (prevProps.currentHomePage !== this.props.currentHomePage) {
        history.push(this.props.currentHomePage);
      }
    }
  };

  // refreshing user auth token
  refreshToken = () => {
    const token = localStorage.getItem("auth_token");
    // token exists
    if (token !== null && this.props.authenticated) {
      this.props.isTokenStillValid(token).then(bool => {
        // checks if the token is still valid
        if (bool) {
          this.props.refreshAuthToken({ token }).then(response => {
            // removing the old token from the local storage
            localStorage.removeItem("auth_token");
            // updating the local storage with the new refreshed token
            localStorage.setItem("auth_token", response.token);
          });
          // token is expired
        } else {
          this.handleExpiredTokenAction();
        }
      });
    }
  };

  render() {
    // Determine if user has already selected a language. Based on local storage, not props
    const isLanguageSelected = localStorage.catLanguage;

    return (
      <div onBlur={this.refreshToken}>
        {!isLanguageSelected && <SelectLanguage initializeLanguage={this.initializeLanguage} />}
        {isLanguageSelected && (
          <div>
            <Helmet>
              <html lang={this.props.currentLanguage} />
            </Helmet>
            <div>
              <Router history={history}>
                <div>
                  <Route component={SiteNavBar} />
                  <Switch>
                    <PrivateRoute
                      exact
                      auth={!this.props.authenticated}
                      path={PATH.login}
                      component={Home}
                      redirectTo={this.props.currentHomePage}
                    />
                    <Route path={PATH.status} component={Status} />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.testBase}
                      component={Emib}
                      redirectTo={PATH.login}
                    />
                    {this.props.isTestActive && <Redirect to={`${PATH.testBase}${PATH.test}`} />}
                    <Route path={PATH.sampleTests} component={SampleTestsRoutes} />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={this.props.currentHomePage}
                      component={
                        this.props.isSuperUser || this.props.isEtta
                          ? SystemAdministration
                          : this.props.isPpc
                          ? PpcAdministration
                          : this.props.isScorer
                          ? Scorer
                          : this.props.isTa
                          ? TestAdministration
                          : Dashboard
                      }
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.isTa && this.props.authenticated}
                      path={PATH.testAdministration}
                      component={TestAdministration}
                      redirectTo={this.props.currentHomePage}
                    />
                    <PrivateRoute
                      auth={this.props.isEtta && this.props.authenticated}
                      path={PATH.systemAdministration}
                      component={SystemAdministration}
                      redirectTo={this.props.currentHomePage}
                    />
                    <PrivateRoute
                      auth={this.props.isPpc && this.props.authenticated}
                      path={PATH.ppcAdministration}
                      component={PpcAdministration}
                      redirectTo={this.props.currentHomePage}
                    />
                    <PrivateRoute
                      auth={this.props.isScorer && this.props.authenticated}
                      path={PATH.scorer}
                      component={Scorer}
                      redirectTo={this.props.currentHomePage}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.dashboard}
                      component={Dashboard}
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.profile}
                      component={Profile}
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.incidentReport}
                      component={IncidentReport}
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.myTests}
                      component={MyTests}
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.contactUs}
                      component={ContactUs}
                      redirectTo={PATH.login}
                    />
                    }
                    <Redirect to={PATH.login} />
                  </Switch>
                </div>
              </Router>
            </div>
          </div>
        )}
        <ExpiredTokenPopop
          showTokenExpiredDialog={this.state.showTokenExpiredDialog}
          closePopupFunction={this.closePopup}
        />
      </div>
    );
  }
}
export { PATH };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    authenticated: state.login.authenticated,
    isTestActive: state.testStatus.isTestActive,
    testNameId: state.testStatus.currentTestId,
    isSuperUser: state.user.isSuperUser,
    isEtta: state.userPermissions.isEtta,
    isPpc: state.userPermissions.isPpc,
    isTa: state.userPermissions.isTa,
    isScorer: state.userPermissions.isScorer,
    currentHomePage: state.userPermissions.currentHomePage,
    username: state.user.username
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      authenticateAction,
      logoutAction,
      refreshAuthToken,
      getUserPermissions,
      updatePermissionsState,
      resetInboxState,
      resetMetaDataState,
      resetTestStatusState,
      resetSampleTestStatusState,
      resetNotepadState,
      resetPermissionsState,
      isTokenStillValid,
      getAssignedTests,
      updateCurrentHomePageState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(App);
