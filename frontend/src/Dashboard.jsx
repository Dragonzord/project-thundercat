import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "./text_resources";
import { connect } from "react-redux";
import { isTokenStillValid } from "./modules/LoginRedux";
import { bindActionCreators } from "redux";
import AssignedTestTable from "./components/eMIB/AssignedTestTable";
import ContentContainer from "./components/commons/ContentContainer";
import { Helmet } from "react-helmet";
import { styles } from "./components/etta/permissions/ActivePermissions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

class Dashboard extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // Props from Redux
    isTokenStillValid: PropTypes.func
  };

  state = {
    isLoaded: false
  };

  // calling redux state on page load to get the first name and last name
  componentDidMount = () => {
    this._isMounted = true;
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid(localStorage.auth_token).then(bool => {
      // if the token is still valid
      if (this._isMounted && bool && !this.props.isUserLoading) {
        // using state value rather than the prop directly
        // the prop is "too" efficent;
        // essentially, when a redirect to another dashboard occurs
        // the prop updates this component faster than the redirect occurs
        // causing the dashboard to flash loading, this dash, and then the correct dashboard
        this.setState({
          isLoaded: true
        });
      }
    });
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  componentDidUpdate = prevState => {
    if (prevState.isLoaded !== this.state.isLoaded) {
      // focusing on welcome message after content load
      document.getElementById("user-welcome-message-div").focus();
    }
  };

  render() {
    // This renders two versions of the dashboard
    // If the user data has loaded, then it shows the candidate dash
    // otherwise it shows the header common to every dashboard
    // and a loading circle
    return (
      <div>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.home}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <div id="user-welcome-message-div" tabIndex={0} aria-labelledby="user-welcome-message">
              <h1 id="user-welcome-message" className="green-divider">
                {LOCALIZE.formatString(
                  LOCALIZE.dashboard.title,
                  this.props.firstName,
                  this.props.lastName
                )}
              </h1>
              {this.state.isLoaded && (
                <div>
                  <p>{LOCALIZE.dashboard.description}</p>
                  <AssignedTestTable username={this.props.username} />
                </div>
              )}
              {!this.state.isLoaded && (
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              )}
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { Dashboard as UnconnectedDashboard };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    username: state.user.username,
    isUserLoading: state.user.isUserLoading
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
