import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "./text_resources";
import PopupBox, { BUTTON_TYPE } from "./components/commons/PopupBox";

class ExpiredTokenPopup extends Component {
  static propTypes = {
    showTokenExpiredDialog: PropTypes.bool.isRequired,
    closePopupFunction: PropTypes.func.isRequired
  };

  render() {
    return (
      <div>
        <PopupBox
          show={this.props.showTokenExpiredDialog}
          title={LOCALIZE.tokenExpired.title}
          handleClose={() => {}}
          description={
            <div>
              <p>{LOCALIZE.tokenExpired.description}</p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.props.closePopupFunction}
        />
      </div>
    );
  }
}

export default ExpiredTokenPopup;
