import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { LANGUAGES } from "../../modules/LocalizeRedux";
import ReactPaginate from "react-paginate";
import Select from "react-select";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMarker, faCaretLeft, faCaretRight, faSpinner } from "@fortawesome/free-solid-svg-icons";
import { getDisplayOptions } from "../../helpers/getDisplayOptions";
import { alternateColorsStyle } from "../commons/Constants";
import { bindActionCreators } from "redux";
import {
  updateCurrentScorerAssignedTestPageState,
  updateScorerAssignedTestPageSizeState
} from "../../modules/ScorerRedux";
import "../../css/etta.css";
import { styles as activePermissionsStyles } from "../etta/permissions/ActivePermissions";

export const styles = {
  ...activePermissionsStyles,
  ...{
    assignedTestIdColumn: {
      paddingLeft: 12,
      width: "14%"
    },
    completionDateColumn: {
      textAlign: "center"
    },
    testNameColumn: {
      textAlign: "center"
    },
    testLanguageColumn: {
      textAlign: "center"
    },
    scoreButtonColumn: {
      textAlign: "center"
    },
    spacingDiv: {
      position: "relative",
      width: 950,
      display: "table-cell"
    },
    scoreTestBtn: {
      width: 150,
      transform: "scale(1.5)",
      padding: "2px 10px"
    },
    scoreText: {
      paddingLeft: "10px",
      margin: 0
    }
  }
};

class ScorerTable extends Component {
  static propTypes = {
    currentLanguage: PropTypes.string.isRequired,
    numberOfPages: PropTypes.number.isRequired,
    currentlyLoading: PropTypes.bool.isRequired,
    assignedTests: PropTypes.array.isRequired,
    scoreTest: PropTypes.func,
    //props from redux
    updateCurrentScorerAssignedTestPageState: PropTypes.func,
    updateScorerAssignedTestPageSizeState: PropTypes.func,
    scorerPaginationPage: PropTypes.number.isRequired,
    scorerPaginationPageSize: PropTypes.number.isRequired
  };

  state = {
    displayOptionsArray: [],
    displayOptionSelectedValue: {
      label: `${this.props.scorerPaginationPageSize}`,
      value: this.props.scorerPaginationPageSize
    },
    assignedTests: []
  };

  componentDidMount = () => {
    this.populateDisplayOptions();
  };

  componentDidUpdate = prevProps => {
    if (prevProps.assignedTests !== this.props.assignedTests) {
      this.setState({ assignedTests: this.props.assignedTests });
    }
  };

  // handling page changes based on pagination selection
  handlePageChange = id => {
    // "+1" because redux scorerPaginationPage uses index 1 and react-paginate (id) uses index 0
    const selectedPage = id.selected + 1;
    // update current page redux state
    this.props.updateCurrentScorerAssignedTestPageState(selectedPage);
    // focusing on the first table's row
    document.getElementById("table-row-0").focus();
  };

  getSelectedDisplayOption = selectedOption => {
    this.setState(
      {
        displayOptionSelectedValue: selectedOption
      },
      () => {
        // update page size
        this.props.updateScorerAssignedTestPageSizeState(selectedOption.value);
        // go back to the first page to avoid display bugs
        this.props.updateCurrentScorerAssignedTestPageState(1);
      }
    );
  };

  // populate display options
  populateDisplayOptions = () => {
    this.setState({ displayOptionsArray: getDisplayOptions() });
  };

  render() {
    return (
      <div>
        <div style={styles.spacingDiv} />
        <div style={styles.displayOptionContainer}>
          <div style={styles.displayOptionLabel}>
            <label id="display-options-label">
              {LOCALIZE.scorer.assignedTest.displayOptionLabel}
            </label>
            <label id="display-option-accessibility" style={styles.hiddenText}>
              {LOCALIZE.scorer.assignedTest.displayOptionAccessibility}
            </label>
          </div>
          <div style={styles.displayOptionDropdown}>
            <Select
              id="display-options-dropdown"
              name="display-options"
              aria-labelledby="display-options-label display-option-accessibility display-option-current-value-accessibility"
              placeholder=""
              options={this.state.displayOptionsArray}
              onChange={this.getSelectedDisplayOption}
              clearable={false}
              value={this.state.displayOptionSelectedValue}
            ></Select>
            <label
              id="display-option-current-value-accessibility"
              style={styles.hiddenText}
            >{`${LOCALIZE.scorer.assignedTest.displayOptionCurrentValueAccessibility} ${this.state.displayOptionSelectedValue.value}`}</label>
          </div>
        </div>
        <table style={styles.table}>
          <thead>
            <tr style={styles.tableHead}>
              <th id="assigned-test-id-column" scope="col" style={styles.assignedTestIdColumn}>
                {LOCALIZE.scorer.assignedTest.table.assignedTestId}
              </th>
              <th id="completion-date-column" scope="col" style={styles.completionDateColumn}>
                {LOCALIZE.scorer.assignedTest.table.completionDate}
              </th>
              <th id="test-name-column" scope="col" style={styles.testNameColumn}>
                {LOCALIZE.scorer.assignedTest.table.testName}
              </th>
              <th id="test-language-column" scope="col" style={styles.testLanguageColumn}>
                {LOCALIZE.scorer.assignedTest.table.testLanguage}
              </th>
              <th id="score-button-column" scope="col" style={styles.scoreButtonColumn}>
                {LOCALIZE.scorer.assignedTest.table.scoreButton}
              </th>
            </tr>
          </thead>
          <tbody>
            {this.state.assignedTests.length > 0 &&
              !this.props.currentlyLoading &&
              this.state.assignedTests.map((assignedTest, id) => {
                return (
                  <tr
                    id={`table-row-${id}`}
                    key={id}
                    style={alternateColorsStyle(id, 60)}
                    tabIndex={0}
                    aria-labelledby={`assigned-test-id-column name-label-${id} completion-date-column completion-date-label-${id} test-name-column test-name-label-${id} test-language-column test-language-label-${id} score-button-column score-button-label-${id}`}
                  >
                    <td id={`assigned-test-id-label-${id}`} style={styles.assignedTestIdColumn}>
                      <label>{assignedTest.assigned_test_id}</label>
                    </td>
                    <td id={`completion-date-label-${id}`} style={styles.completionDateColumn}>
                      <label>{assignedTest.assigned_test_submit_date}</label>
                    </td>

                    <td id={`test-name-label-${id}`} style={styles.testNameColumn}>
                      <label>
                        {this.props.currentLanguage === LANGUAGES.english
                          ? assignedTest.assigned_test_en_name
                          : assignedTest.assigned_test_fr_name}
                      </label>
                    </td>

                    <td id={`test-language-label-${id}`} style={styles.testLanguageColumn}>
                      <label>
                        {assignedTest.assigned_test_test_session_language === LANGUAGES.english
                          ? LOCALIZE.scorer.assignedTest.table.en
                          : assignedTest.assigned_test_test_session_language === LANGUAGES.french
                          ? LOCALIZE.scorer.assignedTest.table.fr
                          : null}
                      </label>
                    </td>

                    <td id={`score-button-label-${id}`} style={styles.scoreButtonColumn}>
                      <button
                        className="btn btn-secondary"
                        aria-labelledby="view-permission-request-btn"
                        style={styles.scoreTestBtn}
                        onClick={() => this.props.scoreTest(id)}
                      >
                        <FontAwesomeIcon icon={faMarker} />
                        <label style={styles.scoreText}>
                          {LOCALIZE.scorer.assignedTest.scoreTest}
                        </label>
                      </button>
                    </td>
                  </tr>
                );
              })}
            {this.state.assignedTests.length <= 0 && !this.props.currentlyLoading && (
              <tr id="no-results-table-row" style={styles.noResultsFoundRow} tabIndex={0}>
                <td colSpan="5" style={styles.noResultsFoundLabel}>
                  <label>{LOCALIZE.scorer.assignedTest.noResultsFound}</label>
                </td>
                <td style={styles.displayNone}></td>
                <td style={styles.displayNone}></td>
                <td style={styles.displayNone}></td>
                <td style={styles.displayNone}></td>
              </tr>
            )}
            {this.props.currentlyLoading && (
              <tr style={styles.loading}>
                <td colSpan="5">
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} />
                  </label>
                </td>
                <td style={styles.displayNone}></td>
                <td style={styles.displayNone}></td>
                <td style={styles.displayNone}></td>
                <td style={styles.displayNone}></td>
              </tr>
            )}
          </tbody>
        </table>
        <div style={styles.paginationContainer}>
          <ReactPaginate
            pageCount={this.props.numberOfPages}
            containerClassName={"pagination"}
            breakClassName={"break"}
            activeClassName={"active-page"}
            marginPagesDisplayed={3}
            // "-1" because react-paginate uses index 0 and scorerPaginationPage redux state uses index 1
            forcePage={this.props.scorerPaginationPage - 1}
            onPageChange={page => {
              this.handlePageChange(page);
            }}
            previousLabel={
              <div style={styles.paginationIcon}>
                <label style={styles.hiddenText}>
                  {LOCALIZE.scorer.assignedTest.previousPageButton}
                </label>
                <FontAwesomeIcon icon={faCaretLeft} />
              </div>
            }
            nextLabel={
              <div style={styles.paginationIcon}>
                <label style={styles.hiddenText}>
                  {LOCALIZE.scorer.assignedTest.nextPageButton}
                </label>
                <FontAwesomeIcon icon={faCaretRight} />
              </div>
            }
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    scorerPaginationPage: state.scorer.scorerPaginationPage,
    scorerPaginationPageSize: state.scorer.scorerPaginationPageSize
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateCurrentScorerAssignedTestPageState,
      updateScorerAssignedTestPageSizeState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ScorerTable);
