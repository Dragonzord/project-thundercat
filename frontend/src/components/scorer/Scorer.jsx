import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import ContentContainer from "../commons/ContentContainer";
import { getAssignedTests } from "../../modules/ScorerRedux";
import ScorerTable from "./ScorerTable";

export const styles = {
  header: {
    marginBottom: 24
  }
};

class Scorer extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // provided by redux
    getAssignedTests: PropTypes.func,
    username: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    currentLanguage: PropTypes.string.isRequired,
    scorerPaginationPageSize: PropTypes.number.isRequired
  };

  state = {
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    currentlyLoading: false,
    assignedTests: []
  };

  componentDidMount = () => {
    this._isMounted = true;
    this.populateAssignedTests();
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  componentDidUpdate = prevProps => {
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateAssignedTests();
    }
    if (prevProps.scorerPaginationPage !== this.props.scorerPaginationPage) {
      this.populateAssignedTests();
    }
    if (prevProps.scorerPaginationPageSize !== this.props.scorerPaginationPageSize) {
      this.populateAssignedTests();
    }
  };

  populateAssignedTests = () => {
    if (this._isMounted) {
      this.setState({ currentlyLoading: true }, () => {
        let assignedTests = [];
        this.props
          .getAssignedTests(
            localStorage.getItem("auth_token"),
            this.props.username,
            this.props.scorerPaginationPage,
            this.props.scorerPaginationPageSize
          )
          .then(response => {
            this.populateAssignedTestsObject(assignedTests, response);
          })
          .then(() => {
            if (this._isMounted) {
              this.setState({ currentlyLoading: false });
            }
          });
      });
    }
  };

  populateAssignedTestsObject = (assignedTests, response) => {
    // looping in response given
    for (let i = 0; i < response.results.length; i++) {
      let result = response.results[i];
      // pushing needed data in assigned tests array
      assignedTests.push({
        id: result.id,
        assigned_test_id: result.assigned_test_id,
        assigned_test_status: result.assigned_test_status,
        assigned_test_start_date: result.assigned_test_start_date,
        assigned_test_internal_name: result.assigned_test_test_name,
        assigned_test_en_name: result.assigned_test_en_name,
        assigned_test_fr_name: result.assigned_test_fr_name,
        assigned_test_submit_date: result.assigned_test_submit_date,
        assigned_test_test_session_language: result.assigned_test_test_session_language,
        scorer_username: result.scorer_username,
        status: result.status,
        score_date: result.score_date,
        assigned_test: result.assigned_test
      });
    }

    // saving results in state
    if (this._isMounted) {
      this.setState({
        assignedTests: assignedTests,
        numberOfPages: Math.ceil(response.count / this.props.scorerPaginationPageSize),
        nextPageNumber: response.next_page_number,
        previousPageNumber: response.previous_page_number
      });
    }
  };

  scoreTest = id => {
    console.log("Score test");
    console.log(id);
  };

  render() {
    return (
      <div>
        <ContentContainer>
          <div>
            <div id="main-content" role="main">
              <div
                id="user-welcome-message-div"
                style={styles.header}
                tabIndex={0}
                aria-labelledby="user-welcome-message"
              >
                <h1 id="user-welcome-message" className="green-divider">
                  {LOCALIZE.formatString(
                    LOCALIZE.profile.title,
                    this.props.firstName,
                    this.props.lastName
                  )}
                </h1>
              </div>
            </div>
            <ScorerTable
              currentLanguage={this.props.currentLanguage}
              numberOfPages={this.state.numberOfPages}
              currentlyLoading={this.state.currentlyLoading}
              assignedTests={this.state.assignedTests}
              scoreTest={this.scoreTest}
            ></ScorerTable>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    username: state.user.username,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    currentLanguage: state.localize.language,
    scorerPaginationPage: state.scorer.scorerPaginationPage,
    scorerPaginationPageSize: state.scorer.scorerPaginationPageSize
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAssignedTests
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Scorer);
