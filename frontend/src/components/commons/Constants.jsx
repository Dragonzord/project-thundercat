const PATH = {
  login: "/login",
  dashboard: "/check-in",
  status: "/status",
  emibSampleTest: "/emib-sample",
  test: "/active",
  testBase: "/test",
  sampleTests: "/sample-tests",
  instructions: "/instructions",
  overview: "/overview",
  submit: "/submit",
  quit: "/quit",
  testAdministration: "/test-sessions",
  profile: "/profile",
  incidentReport: "/incident-report",
  myTests: "/my-tests",
  contactUs: "/contact-us",
  systemAdministration: "/system-administration",
  ppcAdministration: "/ppc-administration",
  scorer: "/scorer"
};

const STATUS_API_PATH = {
  activate: "/api/activate-test/",
  submit: "/api/submit-test/",
  timeout: "/api/timeout-test/",
  quit: "/api/quit-test/"
};

// this function will put alternate colors (grey and white rows) in the respsective table
/* parameters:
    - id (from the map function)
    - height (desired row height)
*/
const alternateColorsStyle = (id, height) => {
  const styles = {
    rowContainerBasicStyle: {
      width: "100%",
      height: `${height}px`,
      padding: "8px 0 8px 12px"
    },
    rowContainerDark: {
      backgroundColor: "#F3F3F3"
    },
    rowContainerLight: {
      backgroundColor: "white"
    },
    rowContainerBorder: {
      borderTop: "1px solid #CECECE"
    }
  };

  if (id === 0) {
    return { ...styles.rowContainerBasicStyle, ...styles.rowContainerLight };
  } else if (id % 2 === 0) {
    return {
      ...styles.rowContainerBasicStyle,
      ...styles.rowContainerBorder,
      ...styles.rowContainerLight
    };
  } else {
    return {
      ...styles.rowContainerBasicStyle,
      ...styles.rowContainerBorder,
      ...styles.rowContainerDark
    };
  }
};

export { PATH, STATUS_API_PATH, alternateColorsStyle };
