import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { Row, Col, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight, faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import PropTypes from "prop-types";

const styles = {
  buttonRowPadding: {
    paddingBottom: 20,
    paddingTop: 20
  },
  buttonNext: {
    float: "right"
  },
  span: {
    paddingLeft: 5,
    paddingRight: 5
  }
};

class NextPreviousButtonNav extends Component {
  static propTypes = {
    showNext: PropTypes.bool,
    showPrevious: PropTypes.bool,
    onChangeToNext: PropTypes.func.isRequired,
    onChangeToPrevious: PropTypes.func.isRequired
  };

  render() {
    return (
      <Row style={styles.buttonRowPadding}>
        <Col xs={6}>
          {this.props.showPrevious && (
            <Button variant="secondary" onClick={this.props.onChangeToPrevious}>
              <FontAwesomeIcon icon={faChevronLeft} />
              <span style={styles.span}>{LOCALIZE.emibTest.tabs.previousButton}</span>
            </Button>
          )}
        </Col>

        <Col xs={6}>
          {this.props.showNext && (
            <Button
              style={styles.buttonNext}
              variant="secondary"
              onClick={this.props.onChangeToNext}
            >
              <span style={styles.span}>{LOCALIZE.emibTest.tabs.nextButton}</span>
              <FontAwesomeIcon icon={faChevronRight} />
            </Button>
          )}
        </Col>
      </Row>
    );
  }
}

export default NextPreviousButtonNav;
