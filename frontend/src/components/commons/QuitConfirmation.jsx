import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import ContentContainer from "./ContentContainer";

class QuitConfirmation extends Component {
  static props = {
    resetRedux: PropTypes.func
  };

  componentDidMount = () => {
    // focusing on you have quit the test div on page load
    document.getElementById("you-have-quit-the-test-div").focus();
  };
  componentWillUnmount = () => {
    this.props.resetRedux();
    this.props.logoutAction();
  };
  render() {
    return (
      <ContentContainer>
        <div id="main-content" role="main">
          <section aria-labelledby="you-have-quit-the-test-div">
            <div id="you-have-quit-the-test-div" tabIndex={0}>
              <h1 className="green-divider">{LOCALIZE.emibTest.quitConfirmationPage.title}</h1>
              <p>
                {LOCALIZE.emibTest.quitConfirmationPage.instructionsTestNotScored1}
                <span className="font-weight-bold">
                  {LOCALIZE.emibTest.quitConfirmationPage.instructionsTestNotScored2}
                </span>
              </p>
              <p>{LOCALIZE.emibTest.quitConfirmationPage.instructionsRaiseHand}</p>
              <p>
                {LOCALIZE.formatString(
                  LOCALIZE.emibTest.quitConfirmationPage.instructionsEmail,
                  <a href="mailto:cfp.cpp-ppc.psc@canada.ca">cfp.cpp-ppc.psc@canada.ca</a>
                )}
              </p>
            </div>
          </section>
        </div>
      </ContentContainer>
    );
  }
}

export default QuitConfirmation;
