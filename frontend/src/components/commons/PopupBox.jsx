import React, { Component } from "react";
import PropTypes from "prop-types";
import Modal from "react-modal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import "../../css/popup-box.css";
import LOCALIZE from "../../text_resources";

export const BUTTON_TYPE = {
  primary: "btn btn-primary",
  secondary: "btn btn-secondary",
  danger: "btn btn-danger"
};

export const BUTTON_STATE = {
  disabled: true,
  enabled: false
};

// default custom style
const customStyles = {
  content: {
    maxWidth: 700,
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    backgroundColor: "#F5FAFB",
    padding: 0
  },
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.7)"
  }
};

// style that allows drop down to overlap the modal, so the drop down is always visible, even if it exceeds the modal window
const customStylesWithVisibleOverflow = {
  content: {
    maxWidth: 700,
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    backgroundColor: "#F5FAFB",
    padding: 0,
    overflow: "visible"
  },
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.7)"
  }
};

const styles = {
  contentPadding: {
    padding: "10px 15px",
    overflow: "auto",
    maxHeight: "calc(100vh - 300px)",
    borderBottom: "1px solid #CECECE",
    borderTop: "1px solid #CECECE"
  },
  headerPaddingWithoutCloseBtn: {
    padding: "15px 15px 5px 15px"
  },
  headerPaddingWithCloseBtn: {
    padding: "15px 55px 5px 15px"
  },
  footerPadding: {
    padding: 15,
    height: 70
  },
  rightButton: {
    float: "right",
    paddingLeft: 30,
    paddingRight: 30,
    height: 38,
    minWidth: 100
  },
  leftButton: {
    paddingLeft: 30,
    paddingRight: 30,
    height: 38,
    minWidth: 100
  },
  overflowVisible: {
    overflow: "visible"
  },
  icon: {
    transform: "scale(2)",
    padding: "1.5px"
  },
  floatLeft: {
    float: "left"
  },
  floatRight: {
    float: "right"
  },
  closeButton: {
    position: "absolute",
    right: 0,
    top: 0,
    margin: "10px 6px 0 0",
    padding: "6px 16px",
    fontSize: 24,
    background: "transparent",
    border: "none",
    color: "black"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  noStyle: {}
};

class PopupBox extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      show: PropTypes.bool,
      handleClose: PropTypes.func,
      customPopupStyle: PropTypes.object,
      title: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      leftButtonType: PropTypes.string,
      leftButtonTitle: PropTypes.string,
      leftButtonIcon: PropTypes.symbol,
      leftButtonIconCustomStyle: PropTypes.object,
      leftButtonLabel: PropTypes.string,
      leftButtonState: PropTypes.string,
      leftButtonAction: PropTypes.func,
      // you should only use this props to change the button's color => leftButtonCustomStyle={{backgroundColor: "<color_code>"}}
      leftButtonCustomStyle: PropTypes.object,
      rightButtonType: PropTypes.string,
      rightButtonTitle: PropTypes.string,
      rightButtonIcon: PropTypes.symbol,
      rightButtonIconCustomStyle: PropTypes.object,
      rightButtonLabel: PropTypes.string,
      rightButtonAction: PropTypes.func,
      rightButtonState: PropTypes.string,
      // you should only use this props to change the button's color => leftButtonCustomStyle={{backgroundColor: "<color_code>"}}
      rightButtonCustomStyle: PropTypes.object,
      isBackdropStatic: PropTypes.bool,
      shouldCloseOnEsc: PropTypes.bool,
      onPopupOpen: PropTypes.func,
      onPopupClose: PropTypes.func,
      overflowVisible: PropTypes.bool,
      displayCloseButton: PropTypes.bool,
      closeButtonAction: PropTypes.func
    };
    // click away or esc to close
    PopupBox.defaultProps = {
      isBackdropStatic: false,
      overflowVisible: false
    };
  }

  leftButtonCloseAndAction = () => {
    if (this.props.leftButtonAction) {
      this.props.leftButtonAction();
    }
    this.props.handleClose();
  };

  rightButtonCloseAndAction = () => {
    if (this.props.rightButtonAction) {
      this.props.rightButtonAction();
    }
    this.props.handleClose();
  };

  handleOnAfterOpen = () => {
    this.removeScrollFromBody();
    this.props.onPopupOpen();
  };

  handleOnAfterClose = () => {
    this.putBackScrollFromBody();
    this.props.onPopupClose();
  };

  // removing scroll from body
  removeScrollFromBody = () => {
    document.body.style.overflow = "hidden";
  };

  // put back scroll from body
  putBackScrollFromBody = () => {
    document.body.removeAttribute("style");
  };

  render() {
    const {
      show,
      handleClose,
      title,
      description,
      leftButtonType,
      leftButtonTitle,
      leftButtonIcon,
      leftButtonLabel,
      leftButtonState,
      rightButtonType,
      rightButtonTitle,
      rightButtonIcon,
      rightButtonLabel,
      rightButtonState
    } = this.props;

    // If a root node exists, the app is being served, otherwise it's a unit test.
    let ariaHideApp = true;
    if (document.getElementById("#root")) {
      Modal.setAppElement("#root");
    } else {
      // Unit tests do not consider outside of the dialog.
      ariaHideApp = false;
    }
    return (
      <Modal
        isOpen={show}
        onRequestClose={handleClose}
        onAfterOpen={
          this.props.onPopupOpen ? this.handleOnAfterOpen : () => this.removeScrollFromBody()
        }
        onAfterClose={
          this.props.onPopupClose ? this.handleOnAfterClose : () => this.putBackScrollFromBody()
        }
        shouldCloseOnEsc={this.props.shouldCloseOnEsc}
        style={this.props.overflowVisible ? customStylesWithVisibleOverflow : customStyles}
        contentLabel={title}
        shouldCloseOnOverlayClick={!this.props.isBackdropStatic}
        aria={{
          labelledby: "modal-heading",
          describedby: "modal-description"
        }}
        ariaHideApp={ariaHideApp}
      >
        <div style={this.props.customPopupStyle ? this.props.customPopupStyle : styles.noStyle}>
          <div
            style={
              this.props.displayCloseButton
                ? styles.headerPaddingWithCloseBtn
                : styles.headerPaddingWithoutCloseBtn
            }
          >
            <h1 id="modal-heading">{title}</h1>
            {this.props.displayCloseButton && (
              <button
                className="close-button btn btn-secondary"
                style={styles.closeButton}
                onClick={this.props.closeButtonAction}
              >
                <FontAwesomeIcon icon={faTimes} />
                <span style={styles.hiddenText}>{LOCALIZE.commons.close}</span>
              </button>
            )}
          </div>

          <div
            id="modal-description"
            style={
              this.props.overflowVisible
                ? { ...styles.contentPadding, ...styles.overflowVisible }
                : styles.contentPadding
            }
          >
            {description}
          </div>

          <div style={styles.footerPadding}>
            {leftButtonType && (
              <div style={styles.floatLeft}>
                <button
                  className={
                    this.props.leftButtonCustomStyle
                      ? `${leftButtonType} custom-button-style`
                      : leftButtonType
                  }
                  style={
                    this.props.leftButtonCustomStyle
                      ? { ...styles.leftButton, ...this.props.leftButtonCustomStyle }
                      : styles.leftButton
                  }
                  aria-label={leftButtonIcon ? leftButtonLabel : leftButtonTitle}
                  onClick={() => this.leftButtonCloseAndAction()}
                  disabled={leftButtonState}
                  id="unit-test-left-btn"
                >
                  {leftButtonTitle ? (
                    leftButtonTitle
                  ) : (
                    <FontAwesomeIcon
                      icon={leftButtonIcon}
                      style={
                        this.props.leftButtonIconCustomStyle
                          ? this.props.leftButtonIconCustomStyle
                          : styles.icon
                      }
                    />
                  )}
                </button>
              </div>
            )}
            {rightButtonType && (
              <div style={styles.floatRight}>
                <button
                  className={
                    this.props.rightButtonCustomStyle
                      ? `${rightButtonType} custom-button-style`
                      : rightButtonType
                  }
                  style={
                    this.props.rightButtonCustomStyle
                      ? { ...styles.rightButton, ...this.props.rightButtonCustomStyle }
                      : styles.rightButton
                  }
                  aria-label={rightButtonIcon ? rightButtonLabel : rightButtonTitle}
                  onClick={this.rightButtonCloseAndAction}
                  disabled={rightButtonState}
                  id="unit-test-right-btn"
                >
                  {rightButtonTitle ? (
                    rightButtonTitle
                  ) : (
                    <FontAwesomeIcon
                      icon={rightButtonIcon}
                      style={
                        this.props.rightButtonIconCustomStyle
                          ? this.props.rightButtonIconCustomStyle
                          : styles.icon
                      }
                    />
                  )}
                </button>
              </div>
            )}
          </div>
        </div>
      </Modal>
    );
  }
}

export default PopupBox;
