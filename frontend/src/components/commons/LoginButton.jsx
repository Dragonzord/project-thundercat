import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { PATH } from "../../App";
import { Button } from "react-bootstrap";

const styles = {
  navlink: {
    marginRight: 15
  }
};

class LoginButton extends Component {
  static propTypes = {
    // Props from Redux
    currentLanguage: PropTypes.string
  };

  handleClick = () => {
    this.props.history.push(PATH.login);
  };

  render() {
    return (
      <div>
        {!this.props.authenticated && (
          <div>
            <Button className="btn btn-primary" onClick={this.handleClick} style={styles.navlink}>
              {LOCALIZE.commons.login}
            </Button>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    authenticated: state.login.authenticated,
    currentLanguage: state.localize.language
  };
};

export default connect(
  mapStateToProps,
  null
)(withRouter(LoginButton));
