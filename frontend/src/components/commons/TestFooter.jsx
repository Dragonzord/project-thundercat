import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { Navbar } from "react-bootstrap";
import Timer from "../commons/Timer";

const styles = {
  footer: {
    borderTop: "1px solid #96a8b2",
    backgroundColor: "#D5DEE0"
  },
  content: {
    width: 1400,
    margin: "0 auto",
    padding: "0px 20px"
  },
  button: {
    float: "right"
  },
  footerIndex: {
    zIndex: 1
  }
};

class TestFooter extends Component {
  static propTypes = {
    submitTest: PropTypes.func,
    timeout: PropTypes.func.isRequired,
    startTime: PropTypes.string
  };

  render() {
    return (
      <div role="contentinfo" className="fixed-bottom" style={styles.footerIndex}>
        <Navbar fixed="bottom" style={styles.footer}>
          <div style={styles.content} id="unit-test-timer">
            <Timer
              timeout={this.props.timeout}
              startTime={this.props.startTime}
              timeLimit={this.props.timeLimit}
            />
            <div style={styles.button} id="unit-test-submit-btn">
              <button
                id="unit-test-submit-btn"
                type="button"
                className="btn btn-success"
                onClick={this.props.submitTest}
              >
                {LOCALIZE.commons.submitTestButton}
              </button>
            </div>
          </div>
        </Navbar>
      </div>
    );
  }
}

export default TestFooter;
