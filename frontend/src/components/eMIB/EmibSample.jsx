import React, { Component } from "react";
import PropTypes from "prop-types";
import Emib from "./Emib";

class EmibSample extends Component {
  static propTypes = {
    testNameId: PropTypes.string
  };

  render() {
    return (
      <div id="unit-test-sample-test">
        <Emib testNameId={this.props.testNameId} updateBackendTest={() => {}} />
      </div>
    );
  }
}

export default EmibSample;
