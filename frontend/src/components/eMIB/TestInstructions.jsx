import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import Email from "./Email";
import ActionViewEmail from "./ActionViewEmail";
import ActionViewTask from "./ActionViewTask";
import { ACTION_TYPE, EMAIL_TYPE } from "./constants";

const styles = {
  containerWidth: {
    width: "100%"
  },
  disabledExampleComponent: {
    border: "1px solid #00565e",
    borderRadius: 4,
    marginTop: 4,
    marginBottom: 4,
    padding: 16
  },
  disabledExampleComponentNoPadding: {
    border: "1px solid #00565e",
    borderRadius: 4,
    marginTop: 4,
    marginBottom: 4,
    margin: 0
  },
  underline: {
    textDecoration: "underline"
  }
};

class TestInstructions extends Component {
  render() {
    const exampleEmail = {
      id: 0,
      to: LOCALIZE.emibTest.howToPage.testInstructions.exampleEmail.to,
      from: LOCALIZE.emibTest.howToPage.testInstructions.exampleEmail.from,
      subject: LOCALIZE.emibTest.howToPage.testInstructions.exampleEmail.subject,
      date: LOCALIZE.emibTest.howToPage.testInstructions.exampleEmail.date,
      body: LOCALIZE.emibTest.howToPage.testInstructions.exampleEmail.body
    };

    const exampleEmailResponse = {
      actionType: ACTION_TYPE.email,
      emailType: EMAIL_TYPE.reply,
      emailTo: [8], // Geneviève Bédard in the address book
      emailCc: [],
      emailBody: LOCALIZE.emibTest.howToPage.testInstructions.exampleEmailResponse.emailBody,
      reasonsForAction: ""
    };

    const exampleEmailResponseWithReasons = {
      actionType: ACTION_TYPE.email,
      emailType: EMAIL_TYPE.reply,
      emailTo: [8], // Geneviève Bédard in the address book
      emailCc: [],
      emailBody: LOCALIZE.emibTest.howToPage.testInstructions.exampleEmailResponse.emailBody,
      reasonsForAction:
        LOCALIZE.emibTest.howToPage.testInstructions.exampleEmailResponse.reasonsForAction
    };

    const exampleTaskResponse = {
      actionType: ACTION_TYPE.task,
      task: LOCALIZE.emibTest.howToPage.testInstructions.exampleTaskResponse.task,
      reasonsForAction: ""
    };

    const exampleTaskResponseWithReasons = {
      actionType: ACTION_TYPE.task,
      task: LOCALIZE.emibTest.howToPage.testInstructions.exampleTaskResponse.task,
      reasonsForAction:
        LOCALIZE.emibTest.howToPage.testInstructions.exampleTaskResponse.reasonsForAction
    };

    return (
      <div style={styles.containerWidth}>
        <h1>{LOCALIZE.emibTest.howToPage.testInstructions.title}</h1>
        <div>
          <p>{LOCALIZE.emibTest.howToPage.testInstructions.para1}</p>
          <p>{LOCALIZE.emibTest.howToPage.testInstructions.para2}</p>
          <ul>
            <li>{LOCALIZE.emibTest.howToPage.testInstructions.bullet1}</li>
            <li>{LOCALIZE.emibTest.howToPage.testInstructions.bullet2}</li>
            <li>{LOCALIZE.emibTest.howToPage.testInstructions.bullet3}</li>
          </ul>
        </div>
        <div>
          <section aria-labelledby="reponding-to-emails">
            <h2 id="reponding-to-emails">
              {LOCALIZE.emibTest.howToPage.testInstructions.step1Section.title}
            </h2>
            <p>{LOCALIZE.emibTest.howToPage.testInstructions.step1Section.description}</p>
            <section aria-labelledby="responding-with-email-response">
              <h3 id="responding-with-email-response">
                {LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part1.title}
              </h3>
              <div style={styles.disabledExampleComponentNoPadding}>
                <Email email={exampleEmail} disabled={true} />
              </div>
              <p>{LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part1.para1}</p>
            </section>
            <section aria-labelledby="reponding-with-email">
              <h3 id="reponding-with-email">
                {LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part2.title}
              </h3>
              <p>{LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part2.para1}</p>
              <p>{LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part2.para2}</p>
            </section>
            <section aria-labelledby="example-of-email-response">
              <h3 id="example-of-email-response">
                {LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part3.title}
              </h3>
              <div style={styles.disabledExampleComponent}>
                <ActionViewEmail
                  action={exampleEmailResponse}
                  actionId={1}
                  email={exampleEmail}
                  disabled={true}
                  isInstructions={true}
                />
              </div>
            </section>
            <section aria-labelledby="adding-a-task">
              <h3 id="adding-a-task">
                {LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part4.title}
              </h3>
              <p>{LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part4.para1}</p>
            </section>
            <section aria-labelledby="example-of-adding-a-task">
              <h3 id="example-of-adding-a-task">
                {LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part5.title}
              </h3>
              <div style={styles.disabledExampleComponent}>
                <ActionViewTask
                  action={exampleTaskResponse}
                  actionId={1}
                  email={exampleEmail}
                  disabled={true}
                />
              </div>
            </section>
            <section aria-labelledby="choosing-method-of-responding">
              <h3 id="choosing-method-of-responding">
                {LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part6.title}
              </h3>
              <p>{LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part6.para1}</p>
              <ol>
                <li>{LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part6.bullet1}</li>
                <li>{LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part6.bullet2}</li>
                <li>{LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part6.bullet3}</li>
              </ol>
              <p>{LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part6.para2}</p>
              <p>
                <span>
                  {LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part6.para3Part1}
                </span>
                <span style={styles.underline}>
                  {LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part6.para3Part2}
                </span>
                <span>
                  {LOCALIZE.emibTest.howToPage.testInstructions.step1Section.part6.para3Part3}
                </span>
              </p>
            </section>
          </section>
        </div>
        <div>
          <section aria-labelledby="adding-reasons-for-action">
            <h2 id="adding-reasons-for-action">
              {LOCALIZE.emibTest.howToPage.testInstructions.step2Section.title}
            </h2>
            <p>{LOCALIZE.emibTest.howToPage.testInstructions.step2Section.description}</p>
            <section aria-labelledby="example-of-response-with-reasons">
              <h3 id="example-of-response-with-reasons">
                {LOCALIZE.emibTest.howToPage.testInstructions.step2Section.part1.title}
              </h3>
              <div style={styles.disabledExampleComponent}>
                <ActionViewEmail
                  action={exampleEmailResponseWithReasons}
                  actionId={1}
                  email={exampleEmail}
                  disabled={true}
                  isInstructions={true}
                />
              </div>
            </section>
            <section aria-labelledby="example-of-task-with-reasons">
              <h3 id="example-of-task-with-reasons">
                {LOCALIZE.emibTest.howToPage.testInstructions.step2Section.part2.title}
              </h3>
              <div style={styles.disabledExampleComponent}>
                <ActionViewTask
                  action={exampleTaskResponseWithReasons}
                  actionId={1}
                  email={exampleEmail}
                  disabled={true}
                />
              </div>
            </section>
          </section>
        </div>
      </div>
    );
  }
}

export default TestInstructions;
