import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import ContentContainer from "../commons/ContentContainer";

class Confirmation extends Component {
  static props = {
    resetRedux: PropTypes.func
  };

  componentDidMount = () => {
    // focusing on your test has been submitted content section on page load
    document.getElementById("test-submission-content").focus();
  };
  componentWillUnmount = () => {
    this.props.resetRedux();
    this.props.logoutAction();
  };
  // TODO add survey link.
  render() {
    return (
      <ContentContainer>
        <div id="main-content" role="main">
          <section aria-labelledby="test-submission-content">
            <div id="test-submission-content" tabIndex={0}>
              <h1 className="green-divider">
                {LOCALIZE.emibTest.confirmationPage.submissionConfirmedTitle}
              </h1>
              <p style={{ fontSize: 24 }}>
                {LOCALIZE.formatString(
                  LOCALIZE.emibTest.confirmationPage.feedbackSurvey,
                  <a
                    href={LOCALIZE.emibTest.confirmationPage.surveyLink}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {LOCALIZE.emibTest.confirmationPage.optionalSurvey}
                  </a>
                )}
              </p>
              <p>
                {LOCALIZE.formatString(
                  LOCALIZE.emibTest.confirmationPage.logout,
                  <a href="mailto:cfp.cpp-ppc.psc@canada.ca">cfp.cpp-ppc.psc@canada.ca</a>
                )}
              </p>
              <p>{LOCALIZE.emibTest.confirmationPage.thankYou}</p>
            </div>
          </section>
        </div>
      </ContentContainer>
    );
  }
}

export { Confirmation as UnconnectedConfirmation };

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Confirmation);
