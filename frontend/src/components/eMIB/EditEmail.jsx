import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { EMAIL_TYPE, actionShape, EDIT_MODE } from "./constants";
import { optionsFromIds } from "../../helpers/transformations";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faReply, faReplyAll, faShareSquare } from "@fortawesome/free-solid-svg-icons";
import { OverlayTrigger, Popover, Button } from "react-bootstrap";
import Select from "react-select";
import "../../css/lib/react-super-select.css";
import { getAddressInCurrentLanguage } from "../../modules/LoadTestContentRedux";
import { addressBookOptionShape } from "./constants";
import "../../css/edit-email.css";

// These two consts limit the number of characters
// that can be entered into two text areas
// and are used to display <x>/<MAX>
// under the text areas
const MAX_RESPONSE = "3000";
const MAX_REASON = "650";

const styles = {
  header: {
    responseTypeIcons: {
      marginRight: 10,
      padding: 6,
      border: "1px solid #00565E",
      borderRadius: 4,
      cursor: "pointer",
      fontSize: 24
    },
    responseTypeIconsSelected: {
      backgroundColor: "#00565E",
      color: "white"
    },
    radioButtonZone: {
      marginBottom: 12
    },
    responseTypeRadio: {
      all: "unset",
      color: "#00565E",
      cursor: "pointer"
    },
    radioPadding: {
      marginBottom: 16
    },
    radioTextUnselected: {
      fontWeight: "normal",
      cursor: "pointer",
      paddingRight: 20,
      color: "#00565E"
    },
    radioTextSelected: {
      fontWeight: "bold",
      cursor: "pointer",
      paddingRight: 20,
      color: "#00565E"
    },
    fieldsetLegend: {
      fontSize: 16,
      marginBottom: 12,
      marginTop: 12,
      paddingTop: 12
    },
    titleStyle: {
      width: 36,
      height: 32,
      lineHeight: "2.1em",
      paddingRight: 4,
      marginTop: 5,
      marginBottom: 5
    },
    toAndCcFieldPadding: {
      marginBottom: 20
    }
  },
  invalidToFieldBorder: {
    border: "3px solid #923534"
  },
  invalidToFieldLabel: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  response: {
    textArea: {
      padding: "6px 12px",
      border: "1px solid #00565E",
      borderRadius: 4,
      width: "100%",
      height: 225,
      resize: "none"
    }
  },
  textCounter: {
    width: "100%",
    textAlign: "right",
    paddingRight: 12
  },
  reasonsForAction: {
    textArea: {
      padding: "6px 12px",
      border: "1px solid #00565E",
      borderRadius: 4,
      width: "100%",
      height: 150,
      resize: "none"
    }
  },
  charLimitReached: {
    marginLeft: 5,
    color: "#923534"
  },
  tooltipButton: {
    float: "right",
    textDecoration: "underline"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  popover: {
    zIndex: 9999,
    padding: "0 12px"
  }
};

function generateToIds(contactsString, addressBook) {
  return contactsString
    .split("; ")
    .map(name => {
      for (let i in addressBook) {
        let contact = addressBook[i];
        if (name === contact.label) {
          return contact.value;
        }
      }
      return "";
    })
    .filter(element => element !== "");
  // filter drops any empty strings
}

class EditEmail extends Component {
  constructor(props) {
    super(props);
    this.toFieldRef = React.createRef();
  }

  static propTypes = {
    onChange: PropTypes.func.isRequired,
    action: actionShape,
    originalFrom: PropTypes.string,
    originalTo: PropTypes.string,
    originalCC: PropTypes.string,
    editMode: PropTypes.oneOf(Object.keys(EDIT_MODE)).isRequired,
    toFieldValid: PropTypes.bool.isRequired,
    triggerPropsUpdate: PropTypes.bool,

    // Provided by redux
    addressBook: PropTypes.arrayOf(addressBookOptionShape)
  };

  componentDidMount() {
    // After generating the initial state, update the parent with it.
    // This allows defaults to be set.
    this.props.onChange(this.state);
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    // if toFieldValid props is updating
    if (prevProps.toFieldValid !== this.props.toFieldValid) {
      // focusing on 'To' field selection box
      this.toFieldRef.current.focus();
      // if triggerPropsUpdate props is updating
    } else if (prevProps.triggerPropsUpdate !== this.props.triggerPropsUpdate) {
      // focusing on 'To' field selection box
      this.toFieldRef.current.focus();
    }
  };

  // Generate an array of ids, representing contacts
  // based on a string of names in a to or from field.
  internalGenerateToIds = contactsString => {
    return generateToIds(contactsString, this.props.addressBook);
  };

  state = {
    emailType: !this.props.action ? EMAIL_TYPE.reply : this.props.action.emailType,
    emailBody: !this.props.action
      ? ""
      : !this.props.action.emailBody
      ? ""
      : this.props.action.emailBody,
    emailTo: !this.props.action
      ? this.props.editMode === EDIT_MODE.create
        ? this.internalGenerateToIds(this.props.originalFrom)
        : []
      : this.props.action.emailTo,
    emailToSelectedValues: [],
    emailCc: !this.props.action ? [] : this.props.action.emailCc,
    emailCcSelectedValues: [],
    reasonsForAction: !this.props.action
      ? ""
      : !this.props.action.reasonsForAction
      ? ""
      : this.props.action.reasonsForAction
  };

  onEmailTypeChange = event => {
    const newEmailType = event.target.value;

    // Names sent in the original email.
    const { originalTo, originalFrom, originalCC } = this.props;

    // By default (and forwarding) should have a blank to field.
    let replyList = [];
    let ccList = [];
    if (newEmailType === EMAIL_TYPE.reply) {
      // Reply to the person that sent you this email.
      replyList = this.internalGenerateToIds(originalFrom);
    } else if (newEmailType === EMAIL_TYPE.replyAll) {
      // Reply all to everyone this email was from and sent to.
      const toList = this.internalGenerateToIds(originalTo);
      const fromList = this.internalGenerateToIds(originalFrom);
      ccList = this.internalGenerateToIds(originalCC);
      replyList = toList.concat(fromList);
    }

    this.setState({ emailType: newEmailType, emailTo: replyList, emailCc: ccList });
    this.props.onChange({
      ...this.state,
      emailType: newEmailType,
      emailTo: replyList,
      emailCc: ccList
    });
  };

  onEmailToChange = options => {
    // Convert options to an array of indexes
    options = options || [];
    const idsArray = options.map(option => {
      return option.value;
    });
    this.setState({ emailTo: idsArray });
    this.props.onChange({ ...this.state, emailTo: idsArray });
  };

  onEmailCcChange = options => {
    // onvert options to an array of indexes
    options = options || [];
    const idsArray = options.map(option => {
      return option.value;
    });
    this.setState({ emailCc: idsArray });
    this.props.onChange({ ...this.state, emailCc: idsArray });
  };

  onEmailBodyChange = event => {
    const newEmailBody = event.target.value;
    this.setState({ emailBody: newEmailBody });
    this.props.onChange({ ...this.state, emailBody: newEmailBody });
  };

  onReasonsForActionChange = event => {
    const newreasonsForAction = event.target.value;
    this.setState({ reasonsForAction: newreasonsForAction });
    this.props.onChange({ ...this.state, reasonsForAction: newreasonsForAction });
  };

  // getting the selected values from To field + updating the needed state using them
  getToFieldSelectedValues = emailTo => {
    let values = [];
    for (let i = 0; i < emailTo.length; i++) {
      values.push(emailTo[i].label);
    }
    this.setState({ emailToSelectedValues: values });
  };

  // getting the selected values from Cc field + updating the needed state using them
  getCcFieldSelectedValues = emailCc => {
    let values = [];
    for (let i = 0; i < emailCc.length; i++) {
      values.push(emailCc[i].label);
    }
    this.setState({ emailCcSelectedValues: values });
  };

  render() {
    let { emailTo, emailCc, emailBody, reasonsForAction } = this.state;
    const replyChecked = this.state.emailType === EMAIL_TYPE.reply;
    const replyAllChecked = this.state.emailType === EMAIL_TYPE.replyAll;
    const forwardChecked = this.state.emailType === EMAIL_TYPE.forward;

    // Get localized to/cc options from the address book.
    const options = this.props.addressBook;

    // Convert emailTo and emailCC from array of indexes to options.
    emailTo = optionsFromIds(this.props.addressBook, emailTo);
    emailCc = optionsFromIds(this.props.addressBook, emailCc);
    // These two constants are used by 2 seperate labels:
    // 1 is a popover (which does not exist until clicked on;
    // thus cannot be used for aria-labelled-by)
    // 2 is a visual hidden label which aria-labelled by can use
    const yourResponseTooltipText =
      LOCALIZE.emibTest.inboxPage.addEmailResponse.emailResponseTooltip;
    const reasonsTooltipText = LOCALIZE.emibTest.inboxPage.addEmailResponse.reasonsForActionTooltip;
    return (
      <div style={styles.container}>
        <form>
          <div>
            <fieldset>
              <legend className="font-weight-bold" style={styles.header.fieldsetLegend}>
                {LOCALIZE.emibTest.inboxPage.addEmailResponse.selectResponseType}
              </legend>
              <div style={styles.header.radioButtonZone} className="radio-button-hover">
                <input
                  id="reply-radio"
                  type="radio"
                  name="responseTypeRadio"
                  style={{ ...styles.header.radioPadding, ...styles.header.responseTypeRadio }}
                  onChange={this.onEmailTypeChange}
                  value={EMAIL_TYPE.reply}
                  checked={replyChecked}
                  className="visually-hidden"
                />
                <label
                  htmlFor="reply-radio"
                  style={
                    replyChecked
                      ? styles.header.radioTextSelected
                      : styles.header.radioTextUnselected
                  }
                >
                  <FontAwesomeIcon
                    icon={faReply}
                    style={{
                      ...styles.header.responseTypeIcons,
                      ...(replyChecked ? styles.header.responseTypeIconsSelected : {})
                    }}
                  />
                  {LOCALIZE.emibTest.inboxPage.emailCommons.reply}
                </label>
                <input
                  id="reply-all-radio"
                  type="radio"
                  name="responseTypeRadio"
                  style={{ ...styles.header.radioPadding, ...styles.header.responseTypeRadio }}
                  onChange={this.onEmailTypeChange}
                  value={EMAIL_TYPE.replyAll}
                  checked={replyAllChecked}
                  className="visually-hidden"
                />
                <label
                  htmlFor="reply-all-radio"
                  style={
                    replyAllChecked
                      ? styles.header.radioTextSelected
                      : styles.header.radioTextUnselected
                  }
                >
                  <FontAwesomeIcon
                    icon={faReplyAll}
                    style={{
                      ...styles.header.responseTypeIcons,
                      ...(replyAllChecked ? styles.header.responseTypeIconsSelected : {})
                    }}
                  />
                  {LOCALIZE.emibTest.inboxPage.emailCommons.replyAll}
                </label>
                <input
                  id="forward-radio"
                  type="radio"
                  name="responseTypeRadio"
                  style={{ ...styles.header.radioPadding, ...styles.header.responseTypeRadio }}
                  onChange={this.onEmailTypeChange}
                  value={EMAIL_TYPE.forward}
                  checked={forwardChecked}
                  className="visually-hidden"
                />
                <label
                  htmlFor="forward-radio"
                  style={
                    forwardChecked
                      ? styles.header.radioTextSelected
                      : styles.header.radioTextUnselected
                  }
                >
                  <FontAwesomeIcon
                    icon={faShareSquare}
                    style={{
                      ...styles.header.responseTypeIcons,
                      ...(forwardChecked ? styles.header.responseTypeIconsSelected : {})
                    }}
                  />
                  {LOCALIZE.emibTest.inboxPage.emailCommons.forward}
                </label>
              </div>
            </fieldset>
          </div>
          <div className="font-weight-bold" style={styles.header.toAndCcFieldPadding}>
            <label style={styles.header.titleStyle}>
              {LOCALIZE.emibTest.inboxPage.emailCommons.to}
            </label>
            <label id="to-field-selected" style={styles.hiddenText}>
              {LOCALIZE.emibTest.inboxPage.emailCommons.toFieldSelected}{" "}
              {this.state.emailToSelectedValues.length === 0
                ? `${LOCALIZE.formatString(
                    LOCALIZE.emibTest.inboxPage.emailCommons.currentSelectedPeople,
                    LOCALIZE.emibTest.inboxPage.emailCommons.currentSelectedPeopleAreNone
                  )}.`
                : `${LOCALIZE.formatString(
                    LOCALIZE.emibTest.inboxPage.emailCommons.currentSelectedPeople,
                    this.state.emailToSelectedValues
                  )}.`}
            </label>
            <label id="to-field-placeholder" style={styles.hiddenText}>
              {LOCALIZE.emibTest.inboxPage.emailCommons.toAndCcFieldsPlaceholder}
            </label>
            <Select
              ref={this.toFieldRef}
              className={!this.props.toFieldValid ? "invalid-to-field" : ""}
              aria-labelledby="to-field-selected invalid-to-field to-field-placeholder"
              placeholder={LOCALIZE.emibTest.inboxPage.emailCommons.toAndCcFieldsPlaceholder}
              value={emailTo}
              onChange={this.onEmailToChange}
              options={options}
              isMulti="true"
              onFocus={() => this.getToFieldSelectedValues(emailTo)}
            />
            {!this.props.toFieldValid && (
              <label id="invalid-to-field" style={styles.invalidToFieldLabel}>
                {LOCALIZE.emibTest.inboxPage.addEmailResponse.invalidToFieldError}
              </label>
            )}
          </div>
          <div className="font-weight-bold" style={styles.header.toAndCcFieldPadding}>
            <label style={styles.header.titleStyle}>
              {LOCALIZE.emibTest.inboxPage.emailCommons.cc}
            </label>
            <label id="cc-field-selected" style={styles.hiddenText}>
              {LOCALIZE.emibTest.inboxPage.emailCommons.ccFieldSelected}{" "}
              {this.state.emailCcSelectedValues.length === 0
                ? `${LOCALIZE.formatString(
                    LOCALIZE.emibTest.inboxPage.emailCommons.currentSelectedPeople,
                    LOCALIZE.emibTest.inboxPage.emailCommons.currentSelectedPeopleAreNone
                  )}.`
                : `${LOCALIZE.formatString(
                    LOCALIZE.emibTest.inboxPage.emailCommons.currentSelectedPeople,
                    this.state.emailCcSelectedValues
                  )}.`}
            </label>
            <label id="cc-field-placeholder" style={styles.hiddenText}>
              {LOCALIZE.emibTest.inboxPage.emailCommons.toAndCcFieldsPlaceholder}
            </label>
            <Select
              aria-labelledby="cc-field-selected cc-field-placeholder"
              placeholder={LOCALIZE.emibTest.inboxPage.emailCommons.toAndCcFieldsPlaceholder}
              value={emailCc}
              onChange={this.onEmailCcChange}
              options={options}
              isMulti="true"
              onFocus={() => this.getCcFieldSelectedValues(emailCc)}
            />
          </div>
          <div>
            <div className="font-weight-bold form-group">
              <label id="your-response-text-label">
                <p>
                  {LOCALIZE.formatString(
                    LOCALIZE.emibTest.inboxPage.addEmailResponse.response,
                    MAX_RESPONSE
                  )}
                </p>
                {this.state.emailBody.length >= MAX_RESPONSE && (
                  <p style={styles.charLimitReached} aria-live="assertive" role="alert">
                    {LOCALIZE.emibTest.inboxPage.characterLimitReached}
                  </p>
                )}
              </label>
              <OverlayTrigger
                trigger="focus"
                placement="right"
                overlay={
                  <Popover style={styles.popover}>
                    <div>
                      <p>{yourResponseTooltipText}</p>
                    </div>
                  </Popover>
                }
              >
                <Button
                  tabIndex="-1"
                  aria-label={LOCALIZE.ariaLabel.emailResponseTooltip}
                  style={styles.tooltipButton}
                  variant="link"
                >
                  ?
                </Button>
              </OverlayTrigger>
              <div>
                <label className="visually-hidden" id="your-response-tooltip-text">
                  {yourResponseTooltipText}
                </label>
                <textarea
                  id="your-response-text-area"
                  maxLength={MAX_RESPONSE}
                  aria-labelledby="your-response-text-label your-response-tooltip-text"
                  style={styles.response.textArea}
                  value={emailBody}
                  onChange={this.onEmailBodyChange}
                />
              </div>
              <div style={styles.textCounter}>
                {this.state.emailBody === undefined ? 0 : this.state.emailBody.length}/
                {MAX_RESPONSE}
              </div>
            </div>
          </div>
          <div>
            <div className="font-weight-bold form-group">
              <label id="reasons-for-action-text-label">
                <p>
                  {LOCALIZE.formatString(
                    LOCALIZE.emibTest.inboxPage.addEmailResponse.reasonsForAction,
                    MAX_REASON
                  )}
                </p>
                {this.state.reasonsForAction.length >= MAX_REASON && (
                  <p style={styles.charLimitReached} aria-live="assertive" role="alert">
                    {LOCALIZE.formatString(
                      LOCALIZE.emibTest.inboxPage.characterLimitReached,
                      MAX_REASON
                    )}
                  </p>
                )}
              </label>
              <OverlayTrigger
                trigger="focus"
                placement="right"
                overlay={
                  <Popover style={styles.popover}>
                    <div>
                      <p>{reasonsTooltipText}</p>
                    </div>
                  </Popover>
                }
              >
                <Button
                  tabIndex="-1"
                  aria-label={LOCALIZE.ariaLabel.reasonsForActionTooltip}
                  style={styles.tooltipButton}
                  variant="link"
                >
                  ?
                </Button>
              </OverlayTrigger>
              <div>
                <label className="visually-hidden" id="reasons-for-action-tooltip-text">
                  {reasonsTooltipText}
                </label>
                <textarea
                  id="reasons-for-action-text-area"
                  maxLength={MAX_REASON}
                  aria-labelledby="reasons-for-action-text-label reasons-for-action-tooltip-text"
                  style={styles.reasonsForAction.textArea}
                  value={reasonsForAction}
                  onChange={this.onReasonsForActionChange}
                />
              </div>
              <div style={styles.textCounter}>
                {this.state.reasonsForAction === undefined ? 0 : this.state.reasonsForAction.length}
                /{MAX_REASON}
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export { EditEmail as UnconnectedEditEmail, generateToIds };

const mapStateToProps = (state, ownProps) => {
  return {
    addressBook: getAddressInCurrentLanguage(state)
  };
};

export default connect(
  mapStateToProps,
  null
)(EditEmail);
