import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import TipsOnTest from "./TipsOnTest";
import PropTypes from "prop-types";
import TestInstructions from "./TestInstructions";
import Evaluation from "./Evaluation";
import SideNavigation from "./SideNavigation";

const styles = {
  tabContainer: {
    overflowY: "scroll",
    zIndex: 1
  },
  nav: {
    marginTop: 10,
    marginLeft: 10
  }
};

class InTestInstructions extends Component {
  static propTypes = {
    isMain: PropTypes.bool,
    headerFooterPX: PropTypes.number
  };

  //Returns array where each item indicates specifications related to How To Page including the title and the body
  getInstructionContent = () => {
    return [
      {
        menuString: LOCALIZE.emibTest.howToPage.testInstructions.title,
        body: <TestInstructions headerFooterPX={this.props.headerFooterPX} />
      },
      { menuString: LOCALIZE.emibTest.howToPage.tipsOnTest.title, body: <TipsOnTest /> },
      { menuString: LOCALIZE.emibTest.howToPage.evaluation.title, body: <Evaluation /> }
    ];
  };

  render() {
    const specs = this.getInstructionContent();
    return (
      <SideNavigation
        specs={specs}
        startIndex={0}
        parentTabName={LOCALIZE.emibTest.howToPage.testInstructions.hiddenTabNameComplementary}
        displayNextPreviousButton={true}
        tabContainerStyle={styles.tabContainer}
        navStyle={styles.nav}
        isMain={this.props.isMain}
        headerFooterPX={this.props.headerFooterPX}
      />
    );
  }
}

export default InTestInstructions;
