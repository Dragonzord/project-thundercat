import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import EmailPreview from "./EmailPreview";
import Email from "./Email";
import "../../css/inbox.css";
import { emailShape } from "./constants";
import { readEmail, changeCurrentEmail } from "../../modules/EmibInboxRedux";
import { Tab, Row, Col, Nav } from "react-bootstrap";
import LOCALIZE from "../../text_resources";

const EVENT_KEYS = [
  "first",
  "second",
  "third",
  "fourth",
  "fifth",
  "sixth",
  "seventh",
  "eigth",
  "nineth",
  "tenth"
];

class Inbox extends Component {
  static propTypes = {
    isMain: PropTypes.bool,
    headerFooterPX: PropTypes.number,
    // Provided by redux
    emails: PropTypes.arrayOf(emailShape),
    emailSummaries: PropTypes.array.isRequired,
    currentEmail: PropTypes.number.isRequired,
    readEmail: PropTypes.func.isRequired,
    changeCurrentEmail: PropTypes.func.isRequired
  };

  INBOX_HEIGHT = `calc(100vh - ${this.props.headerFooterPX}px)`;

  styles = {
    rowContainer: {
      margin: 0
    },
    bodyContent: {
      height: this.INBOX_HEIGHT
    },
    contentColumn: {
      paddingLeft: 0,
      overflowY: "scroll",
      zIndex: 1
    },
    emailContainer: {
      overflow: "hidden",
      paddingLeft: 0
    },
    navIntemContainer: {
      width: "100%",
      height: this.INBOX_HEIGHT,
      overflow: "auto"
    },
    navItem: {
      width: "100%"
    },
    navLink: {
      padding: 0
    },
    hiddenText: {
      position: "absolute",
      left: -10000,
      top: "auto",
      width: 1,
      height: 1,
      overflow: "hidden"
    }
  };

  changeEmail = eventKey => {
    const index = EVENT_KEYS.indexOf(eventKey);
    this.props.readEmail(this.props.currentEmail);
    this.props.changeCurrentEmail(index);

    //scroll email to top
    let main = document.getElementById("main-content");
    let content = document.getElementById("email-content");
    if (main !== null) {
      main.scrollTop = 0;
    }
    if (content !== null) {
      content.scrollTop = 0;
    }
  };

  render() {
    const { emails, emailSummaries, isMain } = this.props;
    return (
      <div>
        <Tab.Container
          id="inbox-tabs"
          defaultActiveKey={EVENT_KEYS[this.props.currentEmail]}
          onSelect={this.changeEmail}
        >
          <Row style={this.styles.rowContainer}>
            <Col
              role="region"
              aria-label={LOCALIZE.ariaLabel.emailsList}
              sm={4}
              style={this.styles.emailContainer}
            >
              <Nav role="navigation" className="flex-column">
                <div style={this.styles.navIntemContainer}>
                  {emails.map((email, index) => (
                    <Nav.Item key={index} style={this.styles.navItem}>
                      <Nav.Link role="tab" eventKey={EVENT_KEYS[index]} style={this.styles.navLink}>
                        <EmailPreview
                          email={email}
                          isRead={this.props.emailSummaries[index].isRead}
                          isRepliedTo={
                            emailSummaries[index].emailCount + emailSummaries[index].taskCount > 0
                          }
                          isSelected={index === this.props.currentEmail}
                        />
                        <span style={this.styles.hiddenText}>
                          {LOCALIZE.emibTest.inboxPage.tabName}
                        </span>
                      </Nav.Link>
                    </Nav.Item>
                  ))}
                </div>
              </Nav>
            </Col>
            <Col
              id={isMain ? "main-content" : "email-content"}
              role="main"
              sm={8}
              tabIndex={0}
              style={this.styles.contentColumn}
            >
              <Tab.Content style={this.styles.bodyContent}>
                {emails.map((email, index) => (
                  <Tab.Pane eventKey={EVENT_KEYS[index]} key={index}>
                    <Email
                      email={email}
                      emailCount={emailSummaries[this.props.currentEmail].emailCount}
                      taskCount={emailSummaries[this.props.currentEmail].taskCount}
                    />
                  </Tab.Pane>
                ))}
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
    );
  }
}

export { Inbox as UnconnectedInbox };
const mapStateToProps = (state, ownProps) => {
  return {
    emails: state.emibInbox.emails,
    emailSummaries: state.emibInbox.emailSummaries,
    currentEmail: state.emibInbox.currentEmail
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      readEmail,
      changeCurrentEmail
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Inbox);
