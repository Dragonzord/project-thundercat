import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import ContentContainer from "../commons/ContentContainer";
import { PATH } from "../commons/Constants";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setCurrentTest } from "../../modules/TestStatusRedux";
import { Button } from "react-bootstrap";
import { TEST_TABLE_STYLES as styles } from "../eMIB/AssignedTestTable";
import { Helmet } from "react-helmet";
import { resetInboxState } from "../../modules/EmibInboxRedux";
import { resetMetaDataState } from "../../modules/LoadTestContentRedux";
import { resetTestStatusState } from "../../modules/SampleTestStatusRedux";
import { resetNotepadState } from "../../modules/NotepadRedux";

class SampleTestDashboard extends Component {
  static propTypes = {
    testNameId: PropTypes.string,
    setCurrentTest: PropTypes.func
  };

  componentDidMount = () => {
    this.resetRedux();
  };

  resetRedux = () => {
    this.props.resetInboxState();
    this.props.resetMetaDataState();
    this.props.resetTestStatusState();
    this.props.resetNotepadState();
  };

  handleClick = () => {
    this.props.history.push(this.props.match.url + PATH.emibSampleTest + PATH.overview);
  };

  render() {
    return (
      <div>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.sampleTests}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <h1 className="green-divider">{LOCALIZE.sampleTestDashboard.title}</h1>
            <p>{LOCALIZE.sampleTestDashboard.description}</p>
          </div>

          <table id="test-assignment-table" style={styles.table}>
            <tbody>
              <tr>
                <th style={{ ...styles.th, ...styles.borderRight, ...styles.topLeftBorderRadius }}>
                  {LOCALIZE.dashboard.table.columnOne}
                </th>
                <th style={{ ...styles.th, ...styles.topRightBorderRadius }}>
                  {LOCALIZE.dashboard.table.columnThree}
                </th>
              </tr>
              {/* can add one of these for each sample test, or can implement a api call to get sample tests*/}
              <tr>
                <td style={{ ...styles.td, ...styles.borderRight }}>
                  {LOCALIZE.mainTabs.sampleTest}
                </td>
                <td style={{ ...styles.td, ...styles.centerText }}>
                  <Button
                    onClick={this.handleClick}
                    className="btn btn-primary"
                    style={styles.viewButton}
                  >
                    <p>{LOCALIZE.sampleTestDashboard.table.viewButton}</p>
                  </Button>
                </td>
              </tr>
            </tbody>
          </table>
        </ContentContainer>
      </div>
    );
  }
}

export { SampleTestDashboard as UnconnectedSampleTestDashboard };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setCurrentTest,
      resetTestStatusState,
      resetInboxState,
      resetMetaDataState,
      resetNotepadState
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(SampleTestDashboard));
