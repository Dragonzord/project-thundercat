import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "../../css/personal-info.css";
import Select from "react-select";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { faSave } from "@fortawesome/free-solid-svg-icons";
import { OverlayTrigger, Popover, Button } from "react-bootstrap";
import { updateUserPersonalInfo } from "../../modules/UserProfileRedux";
import { validatePriOrMilitaryNbr, validateEmail } from "../../helpers/regexValidator";
import { setUserInformation } from "../../modules/UserRedux";

const styles = {
  mainContent: {
    padding: "12px 0 0 24px"
  },
  input: {
    width: 270,
    height: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  label: {
    padding: "3px 0 0 4px"
  },
  optionalLabel: {
    paddingLeft: 4
  },
  dateOfBirthContainer: {
    display: "flex"
  },
  yearField: {
    width: 95,
    marginRight: 36
  },
  monthAndDayField: {
    width: 75,
    marginRight: 36
  },
  titleFieldLabel: {
    margin: 0
  },
  priContainer: {
    display: "inline-block"
  },
  floatLeft: {
    float: "left"
  },
  floatRight: {
    float: "right"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  popover: {
    zIndex: 9999,
    padding: "0 12px",
    margin: 0
  },
  tooltipIconContainer: {
    padding: 0
  },
  tooltipMarginTop: {
    marginTop: 1
  },
  tooltipIcon: {
    height: 36,
    width: 40,
    color: "#00565e",
    padding: 6
  },
  secondaryEmailTooltipIcon: {
    height: "23.25px",
    width: "23.25px",
    color: "#00565e",
    margin: "0 6px"
  },
  updateButtonContainer: {
    float: "right"
  },
  updateButton: {
    marginRight: 30,
    minWidth: 100
  },
  updateButtonConfirmationMsg: {
    float: "left",
    marginRight: 24,
    color: "#278400"
  },
  buttonIcon: {
    transform: "scale(2)",
    padding: "1.5px"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  noMargin: {
    margin: 0
  }
};

class PersonalInfo extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    //provided by redux
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    primaryEmail: PropTypes.string.isRequired,
    secondaryEmail: PropTypes.string,
    dateOfBirth: PropTypes.string.isRequired,
    priOrMilitaryNbr: PropTypes.string,
    updateUserPersonalInfo: PropTypes.func,
    setUserInformation: PropTypes.func
  };

  state = {
    firstNameContent: this.props.firstName,
    lastNameContent: this.props.lastName,
    primaryEmailContent: this.props.primaryEmail,
    secondaryEmailContent: this.props.secondaryEmail === null ? "" : this.props.secondaryEmail,
    isValidSecondaryEmail: true,
    dateOfBirthYearOptions: [],
    dateOfBirthYearSelectedValue: {
      value: parseInt(this.props.dateOfBirth.split("-")[0]),
      label: `${this.props.dateOfBirth.split("-")[0]}`
    },
    dateOfBirthMonthOptions: [],
    dateOfBirthMonthSelectedValue: {
      value: parseInt(this.props.dateOfBirth.split("-")[1]),
      label: `${this.props.dateOfBirth.split("-")[1]}`
    },
    dateOfBirthDayOptions: [],
    dateOfBirthDaySelectedValue: {
      value: parseInt(this.props.dateOfBirth.split("-")[2]),
      label: `${this.props.dateOfBirth.split("-")[2]}`
    },
    priOrMilitaryNbrContent: this.props.priOrMilitaryNbr,
    isValidPriOrMilitaryNbr: true,
    displayDataUpdatedConfirmationMsg: false
  };

  componentDidMount = () => {
    this._isMounted = true;
    this.populateDateOfBirthYearOptions();
    this.populateDateOfBirthMonthOptions();
    this.populateDateOfBirthDayOptions();
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  componentDidUpdate = prevProps => {
    // update first name field based on the prop value
    if (prevProps.firstName !== this.props.firstName) {
      this.setState({ firstNameContent: this.props.firstName });
    }
    // update last name field based on the prop value
    if (prevProps.lastName !== this.props.lastName) {
      this.setState({ lastNameContent: this.props.lastName });
    }
    // update primary email field based on the prop value
    if (prevProps.primaryEmail !== this.props.primaryEmail) {
      this.setState({ primaryEmailContent: this.props.primaryEmail });
    }
    // update secondary email field based on the prop value
    if (prevProps.secondaryEmail !== this.props.secondaryEmail) {
      this.setState({
        secondaryEmailContent: this.props.secondaryEmail === null ? "" : this.props.secondaryEmail
      });
    }
    // update date of birth selected value field based on the prop value
    if (prevProps.dateOfBirth !== this.props.dateOfBirth) {
      this.setState({
        // year
        dateOfBirthYearSelectedValue: {
          value: parseInt(this.props.dateOfBirth.split("-")[0]),
          label: `${this.props.dateOfBirth.split("-")[0]}`
        },
        // month
        dateOfBirthMonthSelectedValue: {
          value: parseInt(this.props.dateOfBirth.split("-")[1]),
          label: `${this.props.dateOfBirth.split("-")[1]}`
        },
        // day
        dateOfBirthDaySelectedValue: {
          value: parseInt(this.props.dateOfBirth.split("-")[2]),
          label: `${this.props.dateOfBirth.split("-")[2]}`
        }
      });
    }
    // update pri or military number field based on the prop value
    if (prevProps.priOrMilitaryNbr !== this.props.priOrMilitaryNbr) {
      this.setState({ priOrMilitaryNbrContent: this.props.priOrMilitaryNbr });
    }
  };

  // populate year (DOB) from 1900 to current year
  populateDateOfBirthYearOptions = () => {
    let yearOptionsArray = [];
    // loop from 1900 to current year
    for (let i = 1900; i <= new Date().getFullYear(); i++) {
      // push each value in yearOptionsArray
      yearOptionsArray.push({ value: i, label: `${i}` });
    }
    // sorting (descending order)
    yearOptionsArray.reverse();
    // saving result in state
    if (this._isMounted) {
      this.setState({ dateOfBirthYearOptions: yearOptionsArray });
    }
  };

  // populate month (DOB)
  populateDateOfBirthMonthOptions = () => {
    let monthOptionsArray = [];
    // loop from 1 to 12
    for (let i = 1; i <= 12; i++) {
      // push each value in monthOptionsArray
      monthOptionsArray.push({ value: i, label: `${i}` });
    }
    // saving result in state
    if (this._isMounted) {
      this.setState({ dateOfBirthMonthOptions: monthOptionsArray });
    }
  };

  // populate day (DOB)
  populateDateOfBirthDayOptions = () => {
    let dayOptionsArray = [];
    // loop from 1 to 31
    for (let i = 1; i <= 31; i++) {
      // push each value in dayOptionsArray
      dayOptionsArray.push({ value: i, label: `${i}` });
    }
    // saving result in state
    if (this._isMounted) {
      this.setState({ dateOfBirthDayOptions: dayOptionsArray });
    }
  };

  // update first name value
  updateFirstNameValue = event => {
    const firstNameContent = event.target.value;
    this.setState({
      firstNameContent: firstNameContent
    });
  };

  // update last name value
  updateLastNameValue = event => {
    const lastNameContent = event.target.value;
    this.setState({
      lastNameContent: lastNameContent
    });
  };

  // update primary email value
  updatePrimaryEmailValue = event => {
    const primaryEmailContent = event.target.value;
    this.setState({
      primaryEmailContent: primaryEmailContent
    });
  };

  // update secondary email value
  updateSecondaryEmailValue = event => {
    const secondaryEmailContent = event.target.value;
    this.setState({
      secondaryEmailContent: secondaryEmailContent,
      // hiding data updated confirmation message on field changes
      displayDataUpdatedConfirmationMsg: false
    });
  };

  // get selected year (DOB)
  getSelectedDateOfBirthYear = selectedOption => {
    this.setState({ dateOfBirthYearSelectedValue: selectedOption });
  };

  // get selected month (DOB)
  getSelectedDateOfBirthMonth = selectedOption => {
    this.setState({ dateOfBirthMonthSelectedValue: selectedOption });
  };

  // get selected day (DOB)
  getSelectedDateOfBirthDay = selectedOption => {
    this.setState({ dateOfBirthDaySelectedValue: selectedOption });
  };

  // update pri or military number value
  updatePriOrMilitaryNbrValue = event => {
    const priOrMilitaryNbrContent = event.target.value;
    /* only the following can be inserted into this field:
          - 1 letter followed by 0 to 6 numbers
          - 0 to 9 numbers
    */
    const regex = /^(([A-Za-z]{1})([0-9]{0,6}))$|^([0-9]{0,9})$/;
    if (event.target.value === "" || regex.test(event.target.value)) {
      this.setState({
        priOrMilitaryNbrContent: priOrMilitaryNbrContent,
        // hiding data updated confirmation message on field changes
        displayDataUpdatedConfirmationMsg: false
      });
    }
  };

  // save user personal info in the database
  handleSaveData = () => {
    // data is valid
    if (this.validatePersonalInfoData()) {
      const token = localStorage.getItem("auth_token");
      let data = {
        firstName: this.state.firstNameContent,
        lastName: this.state.lastNameContent,
        username: this.state.primaryEmailContent, //should be username if we're adding username field
        primaryEmail: this.state.primaryEmailContent,
        secondaryEmail: this.state.secondaryEmailContent,
        dob: `${this.state.dateOfBirthYearSelectedValue.label}-${this.state.dateOfBirthMonthSelectedValue.label}-${this.state.dateOfBirthDaySelectedValue.label}`,
        priOrMilitaryNbr: this.state.priOrMilitaryNbrContent.toUpperCase()
      };
      this.props.setUserInformation(
        data.firstName,
        data.lastName,
        data.username,
        this.props.isSuperUser,
        this.props.lastPasswordChange, // this might not be right....
        data.primaryEmail,
        data.secondaryEmail,
        data.dob,
        data.priOrMilitaryNbr
      );
      this.props.updateUserPersonalInfo(token, data).then(response => {
        // request succeeded
        if (response === 200) {
          this.setState({ displayDataUpdatedConfirmationMsg: true });
          // should never happen
        } else {
          throw new Error("An error has occurred during the data update");
        }
      });
    }
  };

  // validate personal info data that is editable only (needs to be done before sending data in the database)
  validatePersonalInfoData = () => {
    // validating PRI or Military number field
    const isValidPriOrMilitaryNbr = validatePriOrMilitaryNbr(this.state.priOrMilitaryNbrContent);

    // validating secondary email field
    let isValidSecondaryEmail = false;
    // if secondary email string is not empty, validate it using the regex validator
    if (this.state.secondaryEmailContent !== "") {
      isValidSecondaryEmail = validateEmail(this.state.secondaryEmailContent);
      // else the field is empty, so valid since this is not a mandatory field
    } else {
      isValidSecondaryEmail = true;
    }

    // updating fields state
    this.setState(
      {
        isValidPriOrMilitaryNbr: isValidPriOrMilitaryNbr,
        isValidSecondaryEmail: isValidSecondaryEmail
      },
      () => {
        // focusing on highest invalid field
        this.focusOnHighestInvalidField();
      }
    );

    // making sure that all needed fields are valid
    if (isValidPriOrMilitaryNbr && isValidSecondaryEmail) {
      // whole data is valid
      return true;
      // there is at least one invalid field
    } else {
      // hide data updated confirmation message
      this.setState({ displayDataUpdatedConfirmationMsg: false });
      return false;
    }
  };

  // focus on highest invalid field
  focusOnHighestInvalidField = () => {
    if (!this.state.isValidSecondaryEmail) {
      document.getElementById("secondary-email-input").focus();
    } else if (!this.state.isValidPriOrMilitaryNbr) {
      document.getElementById("pri-or-military-number-input").focus();
    }
  };

  render() {
    const {
      firstNameContent,
      lastNameContent,
      primaryEmailContent,
      secondaryEmailContent,
      isValidSecondaryEmail,
      //TODO (fnormand): depending on the decision of dob editable or not, delete/uncomment all date of birth options states
      // dateOfBirthYearOptions,
      dateOfBirthYearSelectedValue,
      // dateOfBirthMonthOptions,
      dateOfBirthMonthSelectedValue,
      // dateOfBirthDayOptions,
      dateOfBirthDaySelectedValue,
      priOrMilitaryNbrContent,
      isValidPriOrMilitaryNbr,
      displayDataUpdatedConfirmationMsg
    } = this.state;
    return (
      <div>
        <h2>{LOCALIZE.profile.personalInfo.title}</h2>
        <div style={styles.mainContent}>
          <div id="personal-info-names" className="profile-peronal-info-grid">
            <div id="name-title" className="profile-peronal-info-first-column">
              <p>{LOCALIZE.profile.personalInfo.nameSection.title}</p>
            </div>
            <div className="profile-peronal-info-second-column">
              <input
                id="first-name-input"
                disabled={true}
                tabIndex={0}
                className="valid-field"
                aria-labelledby="name-title first-name"
                aria-required={true}
                style={styles.input}
                type="text"
                value={firstNameContent}
                onChange={this.updateFirstNameValue}
              ></input>
              <label id="first-name" htmlFor="first-name-input" style={styles.label}>
                {LOCALIZE.profile.personalInfo.nameSection.firstName}
              </label>
            </div>
            <div className="profile-peronal-info-third-column">
              <input
                id="last-name-input"
                disabled={true}
                className="valid-field"
                aria-labelledby="name-title last-name"
                aria-required={true}
                style={styles.input}
                type="text"
                value={lastNameContent}
                onChange={this.updateLastNameValue}
              ></input>
              <label id="last-name" htmlFor="last-name-input" style={styles.label}>
                {LOCALIZE.profile.personalInfo.nameSection.lastName}
              </label>
            </div>
          </div>
          <div id="personal-info-emails" className="profile-peronal-info-grid">
            <div id="email-title" className="profile-peronal-info-first-column">
              <p>{LOCALIZE.profile.personalInfo.emailAddressesSection.title}</p>
            </div>
            <div className="profile-peronal-info-second-column">
              <input
                id="primary-email-input"
                disabled={true}
                className="valid-field"
                aria-labelledby="email-title primary-email"
                aria-required={true}
                style={styles.input}
                type="text"
                value={primaryEmailContent}
                onChange={this.updatePrimaryEmailValue}
              ></input>
              <label id="primary-email" htmlFor="primary-email-input" style={styles.label}>
                {LOCALIZE.profile.personalInfo.emailAddressesSection.primary}
              </label>
            </div>
            <div className="profile-peronal-info-third-column">
              <input
                id="secondary-email-input"
                className={isValidSecondaryEmail ? "valid-field" : "invalid-field"}
                aria-labelledby="email-title secondary-email secondary-email-tooltip-for-accessibility invalid-secondary-email-msg"
                aria-required={false}
                aria-invalid={!isValidSecondaryEmail}
                style={styles.input}
                type="text"
                value={secondaryEmailContent}
                onChange={this.updateSecondaryEmailValue}
              ></input>
              <label id="secondary-email" htmlFor="secondary-email-input" style={styles.label}>
                {LOCALIZE.profile.personalInfo.emailAddressesSection.secondary}
              </label>
              <label htmlFor="secondary-email-input" style={styles.optionalLabel}>
                {LOCALIZE.profile.personalInfo.optionalField}
              </label>
              <label id="secondary-email-tooltip-for-accessibility" style={styles.hiddenText}>
                , {LOCALIZE.profile.personalInfo.emailAddressesSection.secondaryTooltip}
              </label>
              <OverlayTrigger
                trigger="focus"
                placement="bottom"
                overlay={
                  <Popover style={styles.popover}>
                    <div>
                      <p>{LOCALIZE.profile.personalInfo.emailAddressesSection.secondaryTooltip}</p>
                    </div>
                  </Popover>
                }
              >
                <Button tabIndex="-1" variant="link" style={styles.tooltipIconContainer}>
                  <FontAwesomeIcon
                    icon={faQuestionCircle}
                    style={styles.secondaryEmailTooltipIcon}
                  ></FontAwesomeIcon>
                </Button>
              </OverlayTrigger>
              {!isValidSecondaryEmail && (
                <label
                  id="invalid-secondary-email-msg"
                  style={{ ...styles.errorMessage, ...styles.noMargin }}
                >
                  {LOCALIZE.profile.personalInfo.emailAddressesSection.emailError}
                </label>
              )}
            </div>
          </div>
          <div
            id="personal-info-date-of-birth"
            className="profile-peronal-info-grid"
            style={styles.dateOfBirthContainer}
          >
            <div id="dob-title" className="profile-peronal-info-first-column">
              <p>
                <label id="date-of-birth" style={styles.titleFieldLabel}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.title}
                </label>
                <OverlayTrigger
                  trigger="focus"
                  placement="top"
                  overlay={
                    <Popover style={styles.popover}>
                      <div>
                        <p>{LOCALIZE.profile.personalInfo.dateOfBirth.titleTooltip}</p>
                      </div>
                    </Popover>
                  }
                >
                  <Button tabIndex="-1" variant="link" style={styles.tooltipIconContainer}>
                    <FontAwesomeIcon
                      icon={faQuestionCircle}
                      style={styles.tooltipIcon}
                    ></FontAwesomeIcon>
                  </Button>
                </OverlayTrigger>
              </p>
            </div>
            <div style={styles.monthAndDayField}>
              <Select
                id="selected-day-field"
                className="valid-field"
                name="date-of-birth-day"
                aria-labelledby={
                  "dob-title date-of-birth-tooltip-for-accessibility day-field-selected current-day-value-intro day-field-value"
                }
                //TODO (fnormand): make the DOB fields editable or not depending on the decision (same for the other two field below)
                // aria-required={true}
                // options={dateOfBirthDayOptions}
                // onChange={this.getSelectedDateOfBirthDay}
                clearable={false}
                value={dateOfBirthDaySelectedValue}
              ></Select>
              <div>
                <label id="date-of-birth-tooltip-for-accessibility" style={styles.hiddenText}>
                  , {LOCALIZE.profile.personalInfo.dateOfBirth.titleTooltip}
                </label>
                <label id="day-field-label" style={styles.label}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.dayField}
                </label>
                <label id="day-field-selected" style={styles.hiddenText}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.dayFieldSelected}
                </label>
                <label id="current-day-value-intro" style={styles.hiddenText}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.currentValue}
                </label>
                <label id="day-field-value" style={styles.hiddenText}>
                  {this.state.dateOfBirthDaySelectedValue !== null
                    ? this.state.dateOfBirthDaySelectedValue.label
                    : ""}
                </label>
              </div>
            </div>
            <div style={styles.monthAndDayField}>
              <Select
                id="selected-month-field"
                className="valid-field"
                name="date-of-birth-month"
                aria-labelledby={
                  "dob-title month-field-selected current-month-value-intro month-field-value"
                }
                aria-required={true}
                // options={dateOfBirthMonthOptions}
                // onChange={this.getSelectedDateOfBirthMonth}
                // clearable={false}
                value={dateOfBirthMonthSelectedValue}
              ></Select>
              <div>
                <label id="month-field-label" style={styles.label}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.monthField}
                </label>
                <label id="month-field-selected" style={styles.hiddenText}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.monthFieldSelected}
                </label>
                <label id="current-month-value-intro" style={styles.hiddenText}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.currentValue}
                </label>
                <label id="month-field-value" style={styles.hiddenText}>
                  {this.state.dateOfBirthMonthSelectedValue !== null
                    ? this.state.dateOfBirthMonthSelectedValue.label
                    : ""}
                </label>
              </div>
            </div>
            <div style={styles.yearField}>
              <Select
                id="selected-year-field"
                className="valid-field"
                name="date-of-birth-year"
                aria-labelledby={
                  "dob-title year-field-selected current-year-value-intro year-field-value"
                }
                aria-required={true}
                // options={dateOfBirthYearOptions}
                // onChange={this.getSelectedDateOfBirthYear}
                // clearable={false}
                value={dateOfBirthYearSelectedValue}
              ></Select>
              <div>
                <label id="year-field-label" style={styles.label}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.yearField}
                </label>
                <label id="year-field-selected" style={styles.hiddenText}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.yearFieldSelected}
                </label>
                <label id="current-year-value-intro" style={styles.hiddenText}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.currentValue}
                </label>
                <label id="year-field-value" style={styles.hiddenText}>
                  {this.state.dateOfBirthYearSelectedValue !== null
                    ? this.state.dateOfBirthYearSelectedValue.label
                    : ""}
                </label>
              </div>
            </div>
          </div>
          <div
            id="personal-info-pri-or-military-nbr"
            className="profile-peronal-info-grid"
            style={styles.priContainer}
          >
            <div
              id="pri-or-military-nbr-title"
              className="profile-peronal-info-first-column"
              style={styles.floatLeft}
            >
              <label id="pri-or-military-number" htmlFor="pri-or-military-number-input">
                {LOCALIZE.profile.personalInfo.priOrMilitaryNbr.title}
              </label>
              <label htmlFor="pri-or-military-number-input">
                {LOCALIZE.profile.personalInfo.optionalField}
              </label>
              <OverlayTrigger
                trigger="focus"
                placement="bottom"
                overlay={
                  <Popover style={styles.popover}>
                    <div>
                      <p>{LOCALIZE.profile.personalInfo.priOrMilitaryNbr.titleTooltip}</p>
                    </div>
                  </Popover>
                }
              >
                <Button
                  tabIndex="-1"
                  variant="link"
                  style={{ ...styles.tooltipIconContainer, ...styles.tooltipMarginTop }}
                >
                  <FontAwesomeIcon
                    icon={faQuestionCircle}
                    style={styles.tooltipIcon}
                  ></FontAwesomeIcon>
                </Button>
              </OverlayTrigger>
              <label
                id="pri-or-military-number-tooltip-for-accessibility"
                style={styles.hiddenText}
              >
                , {LOCALIZE.profile.personalInfo.priOrMilitaryNbr.titleTooltip}
              </label>
            </div>
            <div style={styles.floatRight}>
              <div>
                <input
                  id="pri-or-military-number-input"
                  className={isValidPriOrMilitaryNbr ? "valid-field" : "invalid-field"}
                  aria-labelledby="pri-or-military-number pri-or-military-number-tooltip-for-accessibility invalid-pri-or-military-number-msg"
                  aria-required={false}
                  aria-invalid={!isValidPriOrMilitaryNbr}
                  style={styles.input}
                  type="text"
                  value={priOrMilitaryNbrContent}
                  onChange={this.updatePriOrMilitaryNbrValue}
                ></input>
              </div>
              {!isValidPriOrMilitaryNbr && (
                <label id="invalid-pri-or-military-number-msg" style={styles.errorMessage}>
                  {LOCALIZE.profile.personalInfo.priOrMilitaryNbr.priOrMilitaryNbrError}
                </label>
              )}
            </div>
          </div>
        </div>
        <div id="data-saved-successfully" style={styles.updateButtonContainer}>
          {displayDataUpdatedConfirmationMsg && (
            <p style={styles.updateButtonConfirmationMsg}>
              {LOCALIZE.profile.personalInfo.dataUpdatedConfirmation}
            </p>
          )}
          <button
            className="btn btn-primary"
            aria-describedby="data-saved-successfully"
            style={styles.updateButton}
            aria-label={LOCALIZE.profile.saveButton}
            onClick={this.handleSaveData}
          >
            <FontAwesomeIcon icon={faSave} style={styles.buttonIcon} />
          </button>
        </div>
      </div>
    );
  }
}

export { PersonalInfo as unconnectedPersonalInfo };

const mapStateToProps = (state, ownProps) => {
  return {
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    username: state.user.username,
    primaryEmail: state.user.primaryEmail,
    secondaryEmail: state.user.secondaryEmail,
    dateOfBirth: state.user.dateOfBirth,
    priOrMilitaryNbr: state.user.priOrMilitaryNbr,
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateUserPersonalInfo,
      setUserInformation
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInfo);
