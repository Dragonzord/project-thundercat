import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "../../css/password.css";
import Select from "react-select";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash, faSave } from "@fortawesome/free-solid-svg-icons";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { OverlayTrigger, Popover, Button } from "react-bootstrap";
import {
  updateUserPassword,
  validateUserCredentials,
  updateUserLastPasswordChangeTime
} from "../../modules/UserProfileRedux";
import { setLastPasswordChange } from "../../modules/UserRedux";
import { validatePassword } from "../../helpers/regexValidator";
import PasswordMinimumRequirements from "../authentication/PasswordMinimumRequirements";
import { PASSWORD_REQUIREMENTS } from "../../helpers/passwordRequirementsDefinition";
import { getUserInformation } from "../../modules/LoginRedux";

const styles = {
  container: {
    padding: "12px 0 0 24px"
  },
  passwordRecoveryMargin: {
    marginTop: 62
  },
  passwordInput: {
    width: 270,
    height: 38,
    padding: "3px 40px 3px 6px",
    borderRadius: 4
  },
  input: {
    width: 270,
    height: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  label: {
    marginTop: 6
  },
  contentContainer: {
    display: "inline-block",
    marginTop: 24
  },
  firstColumn: {
    width: 215,
    float: "left"
  },
  secondColumn: {
    float: "right"
  },
  dropdown: {
    width: 550
  },
  inputIcon: {
    position: "absolute",
    margin: "5px 0 0 228px",
    fontSize: "20px",
    color: "#00565e"
  },
  iconPadding: {
    padding: "5px 9px"
  },
  popover: {
    zIndex: 9999,
    padding: "0 12px",
    margin: 0
  },
  tooltipIconContainer: {
    padding: 0
  },
  tooltipIcon: {
    height: 36,
    width: 40,
    color: "#00565e",
    padding: 6
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  updateButtonContainer: {
    marginTop: 24,
    float: "right"
  },
  updateButton: {
    marginRight: 30,
    minWidth: 100
  },
  updateButtonConfirmationMsg: {
    float: "left",
    marginRight: 24,
    color: "#278400"
  },
  buttonIcon: {
    transform: "scale(2)",
    padding: "1.5px"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  }
};

class Password extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    username: PropTypes.string.isRequired,
    //provided by redux
    updateUserPassword: PropTypes.func,
    validateUserCredentials: PropTypes.func,
    getUserInformation: PropTypes.func,
    updateUserLastPasswordChangeTime: PropTypes.func,
    setLastPasswordChange: PropTypes.func
  };

  state = {
    currentPasswordContent: "",
    viewCurrentPassword: false,
    currentPasswordInputType: "password",
    newPasswordContent: "",
    viewNewPassword: false,
    newPasswordInputType: "password",
    newPasswordConfirmationContent: "",
    viewNewPasswordConfirmation: false,
    newPasswordConfirmationInputType: "password",
    invalidCredentialsError: false,
    newPasswordConfirmationMustMatchError: false,
    minimumNewPasswordRequirementsError: false,
    newPasswordTooCommonError: false,
    newPasswordTooSimilarToUsernameError: false,
    newPasswordTooSimilarToFirstNameError: false,
    newPasswordTooSimilarToLastNameError: false,
    newPasswordTooSimilarToEmailError: false,
    displayPasswordUpdatedSuccessfullyMsg: false,
    secretQuestionOptions: [],
    secretQuestionSelectedValue: null,
    secretAnswerContent: "",
    isValidSecretQuestionSection: true,
    displaySecretQuestionUpdatedSuccessfullyMsg: false
  };

  componentDidMount = () => {
    this._isMounted = true;
    this.populateSecretQuestionOptions();
    this.getLastPasswordChangeTime();
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  // TODO (fnormand): create new model with secret questions + create new view to get the questions and populate them using this function here
  // populate secret question options
  populateSecretQuestionOptions = () => {
    let secretQuestionsArray = [];
    // 5 temporary questions
    secretQuestionsArray.push({ value: 0, label: "Question 1..." });
    secretQuestionsArray.push({ value: 1, label: "Question 2..." });
    secretQuestionsArray.push({ value: 2, label: "Question 3..." });
    secretQuestionsArray.push({ value: 3, label: "Question 4..." });
    secretQuestionsArray.push({ value: 4, label: "Question 5..." });
    //save result in state
    this.setState({ secretQuestionOptions: secretQuestionsArray });
  };

  // update current password content
  updateCurrentPasswordContent = event => {
    const currentPasswordContent = event.target.value;
    this.setState({
      currentPasswordContent: currentPasswordContent,
      displayPasswordUpdatedSuccessfullyMsg: false
    });
  };

  // update new password content
  updateNewPasswordContent = event => {
    const newPasswordContent = event.target.value;
    this.setState({
      newPasswordContent: newPasswordContent,
      displayPasswordUpdatedSuccessfullyMsg: false
    });
  };

  // update confirm password content
  updatenewPasswordConfirmationContent = event => {
    const newPasswordConfirmationContent = event.target.value;
    this.setState({
      newPasswordConfirmationContent: newPasswordConfirmationContent,
      displayPasswordUpdatedSuccessfullyMsg: false
    });
  };

  // update secret answer content
  updateSecretAnswerContent = event => {
    const secretAnswerContent = event.target.value;
    this.setState({
      secretAnswerContent: secretAnswerContent
    });
  };

  // get selected secret question
  getSelectedSecretQuestion = selectedOption => {
    this.setState({ secretQuestionSelectedValue: selectedOption });
  };

  // toggle state to display or not the current password
  toggleCurrentPasswordView = () => {
    this.setState({ viewCurrentPassword: !this.state.viewCurrentPassword }, () => {
      if (this.state.viewCurrentPassword) {
        this.setState({ currentPasswordInputType: "text" });
      } else {
        this.setState({ currentPasswordInputType: "password" });
      }
    });
  };

  // toggle state to display or not the new password
  toggleNewPasswordView = () => {
    this.setState({ viewNewPassword: !this.state.viewNewPassword }, () => {
      if (this.state.viewNewPassword) {
        this.setState({ newPasswordInputType: "text" });
      } else {
        this.setState({ newPasswordInputType: "password" });
      }
    });
  };

  // toggle state to display or not the confirm password
  toggleConfirmPasswordView = () => {
    this.setState({ viewNewPasswordConfirmation: !this.state.viewNewPasswordConfirmation }, () => {
      if (this.state.viewNewPasswordConfirmation) {
        this.setState({ newPasswordConfirmationInputType: "text" });
      } else {
        this.setState({ newPasswordConfirmationInputType: "password" });
      }
    });
  };

  // save new user password/security question in the database
  handleSaveNewPasswordData = () => {
    // reseting all error fields before each validation
    this.resetPasswordErrorFields();
    // checks if we need user's credentials validation
    const passwordChangeRequested =
      this.state.currentPasswordContent !== "" ||
      this.state.newPasswordContent !== "" ||
      this.state.newPasswordConfirmationContent !== "";
    // if at least one password/new password field has been filled
    if (passwordChangeRequested) {
      // password/new password validation
      // validating new password with regex password validator
      let isValidNewPassword =
        validatePassword(this.state.newPasswordContent).length === 0 ? true : false;
      // validating that new password and new password confirmation are the same
      let isValidNewPasswordConfirmation =
        this.state.newPasswordContent === this.state.newPasswordConfirmationContent ? true : false;

      // checking if both (new pasword and new password confirmation) are valid
      if (isValidNewPassword && isValidNewPasswordConfirmation) {
        // validating user's credentials
        this.props
          .validateUserCredentials({
            username: this.props.username,
            password: this.state.currentPasswordContent
          })
          .then(response => {
            // if user's credentials are valid
            if (response.status === 200) {
              // updated user's password
              this.props
                .updateUserPassword(localStorage.getItem("auth_token"), {
                  current_password: this.state.currentPasswordContent,
                  new_password: this.state.newPasswordContent,
                  re_new_password: this.state.newPasswordConfirmationContent
                })
                .then(response => {
                  // password updated successfully
                  if (response.status === 204) {
                    // remove password/new password data from all fields + update displayPasswordUpdatedSuccessfullyMsg state to true
                    this.setState({
                      currentPasswordContent: "",
                      newPasswordContent: "",
                      newPasswordConfirmationContent: "",
                      displayPasswordUpdatedSuccessfullyMsg: true
                    });
                    // updating last password change time
                    this.props
                      .updateUserLastPasswordChangeTime(
                        localStorage.getItem("auth_token"),
                        this.props.username
                      )
                      .then(() => {
                        // getting new latest password change time
                        this.getLastPasswordChangeTime();
                      });
                    // there are new password errors
                  } else if (response.new_password.length > 0) {
                    this.handlePasswordErrors(response);
                    this.focusOnHighestPasswordErrorField();
                    // should never happen
                  } else {
                    throw new Error("An error occurred during the password change process");
                  }
                });

              // user's credentials are not valid, so password section is invalid
            } else {
              this.setState({ invalidCredentialsError: true }, () => {
                this.focusOnHighestPasswordErrorField();
              });
            }
          });
        // new password and/or new password confirmation are invalid
      } else {
        // invalid new password based on minimum requirements
        if (!isValidNewPassword) {
          this.setState({ minimumNewPasswordRequirementsError: true }, () => {
            this.focusOnHighestPasswordErrorField();
          });
        }
        // new password does not match new password confirmation
        if (!isValidNewPasswordConfirmation) {
          this.setState(
            {
              newPasswordConfirmationMustMatchError: true
            },
            () => {
              this.focusOnHighestPasswordErrorField();
            }
          );
        }
      }
    }
  };

  // handle password errors, such as password too similar to username, first name, last name and email and password is too common
  handlePasswordErrors = response => {
    // checking for password too common error
    if (response.new_password.indexOf(PASSWORD_REQUIREMENTS.passwordTooCommon) >= 0) {
      this.setState({ newPasswordTooCommonError: true });
    }

    // checking for password too similar to username error
    if (response.new_password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToUsername) >= 0) {
      this.setState({ newPasswordTooSimilarToUsernameError: true });
    }

    // checking for password too similar to first name error
    if (response.new_password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToFirstName) >= 0) {
      this.setState({ newPasswordTooSimilarToFirstNameError: true });
    }

    // checking for password too similar to last name error
    if (response.new_password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToLastName) >= 0) {
      this.setState({ newPasswordTooSimilarToLastNameError: true });
    }

    // checking for password too similar to email error
    if (response.new_password.indexOf(PASSWORD_REQUIREMENTS.passwordTooSimilarToEmail) >= 0) {
      this.setState({ newPasswordTooSimilarToEmailError: true });
    }
  };

  // reseting all password/new password error fields
  resetPasswordErrorFields = () => {
    this.setState({
      invalidCredentialsError: false,
      newPasswordConfirmationMustMatchError: false,
      minimumNewPasswordRequirementsError: false,
      newPasswordTooCommonError: false,
      newPasswordTooSimilarToUsernameError: false,
      newPasswordTooSimilarToFirstNameError: false,
      newPasswordTooSimilarToLastNameError: false,
      newPasswordTooSimilarToEmailError: false
    });
  };

  // getting last_password_change time
  getLastPasswordChangeTime = () => {
    if (this._isMounted) {
      this.props.getUserInformation(localStorage.getItem("auth_token")).then(response => {
        // saving result in state
        this.props.setLastPasswordChange(response.last_password_change);
      });
    }
  };

  // analyses field by field and focus on the highest error field (password section only)
  focusOnHighestPasswordErrorField = () => {
    if (this.state.invalidCredentialsError) {
      document.getElementById("current-password-input").focus();
    } else if (
      this.state.minimumNewPasswordRequirementsError ||
      this.state.newPasswordTooCommonError ||
      this.state.newPasswordTooSimilarToUsernameError ||
      this.state.newPasswordTooSimilarToFirstNameError ||
      this.state.newPasswordTooSimilarToLastNameError ||
      this.state.newPasswordTooSimilarToEmailError
    ) {
      document.getElementById("new-password-input").focus();
    } else if (this.state.newPasswordConfirmationMustMatchError) {
      document.getElementById("confirm-password-input").focus();
    }
  };

  //TODO
  handleSaveSecretQuestionData = () => {
    console.log("HANDLE SAVE SECRET QUESTION DATA");
  };

  render() {
    const {
      currentPasswordContent,
      currentPasswordInputType,
      newPasswordContent,
      newPasswordInputType,
      newPasswordConfirmationContent,
      newPasswordConfirmationInputType,
      invalidCredentialsError,
      newPasswordConfirmationMustMatchError,
      minimumNewPasswordRequirementsError,
      newPasswordTooCommonError,
      newPasswordTooSimilarToUsernameError,
      newPasswordTooSimilarToFirstNameError,
      newPasswordTooSimilarToLastNameError,
      newPasswordTooSimilarToEmailError,
      displayPasswordUpdatedSuccessfullyMsg,
      secretAnswerContent,
      secretQuestionOptions,
      secretQuestionSelectedValue,
      displaySecretQuestionUpdatedSuccessfullyMsg
    } = this.state;
    let lastPasswordChangeTime = this.props.lastPasswordChange;
    // catches an odd case where it may be null; set to "" if that happens
    if (lastPasswordChangeTime === undefined || lastPasswordChangeTime === null) {
      lastPasswordChangeTime = "";
    }
    return (
      <div>
        <div>
          <h2>{LOCALIZE.profile.password.newPassword.title}</h2>
          <div style={styles.container}>
            <p>
              {LOCALIZE.formatString(
                LOCALIZE.profile.password.newPassword.updatedDate,
                lastPasswordChangeTime !== ""
                  ? lastPasswordChangeTime
                  : LOCALIZE.profile.password.newPassword.updatedDateNever
              )}
            </p>
            <div style={styles.contentContainer}>
              <div style={styles.firstColumn}>
                <label id="current-password" htmlFor="current-password-input" style={styles.label}>
                  {LOCALIZE.profile.password.newPassword.currentPassword}
                </label>
              </div>
              <div style={styles.secondColumn}>
                <div style={styles.inputIcon} onClick={this.toggleCurrentPasswordView}>
                  <span className="password-visility-icon" style={styles.iconPadding}>
                    <FontAwesomeIcon
                      icon={currentPasswordInputType === "password" ? faEyeSlash : faEye}
                    />
                  </span>
                </div>
                <div>
                  <div>
                    <input
                      id="current-password-input"
                      className={invalidCredentialsError ? "invalid-field" : "valid-field"}
                      aria-labelledby="current-password current-password-error-labels"
                      aria-required={true}
                      aria-invalid={invalidCredentialsError}
                      style={styles.passwordInput}
                      type={currentPasswordInputType}
                      value={currentPasswordContent}
                      onChange={this.updateCurrentPasswordContent}
                    ></input>
                  </div>
                  <div id="current-password-error-labels">
                    {invalidCredentialsError && (
                      <label htmlFor="current-password-input" style={styles.errorMessage}>
                        {LOCALIZE.authentication.login.invalidCredentials}
                      </label>
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div style={styles.contentContainer}>
              <div style={styles.firstColumn}>
                <label id="new-password" htmlFor="new-password-input" style={styles.label}>
                  {LOCALIZE.profile.password.newPassword.newPassword}
                </label>
                <OverlayTrigger
                  trigger="focus"
                  placement="top"
                  overlay={
                    <Popover style={styles.popover}>
                      <PasswordMinimumRequirements />
                    </Popover>
                  }
                >
                  <Button tabIndex="-1" variant="link" style={styles.tooltipIconContainer}>
                    <FontAwesomeIcon
                      icon={faQuestionCircle}
                      style={styles.tooltipIcon}
                    ></FontAwesomeIcon>
                  </Button>
                </OverlayTrigger>
                <label id="new-password-tooltip-for-accessibility" style={styles.hiddenText}>
                  <PasswordMinimumRequirements />
                </label>
              </div>
              <div style={styles.secondColumn}>
                <div style={styles.inputIcon} onClick={this.toggleNewPasswordView}>
                  <span className="password-visility-icon" style={styles.iconPadding}>
                    <FontAwesomeIcon
                      icon={newPasswordInputType === "password" ? faEyeSlash : faEye}
                    />
                  </span>
                </div>
                <div>
                  <div>
                    <input
                      id="new-password-input"
                      className={
                        newPasswordTooCommonError ||
                        newPasswordTooSimilarToUsernameError ||
                        newPasswordTooSimilarToFirstNameError ||
                        newPasswordTooSimilarToLastNameError ||
                        newPasswordTooSimilarToEmailError ||
                        minimumNewPasswordRequirementsError
                          ? "invalid-field"
                          : "valid-field"
                      }
                      aria-labelledby={`new-password ${
                        minimumNewPasswordRequirementsError
                          ? ""
                          : "new-password-tooltip-for-accessibility"
                      } new-password-error-labels`}
                      aria-required={true}
                      aria-invalid={
                        newPasswordTooCommonError ||
                        newPasswordTooSimilarToUsernameError ||
                        newPasswordTooSimilarToFirstNameError ||
                        newPasswordTooSimilarToLastNameError ||
                        newPasswordTooSimilarToEmailError ||
                        minimumNewPasswordRequirementsError
                      }
                      style={styles.passwordInput}
                      type={newPasswordInputType}
                      value={newPasswordContent}
                      onChange={this.updateNewPasswordContent}
                    ></input>
                  </div>
                  <div id="new-password-error-labels">
                    {newPasswordTooCommonError && (
                      <label htmlFor="new-password-input" style={styles.errorMessage}>
                        {LOCALIZE.authentication.createAccount.passwordTooCommonError}
                      </label>
                    )}
                    {newPasswordTooSimilarToUsernameError && (
                      <label htmlFor="new-password-input" style={styles.errorMessage}>
                        {LOCALIZE.authentication.createAccount.passwordTooSimilarToUsernameError}
                      </label>
                    )}
                    {newPasswordTooSimilarToFirstNameError && (
                      <label htmlFor="new-password-input" style={styles.errorMessage}>
                        {LOCALIZE.authentication.createAccount.passwordTooSimilarToFirstNameError}
                      </label>
                    )}
                    {newPasswordTooSimilarToLastNameError && (
                      <label htmlFor="new-password-input" style={styles.errorMessage}>
                        {LOCALIZE.authentication.createAccount.passwordTooSimilarToLastNameError}
                      </label>
                    )}
                    {newPasswordTooSimilarToEmailError && (
                      <label htmlFor="new-password-input" style={styles.errorMessage}>
                        {LOCALIZE.authentication.createAccount.passwordTooSimilarToEmailError}
                      </label>
                    )}
                    {minimumNewPasswordRequirementsError && (
                      <label htmlFor="new-password-input" style={styles.errorMessage}>
                        <PasswordMinimumRequirements />
                      </label>
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div style={styles.contentContainer}>
              <div style={styles.firstColumn}>
                <label id="confirm-password" htmlFor="confirm-password-input" style={styles.label}>
                  {LOCALIZE.profile.password.newPassword.confirmPassword}
                </label>
              </div>
              <div style={styles.secondColumn}>
                <div style={styles.inputIcon} onClick={this.toggleConfirmPasswordView}>
                  <span className="password-visility-icon" style={styles.iconPadding}>
                    <FontAwesomeIcon
                      icon={newPasswordConfirmationInputType === "password" ? faEyeSlash : faEye}
                    />
                  </span>
                </div>
                <div>
                  <div>
                    <input
                      id="confirm-password-input"
                      className={
                        newPasswordConfirmationMustMatchError ? "invalid-field" : "valid-field"
                      }
                      aria-labelledby="confirm-password new-password-confirmation-error-labels"
                      aria-required={true}
                      aria-invalid={newPasswordConfirmationMustMatchError}
                      style={styles.passwordInput}
                      type={newPasswordConfirmationInputType}
                      value={newPasswordConfirmationContent}
                      onChange={this.updatenewPasswordConfirmationContent}
                    ></input>
                  </div>
                  <div id="new-password-confirmation-error-labels">
                    {newPasswordConfirmationMustMatchError && (
                      <label htmlFor="confirm-password-input" style={styles.errorMessage}>
                        {
                          LOCALIZE.authentication.createAccount.content.inputs
                            .passwordConfirmationError
                        }
                      </label>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="new-password-saved-successfully" style={styles.updateButtonContainer}>
            {displayPasswordUpdatedSuccessfullyMsg && (
              <p style={styles.updateButtonConfirmationMsg}>
                {LOCALIZE.profile.password.newPassword.passwordUpdatedConfirmation}
              </p>
            )}
            <button
              className="btn btn-primary"
              aria-describedby="new-password-saved-successfully"
              style={styles.updateButton}
              aria-label={LOCALIZE.profile.saveButton}
              onClick={this.handleSaveNewPasswordData}
            >
              <FontAwesomeIcon icon={faSave} style={styles.buttonIcon} />
            </button>
          </div>
        </div>
        <div style={styles.passwordRecoveryMargin}>
          <h2>{LOCALIZE.profile.password.passwordRecovery.title}</h2>
          <div style={styles.container}>
            <div style={styles.contentContainer}>
              <div style={styles.firstColumn}>
                <label id="secret-question" style={styles.label}>
                  <p>{LOCALIZE.profile.password.passwordRecovery.secretQuestion}</p>
                </label>
              </div>
              <div style={{ ...styles.secondColumn, ...styles.dropdown }}>
                <Select
                  id="secret-question-select"
                  className="valid-field"
                  name="secret-question-name"
                  aria-labelledby={"secret-question"}
                  aria-required={true}
                  options={secretQuestionOptions}
                  onChange={this.getSelectedSecretQuestion}
                  clearable={false}
                  value={secretQuestionSelectedValue}
                ></Select>
              </div>
            </div>
            <div style={styles.contentContainer}>
              <div style={styles.firstColumn}>
                <label id="secret-answer" htmlFor="secret-answer-input" style={styles.label}>
                  {LOCALIZE.profile.password.passwordRecovery.secretAnswer}
                </label>
              </div>
              <div style={styles.secondColumn}>
                <input
                  id="secret-answer-input"
                  className="valid-field"
                  aria-labelledby="secret-answer"
                  aria-required={true}
                  style={styles.input}
                  type="text"
                  value={secretAnswerContent}
                  onChange={this.updateSecretAnswerContent}
                ></input>
              </div>
            </div>
          </div>
        </div>
        <div id="secret-question-saved-successfully" style={styles.updateButtonContainer}>
          {displaySecretQuestionUpdatedSuccessfullyMsg && (
            <p style={styles.updateButtonConfirmationMsg}>
              {LOCALIZE.profile.password.passwordRecovery.secretQuestionUpdatedConfirmation}
            </p>
          )}
          <button
            className="btn btn-primary"
            aria-describedby="secret-question-saved-successfully"
            style={styles.updateButton}
            aria-label={LOCALIZE.profile.saveButton}
            onClick={this.handleSaveSecretQuestionData}
          >
            <FontAwesomeIcon icon={faSave} style={styles.buttonIcon} />
          </button>
        </div>
      </div>
    );
  }
}

export { Password as unconnectedPassword };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    lastPasswordChange: state.user.lastPasswordChange
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateUserPassword,
      validateUserCredentials,
      getUserInformation,
      updateUserLastPasswordChangeTime,
      setLastPasswordChange
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Password);
