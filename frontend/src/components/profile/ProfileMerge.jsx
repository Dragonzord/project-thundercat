import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

class ProfileMerge extends Component {
  render() {
    return (
      <div>
        <h1>{LOCALIZE.profile.sideNavItems.profileMerge}</h1>
        <p>Coming Soon</p>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileMerge);
