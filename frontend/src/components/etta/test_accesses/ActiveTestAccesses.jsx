import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { LANGUAGES } from "../../../modules/LocalizeRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSearch,
  faSpinner,
  faBinoculars,
  faCaretLeft,
  faCaretRight,
  faTrashAlt,
  faSave,
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import Select from "react-select";
import { styles } from "../permissions/ActivePermissions";
import { styles as assignTestAccessesStyles } from "./AssignTestAccesses";
import {
  getAllActiveTestPermissions,
  getFoundActiveTestPermissions,
  updateCurrentTestPermissionsPageState,
  updateTestPermissionsPageSizeState,
  updateSearchActiveTestPermissionsStates,
  deleteTestPermission,
  updateTestPermission
} from "../../../modules/PermissionsRedux";
import { alternateColorsStyle } from "../../commons/Constants";
import ReactPaginate from "react-paginate";
import "../../../css/etta.css";
import { getDisplayOptions } from "../../../helpers/getDisplayOptions";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import DatePicker from "../../../components/commons/DatePicker";
import populateCustomFiveYearsFutureDateOptions from "../../../helpers/populateCustomDatePickerOptions";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";

const componentStyles = {
  tableHeadCenteredColumns: {
    textAlign: "center"
  },
  testAdministratorColumn: {
    paddingLeft: 12,
    width: "28%"
  },
  centeredColumns: {
    textAlign: "center",
    width: "18%"
  },
  popupContainer: {
    padding: "10px 15px"
  },
  labelContainer: {
    display: "table-cell"
  },
  boldText: {
    fontWeight: "bold"
  }
};

class ActiveTestAccesses extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  expiryDateRef = React.createRef();

  static propTypes = {
    // provided by redux
    getAllActiveTestPermissions: PropTypes.func,
    getFoundActiveTestPermissions: PropTypes.func,
    updateCurrentTestPermissionsPageState: PropTypes.func,
    updateTestPermissionsPageSizeState: PropTypes.func,
    updateSearchActiveTestPermissionsStates: PropTypes.func,
    deleteTestPermission: PropTypes.func,
    updateTestPermission: PropTypes.func
  };

  state = {
    displayResultsFound: false,
    searchBarContent: "",
    resultsFound: 0,
    clearSearchTriggered: false,
    displayOptionsArray: [],
    displayOptionSelectedValue: {
      label: `${this.props.testPermissionsPaginationPageSize}`,
      value: this.props.testPermissionsPaginationPageSize
    },
    activeTestPermissions: [],
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    isLoadingActiveTestPermissions: false,
    showViewTestPermissionPopup: false,
    test_permission_id: "",
    user: "",
    username: "",
    testOrderNumber: "",
    staffingProcessNumber: "",
    departmentMinistryCode: "",
    billingContact: "",
    billingContactInfo: "",
    isOrg: "",
    isRef: "",
    testAccess: "",
    expiryDate: "",
    reasonForModifications: "",
    isValidReasonForModifications: true,
    triggerExpiryDateValidation: false,
    showDeleteConfirmationPopup: false,
    showSaveConfirmationPopup: false
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentTestPermissionsPageState(1);
    // initialize active search redux state
    this.props.updateSearchActiveTestPermissionsStates("", false);
    this.populateActiveTestPermissions();
    this.populateDisplayOptions();
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate = prevProps => {
    //if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateActiveTestPermissionsBasedOnActiveSearch();
    }
    // if testPermissionsPaginationPage gets updated
    if (prevProps.testPermissionsPaginationPage !== this.props.testPermissionsPaginationPage) {
      this.populateActiveTestPermissionsBasedOnActiveSearch();
    }
    // if testPermissionsPaginationPageSize get updated
    if (
      prevProps.testPermissionsPaginationPageSize !== this.props.testPermissionsPaginationPageSize
    ) {
      this.populateActiveTestPermissionsBasedOnActiveSearch();
    }
    // if search testPermissionsKeyword gets updated
    if (prevProps.testPermissionsKeyword !== this.props.testPermissionsKeyword) {
      // if testPermissionsKeyword redux state is empty
      if (this.props.testPermissionsKeyword !== "") {
        this.populateFoundActiveTestPermissions();
      } else if (
        this.props.testPermissionsKeyword === "" &&
        this.props.testPermissionsActiveSearch
      ) {
        this.populateFoundActiveTestPermissions();
      }
    }
    // if testPermissionsActiveSearch gets updated
    if (prevProps.testPermissionsActiveSearch !== this.props.testPermissionsActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.testPermissionsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateActiveTestPermissions();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundActiveTestPermissions();
      }
    }
    // if triggerPopulateTestPermissions gets updated
    if (prevProps.triggerPopulateTestPermissions !== this.props.triggerPopulateTestPermissions) {
      this.populateActiveTestPermissionsBasedOnActiveSearch();
    }
    // if completeDatePicked gets updated
    if (prevProps.completeDatePicked !== this.props.completeDatePicked) {
      // set new expiryDate state
      this.setState({ expiryDate: this.props.completeDatePicked });
    }
  };

  // update searchBarContent content
  updateSearchBarContent = event => {
    const searchBarContent = event.target.value;
    this.setState({
      searchBarContent: searchBarContent
    });
  };

  // get selected display option
  getSelectedDisplayOption = selectedOption => {
    this.setState(
      {
        displayOptionSelectedValue: selectedOption
      },
      () => {
        // update page size
        this.props.updateTestPermissionsPageSizeState(selectedOption.value);
        // go back to the first page to avoid display bugs
        this.props.updateCurrentTestPermissionsPageState(1);
      }
    );
  };

  // populating active test permissions
  populateActiveTestPermissions = () => {
    this._isMounted = true;
    this.setState({ isLoadingActiveTestPermissions: true }, () => {
      let activeTestPermissionsArray = [];
      this.props
        .getAllActiveTestPermissions(
          localStorage.getItem("auth_token"),
          this.props.testPermissionsPaginationPage,
          this.props.testPermissionsPaginationPageSize
        )
        .then(response => {
          if (this._isMounted) {
            this.populateActiveTestPermissionsObject(activeTestPermissionsArray, response);
          }
        })
        .then(() => {
          if (this._isMounted) {
            if (this.state.clearSearchTriggered) {
              // go back to the first page to avoid display bugs
              this.props.updateCurrentTestPermissionsPageState(1);
              this.setState({ clearSearchTriggered: false });
            }
          }
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({ isLoadingActiveTestPermissions: false });
          }
        });
    });
  };

  // populating all found active test permissions based on a search
  populateFoundActiveTestPermissions = () => {
    this.setState({ isLoadingActiveTestPermissions: true }, () => {
      let activeTestPermissionsArray = [];
      this.props
        .getFoundActiveTestPermissions(
          localStorage.getItem("auth_token"),
          this.props.currentLanguage,
          this.props.testPermissionsKeyword,
          this.props.testPermissionsPaginationPage,
          this.props.testPermissionsPaginationPageSize
        )
        .then(response => {
          // there are no results found
          if (response[0] === "no results found") {
            this.setState({
              activeTestPermissions: [],
              numberOfPages: 1,
              nextPageNumber: 0,
              previousPageNumber: 0,
              resultsFound: 0
            });
            // there is at least one result found
          } else {
            this.populateActiveTestPermissionsObject(activeTestPermissionsArray, response);
            this.setState({ resultsFound: response.count });
          }
        })
        .then(() => {
          this.setState(
            { isLoadingActiveTestPermissions: false, displayResultsFound: true },
            () => {
              // if there is at least one result found
              if (activeTestPermissionsArray.length > 0) {
                // focusing on results found label only if save confirmation popup is not displayed (avoiding focus bug)
                if (!this.state.showSaveConfirmationPopup) {
                  document.getElementById("results-found-label").focus();
                }
                // no results found
              } else {
                // focusing on results found label
                document.getElementById("no-results-table-row").focus();
              }
            }
          );
        });
    });
  };

  populateActiveTestPermissionsObject = (activeTestPermissionsArray, response) => {
    // looping in response given
    for (let i = 0; i < response.results.length; i++) {
      // pushing needed data in activeTestPermissions array
      activeTestPermissionsArray.push({
        id: response.results[i].id,
        first_name: response.results[i].first_name,
        last_name: response.results[i].last_name,
        en_test_name: response.results[i].en_test_name,
        fr_test_name: response.results[i].fr_test_name,
        test_order_number: response.results[i].test_order_number,
        expiry_date: response.results[i].expiry_date,
        username: response.results[i].username,
        staffing_process_number: response.results[i].staffing_process_number,
        department_ministry_code: response.results[i].department_ministry_code,
        billing_contact: response.results[i].billing_contact,
        billing_contact_info: response.results[i].billing_contact_info,
        is_org: response.results[i].is_org,
        is_ref: response.results[i].is_ref
      });
    }

    // saving results in state
    this.setState({
      activeTestPermissions: activeTestPermissionsArray,
      numberOfPages: Math.ceil(response.count / this.props.testPermissionsPaginationPageSize),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  // populating active test permissions based on the testPermissionsActiveSearch redux state
  populateActiveTestPermissionsBasedOnActiveSearch = () => {
    // current search
    if (this.props.testPermissionsActiveSearch) {
      this.populateFoundActiveTestPermissions();
      // no current search
    } else {
      this.populateActiveTestPermissions();
    }
  };

  // handling search functionality
  handleSearch = () => {
    // this.setState({ displayResultsFound: true }, () => {
    // go back to the first page to avoid display bugs
    this.props.updateCurrentTestPermissionsPageState(1);
    // updating search testPermissionsKeyword, testPermissionsActiveSearch redux states
    this.props.updateSearchActiveTestPermissionsStates(this.state.searchBarContent, true);
    // });
  };

  // handling clear search functionality
  handleClearSearch = () => {
    // updating search testPermissionsKeyword, testPermissionsActiveSearch redux states
    this.props.updateSearchActiveTestPermissionsStates("", false);
    // hide results found label + put back all existing active permissions + delete search bar content
    this.setState(
      {
        displayResultsFound: false,
        searchBarContent: ""
      },
      () => {
        // go back to the first page to avoid display bugs
        this.props.updateCurrentTestPermissionsPageState(1);
        // focusing on search bar
        document.getElementById("search-bar").focus();
        this.populateActiveTestPermissions();
      }
    );
  };

  // populate display options
  populateDisplayOptions = () => {
    this.setState({ displayOptionsArray: getDisplayOptions() });
  };

  // handling page changes based on pagination selection
  handlePageChange = id => {
    // "+1" because redux permissionsPaginationPage uses index 1 and react-paginate (id) uses index 0
    const selectedPage = id.selected + 1;
    // update current page redux state
    this.props.updateCurrentTestPermissionsPageState(selectedPage);
    // focusing on the first table's row
    document.getElementById("active-test-permission-row-0").focus();
  };

  // handling open selected test permission view popup
  handleOpenViewPopup = id => {
    this.setState(
      {
        test_permission_id: this.state.activeTestPermissions[id].id,
        user: `${this.state.activeTestPermissions[id].first_name} ${this.state.activeTestPermissions[id].last_name}`,
        username: this.state.activeTestPermissions[id].username,
        testOrderNumber: this.state.activeTestPermissions[id].test_order_number,
        staffingProcessNumber: this.state.activeTestPermissions[id].staffing_process_number,
        departmentMinistryCode: this.state.activeTestPermissions[id].department_ministry_code,
        billingContact: this.state.activeTestPermissions[id].billing_contact,
        billingContactInfo: this.state.activeTestPermissions[id].billing_contact_info,
        isOrg: this.state.activeTestPermissions[id].is_org,
        isRef: this.state.activeTestPermissions[id].is_ref,
        testAccess:
          this.props.currentLanguage === LANGUAGES.english
            ? this.state.activeTestPermissions[id].en_test_name
            : this.state.activeTestPermissions[id].fr_test_name,
        expiryDate: this.state.activeTestPermissions[id].expiry_date
      },
      () => {
        this.setState({ showViewTestPermissionPopup: true });
      }
    );
  };

  // getting initial date year value
  getInitialDateYearValue = expiryDate => {
    let initialDateYearValue = expiryDate.split("-")[0];
    return { label: initialDateYearValue, value: Number(initialDateYearValue) };
  };

  // getting initial date month value
  getInitialDateMonthValue = expiryDate => {
    let initialDateMonthValue = expiryDate.split("-")[1];
    let croppedInitialDateMonthValue = initialDateMonthValue;
    // needs to be defnied
    if (typeof initialDateMonthValue !== "undefined") {
      // if month is between 01 and 09
      if (initialDateMonthValue.charAt(0) === "0") {
        // remove first char to only get the number (03 -> 3)
        croppedInitialDateMonthValue = initialDateMonthValue.substr(1);
      }
    }
    return { label: initialDateMonthValue, value: Number(croppedInitialDateMonthValue) };
  };

  // getting initial date day value
  getInitialDateDayValue = expiryDate => {
    let initialDateDayValue = expiryDate.split("-")[2];
    let croppedInitialDateDayValue = initialDateDayValue;
    // needs to be defnied
    if (typeof initialDateDayValue !== "undefined") {
      // if day is between 01 and 09
      if (initialDateDayValue.charAt(0) === "0") {
        // remove first char to only get the number (03 -> 3)
        croppedInitialDateDayValue = initialDateDayValue.substr(1);
      }
    }
    return { label: initialDateDayValue, value: Number(croppedInitialDateDayValue) };
  };

  // update reasonForModifications content
  updateReasonForModificationsContent = event => {
    const reasonForModifications = event.target.value;
    this.setState({
      reasonForModifications: reasonForModifications
    });
  };

  handleCloseViewPopup = () => {
    this.setState({
      showViewTestPermissionPopup: false,
      isValidReasonForModifications: true,
      reasonForModifications: ""
    });
  };

  handleDeleteTestPermission = () => {
    this.props
      .deleteTestPermission(localStorage.getItem("auth_token"), this.state.test_permission_id)
      .then(response => {
        if (response.status === 200) {
          // re-populating test permissions table
          this.populateActiveTestPermissionsBasedOnActiveSearch();
          this.setState({ showViewTestPermissionPopup: false });
        } else {
          throw new Error("An error occurred during the delete test permission request process");
        }
      });
  };

  openDeleteConfirmationPopup = () => {
    this.setState({ showDeleteConfirmationPopup: true });
  };

  closeDeleteConfirmationPopup = () => {
    this.setState({ showDeleteConfirmationPopup: false });
  };

  closeSaveConfirmationPopup = () => {
    this.setState({ showSaveConfirmationPopup: false });
  };

  handleSaveTestPermission = () => {
    // reason for modifications validation
    // must contain at least one letter to be valid
    const regexExpression = /^(.*[A-Za-z].*)$/;
    const isValidReasonForModifications = regexExpression.test(this.state.reasonForModifications)
      ? true
      : false;

    // trigger date validation
    this.setState({ triggerExpiryDateValidation: !this.state.triggerExpiryDateValidation }, () => {
      // saving validation results in states
      this.setState(
        {
          isValidReasonForModifications: isValidReasonForModifications
        },
        () => {
          // all fields are valid
          if (this.state.isValidReasonForModifications && this.props.completeDateValidState) {
            // TODO (fnormand): implement save test permission data (including new API)
            this.props
              .updateTestPermission(
                localStorage.getItem("auth_token"),
                this.state.test_permission_id,
                this.state.expiryDate
              )
              .then(response => {
                if (response.status === 200) {
                  // re-populating test permissions table
                  this.populateActiveTestPermissionsBasedOnActiveSearch();
                  this.setState({
                    showViewTestPermissionPopup: false,
                    showSaveConfirmationPopup: true,
                    reasonForModifications: ""
                  });
                } else {
                  throw new Error(
                    "An error occurred during the delete test permission request process"
                  );
                }
              });
            // there is at least one field invalid, so focus on the highest error field
          } else {
            this.focusOnHighestErrorField();
          }
        }
      );
    });
  };

  // analysing field by field and focusing on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.props.completeDateValidState) {
      this.expiryDateRef.current.focus();
    } else if (!this.state.isValidReasonForModifications) {
      document.getElementById("reason-for-modifications").focus();
    }
  };

  render() {
    return (
      <div>
        <div style={styles.searchBarAndDisplayContainer}>
          <div
            style={
              this.state.displayResultsFound
                ? styles.searchBarContainerWithResultsFound
                : styles.searchBarContainerWithoutResultsFound
            }
          >
            <label htmlFor="search-bar" id="search-bar-title" style={styles.searchBarLabel}>
              {LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.searchBarTitle}
            </label>
            <div style={styles.searchBarContainer}>
              <input
                id="search-bar"
                aria-labelledby="search-bar-title"
                style={styles.searchBarInput}
                type="text"
                value={this.state.searchBarContent}
                onChange={this.updateSearchBarContent}
                onKeyPress={event => {
                  if (event.key === "Enter") {
                    this.handleSearch();
                  }
                }}
              ></input>
              <button
                className="btn btn-secondary"
                onClick={this.handleSearch}
                style={styles.searchIconButton}
              >
                <FontAwesomeIcon icon={faSearch} />
                <label style={styles.hiddenText}>
                  {LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.searchBarTitle}
                </label>
              </button>
              {this.state.displayResultsFound && !this.props.isLoadingActiveTestPermissions && (
                <div style={styles.resultsFound}>
                  <label id="results-found-label" htmlFor="search-bar" tabIndex={0}>
                    {this.state.activeTestPermissions.length > 1
                      ? LOCALIZE.formatString(
                          LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                            .multipleResultsFound,
                          this.state.resultsFound
                        )
                      : LOCALIZE.formatString(
                          LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                            .singularResultFound,
                          this.state.resultsFound
                        )}
                  </label>
                  <button
                    className="clear-search"
                    style={styles.clearSearchResults}
                    tabIndex={0}
                    onClick={this.handleClearSearch}
                  >
                    {LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.clearSearch}
                  </button>
                </div>
              )}
            </div>
          </div>
          <div style={styles.displayOptionContainer}>
            <div style={styles.displayOptionLabel}>
              <label id="display-options-label">
                {
                  LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                    .displayOptionLabel
                }
              </label>
              <label id="display-option-accessibility" style={styles.hiddenText}>
                {
                  LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                    .displayOptionAccessibility
                }
              </label>
            </div>
            <div style={styles.displayOptionDropdown}>
              <Select
                id="display-options-dropdown"
                name="display-options"
                aria-labelledby="display-options-label display-option-accessibility display-option-current-value-accessibility"
                placeholder=""
                options={this.state.displayOptionsArray}
                onChange={this.getSelectedDisplayOption}
                clearable={false}
                value={this.state.displayOptionSelectedValue}
              ></Select>
              <label
                id="display-option-current-value-accessibility"
                style={styles.hiddenText}
              >{`${LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.displayOptionCurrentValueAccessibility} ${this.state.displayOptionSelectedValue.value}`}</label>
            </div>
          </div>
        </div>
        <div>
          <table style={styles.table}>
            <thead>
              <tr style={styles.tableHead}>
                <th
                  id="test-administrator-column"
                  scope="col"
                  style={componentStyles.testAdministratorColumn}
                >
                  {
                    LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table
                      .testAdministrator
                  }
                </th>
                <th id="test-column" scope="col" style={componentStyles.tableHeadCenteredColumns}>
                  {LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table.test}
                </th>
                <th
                  id="test-order-number-column"
                  scope="col"
                  style={componentStyles.centeredColumns}
                >
                  {
                    LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table
                      .orderNumber
                  }
                </th>
                <th id="expiry-date-column" scope="col" style={componentStyles.centeredColumns}>
                  {
                    LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table
                      .expiryDate
                  }
                </th>
                <th
                  id="view-test-permission-column"
                  scope="col"
                  style={componentStyles.centeredColumns}
                >
                  {LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table.view}
                </th>
              </tr>
            </thead>
            <tbody>
              {this.state.activeTestPermissions.length > 0 &&
                !this.state.isLoadingActiveTestPermissions &&
                this.state.activeTestPermissions.map((testPermission, id) => {
                  return (
                    <tr
                      id={`active-test-permission-row-${id}`}
                      key={id}
                      style={alternateColorsStyle(id, 60)}
                      tabIndex={0}
                      aria-labelledby={`test-administrator-column test-administrator-label-${id} 
                      test-column test-label-${id}
                      test-order-number-column test-order-number-label-${id}
                      expiry-date-column expiry-date-label-${id}
                      view-test-permission-column view-test-permission-${id}`}
                    >
                      <td
                        id={`test-administrator-label-${id}`}
                        style={componentStyles.testAdministratorColumn}
                      >{`${testPermission.last_name}, ${testPermission.first_name}`}</td>
                      <td id={`test-label-${id}`} style={componentStyles.centeredColumns}>
                        {this.props.currentLanguage === LANGUAGES.english
                          ? testPermission.en_test_name
                          : testPermission.fr_test_name}
                      </td>
                      <td
                        id={`test-order-number-label-${id}`}
                        style={componentStyles.centeredColumns}
                      >
                        {testPermission.test_order_number}
                      </td>
                      <td id={`expiry-date-label-${id}`} style={componentStyles.centeredColumns}>
                        {testPermission.expiry_date}
                      </td>
                      <td id={`view-test-permission-${id}`} style={componentStyles.centeredColumns}>
                        <button
                          className="btn btn-secondary"
                          aria-labelledby="view-test-permission-column"
                          style={styles.viewEditDetailsBtn}
                          onClick={() => this.handleOpenViewPopup(id)}
                        >
                          <FontAwesomeIcon icon={faBinoculars} style={styles.buttonIcon} />
                        </button>
                      </td>
                    </tr>
                  );
                })}
              {this.state.activeTestPermissions.length <= 0 &&
                !this.state.isLoadingActiveTestPermissions && (
                  <tr id="no-results-table-row" style={styles.noResultsFoundRow} tabIndex={0}>
                    <td colSpan="5" style={styles.noResultsFoundLabel}>
                      <label>
                        {
                          LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                            .noResultsFound
                        }
                      </label>
                    </td>
                    <td style={styles.displayNone}></td>
                    <td style={styles.displayNone}></td>
                    <td style={styles.displayNone}></td>
                    <td style={styles.displayNone}></td>
                  </tr>
                )}
              {this.state.isLoadingActiveTestPermissions && (
                <tr id="loading">
                  <td colSpan="5" style={styles.loading}>
                    <label className="fa fa-spinner fa-spin">
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  </td>
                  <td style={styles.displayNone}></td>
                  <td style={styles.displayNone}></td>
                  <td style={styles.displayNone}></td>
                  <td style={styles.displayNone}></td>
                </tr>
              )}
            </tbody>
          </table>
          <div style={styles.paginationContainer}>
            <ReactPaginate
              pageCount={this.state.numberOfPages}
              containerClassName={"pagination"}
              breakClassName={"break"}
              activeClassName={"active-page"}
              marginPagesDisplayed={3}
              // "-1" because react-paginate uses index 0 and permissionsPaginationPage redux state uses index 1
              forcePage={this.props.testPermissionsPaginationPage - 1}
              onPageChange={page => {
                this.handlePageChange(page);
              }}
              previousLabel={
                <div style={styles.paginationIcon}>
                  <label style={styles.hiddenText}>
                    {
                      LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                        .previousPageButton
                    }
                  </label>
                  <FontAwesomeIcon icon={faCaretLeft} />
                </div>
              }
              nextLabel={
                <div style={styles.paginationIcon}>
                  <label style={styles.hiddenText}>
                    {LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.nextPageButton}
                  </label>
                  <FontAwesomeIcon icon={faCaretRight} />
                </div>
              }
            />
          </div>
        </div>
        <PopupBox
          show={this.state.showViewTestPermissionPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          displayCloseButton={true}
          closeButtonAction={this.handleCloseViewPopup}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .viewTestPermissionPopup.title
          }
          description={
            <div style={componentStyles.popupContainer}>
              <p>
                {LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                    .viewTestPermissionPopup.description,
                  <span style={componentStyles.boldText}>{this.state.user}</span>
                )}
              </p>
              <div style={assignTestAccessesStyles.fieldSeparator}>
                <div style={componentStyles.labelContainer}>
                  <label style={assignTestAccessesStyles.label}>
                    {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.username}
                  </label>
                </div>
                <div style={assignTestAccessesStyles.inputContainer}>
                  <input
                    id="username"
                    type="text"
                    disabled={true}
                    style={assignTestAccessesStyles.input}
                    value={this.state.username}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={assignTestAccessesStyles.fieldSeparator}>
                <div style={componentStyles.labelContainer}>
                  <label style={assignTestAccessesStyles.label}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .testOrderNumberLabel
                    }
                  </label>
                </div>
                <div style={assignTestAccessesStyles.inputContainer}>
                  <input
                    id="test-order-number"
                    type="text"
                    disabled={true}
                    style={assignTestAccessesStyles.input}
                    value={this.state.testOrderNumber}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={assignTestAccessesStyles.fieldSeparator}>
                <div style={componentStyles.labelContainer}>
                  <label style={assignTestAccessesStyles.label}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .staffingProcessNumber
                    }
                  </label>
                </div>
                <div style={assignTestAccessesStyles.inputContainer}>
                  <input
                    id="staffing-process-number"
                    type="text"
                    disabled={true}
                    style={assignTestAccessesStyles.input}
                    value={this.state.staffingProcessNumber}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={assignTestAccessesStyles.fieldSeparator}>
                <div style={componentStyles.labelContainer}>
                  <label style={assignTestAccessesStyles.label}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .departmentMinistry
                    }
                  </label>
                </div>
                <div style={assignTestAccessesStyles.inputContainer}>
                  <input
                    id="department-ministry-code"
                    type="text"
                    disabled={true}
                    style={assignTestAccessesStyles.input}
                    value={this.state.departmentMinistryCode}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={assignTestAccessesStyles.fieldSeparator}>
                <div style={componentStyles.labelContainer}>
                  <label style={assignTestAccessesStyles.label}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .billingContact
                    }
                  </label>
                </div>
                <div style={assignTestAccessesStyles.inputContainer}>
                  <input
                    id="billing-contact"
                    type="text"
                    disabled={true}
                    style={assignTestAccessesStyles.input}
                    value={this.state.billingContact}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={assignTestAccessesStyles.fieldSeparator}>
                <div style={componentStyles.labelContainer}>
                  <label style={assignTestAccessesStyles.label}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .billingContactInfo
                    }
                  </label>
                </div>
                <div style={assignTestAccessesStyles.inputContainer}>
                  <input
                    id="billing-contact-info"
                    type="text"
                    disabled={true}
                    style={assignTestAccessesStyles.input}
                    value={this.state.billingContactInfo}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={assignTestAccessesStyles.fieldSeparator}>
                <div style={componentStyles.labelContainer}>
                  <label style={assignTestAccessesStyles.label}>
                    {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.isOrg}
                  </label>
                </div>
                <div style={assignTestAccessesStyles.inputContainer}>
                  <input
                    id="is-org"
                    type="text"
                    disabled={true}
                    style={assignTestAccessesStyles.input}
                    value={this.state.isOrg}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={assignTestAccessesStyles.fieldSeparator}>
                <div style={componentStyles.labelContainer}>
                  <label style={assignTestAccessesStyles.label}>
                    {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.isRef}
                  </label>
                </div>
                <div style={assignTestAccessesStyles.inputContainer}>
                  <input
                    id="is-ref"
                    type="text"
                    disabled={true}
                    style={assignTestAccessesStyles.input}
                    value={this.state.isRef}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={assignTestAccessesStyles.fieldSeparator}>
                <div style={componentStyles.labelContainer}>
                  <label style={assignTestAccessesStyles.label}>
                    {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.testAccess}
                  </label>
                </div>
                <div style={assignTestAccessesStyles.inputContainer}>
                  <input
                    id="test-access"
                    type="text"
                    disabled={true}
                    style={assignTestAccessesStyles.input}
                    value={this.state.testAccess}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={assignTestAccessesStyles.fieldSeparator}>
                <div style={assignTestAccessesStyles.expiryDateLabelContainer}>
                  <label id="expiry-date-label" style={assignTestAccessesStyles.expiryDateLabel}>
                    {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.expiryDate}
                  </label>
                </div>
                <div style={assignTestAccessesStyles.inputContainer}>
                  <DatePicker
                    dateDayFieldRef={this.expiryDateRef}
                    dateLabelId={"expiry-date-label"}
                    triggerValidation={this.state.triggerExpiryDateValidation}
                    displayValidIcon={false}
                    customYearOptions={populateCustomFiveYearsFutureDateOptions()}
                    initialDateYearValue={this.getInitialDateYearValue(this.state.expiryDate)}
                    initialDateMonthValue={this.getInitialDateMonthValue(this.state.expiryDate)}
                    initialDateDayValue={this.getInitialDateDayValue(this.state.expiryDate)}
                    futureDateValidation={true}
                  />
                </div>
              </div>
              <div>
                <label id="reason-for-modification-label" style={styles.reasonLabel}>
                  {
                    LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                      .viewEditDetailsPopup.reasonForModification
                  }
                </label>

                <textarea
                  id="reason-for-modifications"
                  className={
                    this.state.isValidReasonForModifications ? "valid-field" : "invalid-field"
                  }
                  aria-labelledby="reason-for-modification-label reason-for-modifications-error"
                  aria-required={true}
                  aria-invalid={!this.state.isValidReasonForModifications}
                  style={{ ...styles.input, ...styles.reasonInput }}
                  value={this.state.reasonForModifications}
                  onChange={this.updateReasonForModificationsContent}
                  maxLength="300"
                ></textarea>
                {!this.state.isValidReasonForModifications && (
                  <label
                    id="reason-for-modifications-error"
                    htmlFor="reason-for-modifications"
                    style={styles.ReasonErrorMessage}
                  >
                    {
                      LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                        .viewEditDetailsPopup.reasonForModificationError
                    }
                  </label>
                )}
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.danger}
          leftButtonIcon={faTrashAlt}
          leftButtonLabel={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .viewTestPermissionPopup.deleteButtonAccessibility
          }
          leftButtonAction={this.openDeleteConfirmationPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonCustomStyle={{ backgroundColor: "#278400" }}
          rightButtonIcon={faSave}
          rightButtonLabel={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .viewTestPermissionPopup.saveButtonAccessibility
          }
          rightButtonAction={this.handleSaveTestPermission}
        />
        <PopupBox
          show={this.state.showDeleteConfirmationPopup}
          handleClose={this.closeDeleteConfirmationPopup}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          customPopupStyle={styles.deleteConfirmationPopupWidth}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .deleteConfirmationPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={
                  LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                    .deleteConfirmationPopup.systemMessageTitle
                }
                message={
                  <div>
                    <p>
                      {LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                          .deleteConfirmationPopup.systemMessageDescription,
                        <span style={styles.boldText}>
                          {this.state.testAccess} (
                          {
                            LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                              .testOrderNumberLabel
                          }{" "}
                          {this.state.testOrderNumber})
                        </span>,
                        <span style={styles.boldText}>{this.state.user}</span>
                      )}
                    </p>
                  </div>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.primary}
          leftButtonIcon={faTimes}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeDeleteConfirmationPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonIcon={faTrashAlt}
          rightButtonLabel={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .viewTestPermissionPopup.deleteButtonAccessibility
          }
          rightButtonAction={this.handleDeleteTestPermission}
        />
        <PopupBox
          show={this.state.showSaveConfirmationPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.saveConfirmationPopup
              .title
          }
          description={
            <div>
              {LOCALIZE.formatString(
                LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                  .saveConfirmationPopup.description,
                <span style={styles.boldText}>{this.state.user}</span>
              )}
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.closeSaveConfirmationPopup}
        />
      </div>
    );
  }
}

export { ActiveTestAccesses as unconnectedActiveTestAccesses };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testPermissionsPaginationPageSize: state.userPermissions.testPermissionsPaginationPageSize,
    testPermissionsPaginationPage: state.userPermissions.testPermissionsPaginationPage,
    testPermissionsKeyword: state.userPermissions.testPermissionsKeyword,
    triggerPopulateTestPermissions: state.userPermissions.triggerPopulateTestPermissions,
    testPermissionsActiveSearch: state.userPermissions.testPermissionsActiveSearch,
    completeDatePicked: state.datePicker.completeDatePicked,
    completeDateValidState: state.datePicker.completeDateValidState
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllActiveTestPermissions,
      getFoundActiveTestPermissions,
      updateCurrentTestPermissionsPageState,
      updateTestPermissionsPageSizeState,
      updateSearchActiveTestPermissionsStates,
      deleteTestPermission,
      updateTestPermission
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ActiveTestAccesses);
