import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Select from "react-select";
import LOCALIZE from "../../../text_resources";
import { LANGUAGES } from "../../../modules/LocalizeRedux";
import {
  getUsersBasedOnSpecifiedPermission,
  grantTestPermission,
  updateTriggerPopulateTestPermissionsState
} from "../../../modules/PermissionsRedux";
import getNonPublicTests from "../../../modules/TestRedux";
import DatePicker from "../../../components/commons/DatePicker";
import CollapsingItemContainer from "../../../components/eMIB/CollapsingItemContainer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSave, faUndo, faTimes, faSearch, faEdit } from "@fortawesome/free-solid-svg-icons";
import { resetDatePickedStates } from "../../../modules/DatePickerRedux";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import "../../../css/etta.css";
import ConfirmTestAccessAssignmentPopup from "./ConfirmTestAccessAssignmentPopup";
import populateCustomFiveYearsFutureDateOptions from "../../../helpers/populateCustomDatePickerOptions";
import { styles as activePermissionsStyles } from "../permissions/ActivePermissions";
import { OverlayTrigger, Popover } from "react-bootstrap";

export const styles = {
  container: {
    margin: "24px 60px"
  },
  labelContainer: {
    display: "table-cell"
  },
  label: {
    width: 250
  },
  expiryDateLabelContainer: {
    display: "table-cell",
    verticalAlign: "top"
  },
  expiryDateLabel: {
    width: 250,
    paddingTop: 8
  },
  dropdown: {
    width: "100%",
    display: "table-cell"
  },
  fieldSeparator: {
    marginTop: 18
  },
  usersFieldSeparator: {
    marginTop: 60
  },
  inputContainer: {
    display: "table-cell",
    width: "100%"
  },
  input: {
    width: "100%",
    height: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  customSearchBarContainer: {
    padding: 0,
    width: "100%"
  },
  searchEditBar: {
    padding: "3px 95px 3px 6px"
  },
  customSearchButton: {
    padding: "2px 0 10px 10px",
    marginRight: 48
  },
  customEditButton: {
    padding: "2px 10px 0",
    borderRadius: "inherit",
    borderLeft: "none"
  },
  buttonSeparator: {
    borderRight: "1px solid #CECECE",
    marginLeft: 10
  },
  collapsingItemLabel: {
    verticalAlign: "middle",
    margin: 0,
    padding: "6px 12px 6px 24px"
  },
  collapsingItemContainerBody: {
    marginLeft: 18
  },
  testAccessItemContainer: {
    padding: "0 12px"
  },
  checkbox: {
    transform: "scale(1.5)",
    verticalAlign: "middle"
  },
  buttonsContainer: {
    textAlign: "center",
    marginTop: 48
  },
  refreshButton: {
    width: 100,
    marginRight: 60
  },
  saveButton: {
    width: 100
  },
  saveButtonIcon: {
    transform: "scale(1.5)"
  },
  refreshButtonIcon: {
    transform: "scale(1.3)"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  popover: {
    zIndex: 9999,
    padding: "0 12px"
  }
};

class AssignTestAccesses extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  usersRef = React.createRef();
  expiryDateRef = React.createRef();

  static propTypes = {
    triggerResetDateFieldValuesProps: PropTypes.string,
    // provided by redux
    getUsersBasedOnSpecifiedPermission: PropTypes.func,
    getNonPublicTests: PropTypes.func,
    resetDatePickedStates: PropTypes.func,
    grantTestPermission: PropTypes.func,
    updateTriggerPopulateTestPermissionsState: PropTypes.func
  };

  state = {
    testOrderNumber: "",
    isValidTestOrderNumber: true,
    staffingProcessNumber: "",
    isValidStaffingProcessNumber: true,
    departmentMinistryCode: "",
    isValidDepartmentMinistryCode: true,
    isOrg: "",
    isValidIsOrg: true,
    isRef: "",
    isValidIsRef: true,
    billingContact: "",
    isValidBillingContact: true,
    billingContactInfo: "",
    isValidBillingContactInfo: true,
    usersOptions: [],
    usersSelectedOptions: [],
    isValidUsers: true,
    currentSelectedOptionsAccessibility:
      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.noUserAccessibility,
    triggerExpiryDateValidation: false,
    testAccessesOptions: [],
    selectedTestAccessesNames: [],
    isValidTestAccesses: true,
    triggerResetDateFieldValues: false,
    showClearFieldsPopup: false,
    showAssignTestPermissionsConfirmationPopup: false,
    showTestAccessesGrantedPopup: false,
    financialDataFieldsVisible: false,
    financialDataFieldsDisabled: true
  };

  componentDidMount = () => {
    this.populateUsersOptions();
    this.populatetestAccessesOptions();
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate = prevProps => {
    // if triggerResetDateFieldValuesProps get updated
    if (
      prevProps.triggerResetDateFieldValuesProps !== this.props.triggerResetDateFieldValuesProps
    ) {
      // if selected tab is assign test access (first tab)
      if (this.props.triggerResetDateFieldValuesProps === "assign-test-access") {
        // trigger reset date field values
        this.setState({ triggerResetDateFieldValues: !this.state.triggerResetDateFieldValues });
      }
    }
  };

  // get selected test order number content
  getTestOrderNumberContent = event => {
    const testOrderNumber = event.target.value;
    this.setState({
      testOrderNumber: testOrderNumber
    });
  };

  // get staffing process number content
  getStaffingProcessNumberContent = event => {
    const staffingProcessNumber = event.target.value;
    this.setState({ staffingProcessNumber: staffingProcessNumber });
  };

  // get department/ministry content
  getDepartmentMinistryCodeContent = event => {
    const departmentMinistryCode = event.target.value;
    this.setState({ departmentMinistryCode: departmentMinistryCode });
  };

  // get is org content
  getIsOrgContent = event => {
    const isOrg = event.target.value;
    this.setState({ isOrg: isOrg });
  };

  // get is ref content
  getIsRefContent = event => {
    const isRef = event.target.value;
    this.setState({ isRef: isRef });
  };

  // get billing contact content
  getBillingContactContent = event => {
    const billingContact = event.target.value;
    this.setState({ billingContact: billingContact });
  };

  // get billing contact info content
  getBillingContactInfoContent = event => {
    const billingContactInfo = event.target.value;
    this.setState({ billingContactInfo: billingContactInfo });
  };

  // populate users options (all users that have test administrators permission/role)
  populateUsersOptions = () => {
    let usersOptions = [];
    this.props
      .getUsersBasedOnSpecifiedPermission(
        localStorage.getItem("auth_token"),
        "is_test_administrator"
      )
      .then(response => {
        for (let i = 0; i < response.length; i++) {
          usersOptions.push({
            label: `${response[i].last_name}, ${response[i].first_name} - ${response[i].pri_or_military_nbr}`,
            value: `${response[i].user}`
          });
        }
      });
    this.setState({ usersOptions: usersOptions });
  };

  // get selected test order number option
  getSelectedUsersOptions = selectedOption => {
    if (selectedOption === null) {
      selectedOption = [];
    }
    this.setState({
      usersSelectedOptions: selectedOption
    });
    this.getSelectedUsersOptionsAccessibility(selectedOption);
  };

  // getting/formatting current selected users options (for accessibility only)
  getSelectedUsersOptionsAccessibility = options => {
    let currentOptions = "";
    if (options.length > 0) {
      for (let i = 0; i < options.length; i++) {
        currentOptions += `${options[i].label}, `;
      }
    } else {
      currentOptions =
        LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.noUserAccessibility;
    }
    this.setState({ currentSelectedOptionsAccessibility: currentOptions });
  };

  // populate test accesses options
  populatetestAccessesOptions = () => {
    this._isMounted = true;
    const token = localStorage.getItem("auth_token");
    let testAccessesOptions = [];
    this.props.getNonPublicTests(token).then(response => {
      if (this._isMounted) {
        for (let i = 0; i < response.length; i++) {
          testAccessesOptions.push({
            test_name: response[i].test_name,
            en_test_name: response[i].en_test_name,
            fr_test_name: response[i].fr_test_name,
            checked: false
          });
        }
        this.setState({ testAccessesOptions: testAccessesOptions });
      }
    });
  };

  // update test accesses checkboxes status
  toggleTestAccessesCheckbox = id => {
    let updatedtestAccessesOptions = Array.from(this.state.testAccessesOptions);
    updatedtestAccessesOptions[id].checked = !updatedtestAccessesOptions[id].checked;
    this.setState({ testAccessesOptions: updatedtestAccessesOptions });
  };

  openClearAllFieldsPopup = () => {
    this.setState({ showClearFieldsPopup: true });
  };

  closeClearAllFieldsPopup = () => {
    this.setState({ showClearFieldsPopup: false });
  };

  openAssignTestPermissionsConfirmationPopup = () => {
    this.setState({ showAssignTestPermissionsConfirmationPopup: true });
  };

  closeAssignTestPermissionsConfirmationPopup = () => {
    this.setState({ showAssignTestPermissionsConfirmationPopup: false });
  };

  openTestAccessesGrantedConfirmationPopup = () => {
    this.setState({ showTestAccessesGrantedPopup: true });
  };

  closeTestAccessesGrantedConfirmationPopup = () => {
    this.setState({ showTestAccessesGrantedPopup: false });
    this.props.updateTriggerPopulateTestPermissionsState(
      !this.props.triggerPopulateTestPermissions
    );
  };

  // handling refresh action
  handleRefreshAction = () => {
    // resetting all checkboxes (test accesses options)
    let updatedtestAccessesOptions = Array.from(this.state.testAccessesOptions);
    for (let i = 0; i < updatedtestAccessesOptions.length; i++) {
      updatedtestAccessesOptions[i].checked = false;
    }

    // resetting all needed states
    this.setState({
      testOrderNumber: "",
      isValidTestOrderNumber: true,
      staffingProcessNumber: "",
      isValidStaffingProcessNumber: true,
      departmentMinistryCode: "",
      isValidDepartmentMinistryCode: true,
      isOrg: "",
      isValidIsOrg: true,
      isRef: "",
      isValidIsRef: true,
      billingContact: "",
      isValidBillingContact: true,
      billingContactInfo: "",
      isValidBillingContactInfo: true,
      usersSelectedOptions: "",
      currentSelectedOptionsAccessibility:
        LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.noUserAccessibility,
      isValidUsers: true,
      testAccessesOptions: updatedtestAccessesOptions,
      selectedTestAccessesArray: [],
      isValidTestAccesses: true,
      triggerResetDateFieldValues: !this.state.triggerResetDateFieldValues,
      showClearFieldsPopup: false,
      financialDataFieldsVisible: false,
      financialDataFieldsDisabled: true
    });

    // resetting DatePicker redux states
    this.props.resetDatePickedStates();
  };

  // handling submit form
  handleSubmitForm = () => {
    // creating data object
    const data = {
      expiryDate: this.props.completeDatePicked,
      testOrderNumber: this.state.testOrderNumber.value,
      staffingProcessNumber: this.state.staffingProcessNumber,
      departmentMinistryCode: this.state.departmentMinistryCode,
      isOrg: this.state.isOrg,
      isRef: this.state.isRef,
      billingContact: this.state.billingContact,
      billingContactInfo: this.state.billingContactInfo
    };
    // getting selected TA users
    let selectedUsersArray = [];
    for (let i = 0; i < this.state.usersSelectedOptions.length; i++) {
      selectedUsersArray.push(this.state.usersSelectedOptions[i].value);
    }
    // getting checked test accesses options
    let selectedTestAccessesArray = [];
    for (let i = 0; i < this.state.testAccessesOptions.length; i++) {
      // if current option is checked
      if (this.state.testAccessesOptions[i].checked) {
        selectedTestAccessesArray.push(this.state.testAccessesOptions[i].test_name);
      }
    }
    // granting all needed test permissions based on selectedUsersArray and selectedTestAccessesArray
    this.props
      .grantTestPermission(
        localStorage.getItem("auth_token"),
        selectedUsersArray,
        selectedTestAccessesArray,
        data
      )
      .then(response => {
        // all test permissions have been granted
        if (response.status === 200) {
          // close data confirmation popup
          this.closeAssignTestPermissionsConfirmationPopup();
          //open test accesses granted confirmation popup
          this.openTestAccessesGrantedConfirmationPopup();
          // reset form
          this.handleRefreshAction();
        } else {
          throw new Error("An error occurred during the grant test permission process");
        }
      });
  };

  getSelectedTestNamesArray = () => {
    // getting checked test accesses options
    let selectedTestAccessesNames = [];
    for (let i = 0; i < this.state.testAccessesOptions.length; i++) {
      // if current option is checked
      if (this.state.testAccessesOptions[i].checked) {
        if (this.props.currentLanguage === LANGUAGES.english) {
          selectedTestAccessesNames.push(this.state.testAccessesOptions[i].en_test_name);
        } else {
          selectedTestAccessesNames.push(this.state.testAccessesOptions[i].fr_test_name);
        }
      }
    }
    // saving selected test accesses array in state (need it in ConfirmTestAccessAssignmentPopup component)
    this.setState({ selectedTestAccessesNames: selectedTestAccessesNames });
  };

  // validating form (except expiry_date field)
  validateForm = () => {
    const {
      testOrderNumber,
      staffingProcessNumber,
      departmentMinistryCode,
      isOrg,
      isRef,
      billingContact,
      billingContactInfo,
      usersSelectedOptions,
      testAccessesOptions
    } = this.state;

    // test order number validation
    const isValidTestOrderNumber =
      testOrderNumber !== "" && testOrderNumber.length <= 12 ? true : false;

    // staffing process number validation (max DB field length = 50)
    const isValidStaffingProcessNumber =
      staffingProcessNumber !== "" && staffingProcessNumber.length <= 50 ? true : false;

    // department/ministry validation (max DB field length = 10)
    const isValidDepartmentMinistryCode =
      departmentMinistryCode !== "" && departmentMinistryCode.length <= 10 ? true : false;

    // is org validation (max DB field length = 16)
    const isValidIsOrg = isOrg !== "" && isOrg.length <= 16 ? true : false;

    // is ref validation (max DB field length = 20)
    const isValidIsRef = isRef !== "" && isRef.length <= 20 ? true : false;

    // billing contact validation (max DB field length = 180)
    const isValidBillingContact =
      billingContact !== "" && billingContact.length <= 180 ? true : false;

    // billing contact info validation (max DB field length = 255)
    const isValidBillingContactInfo =
      billingContactInfo !== "" && billingContactInfo.length <= 255 ? true : false;

    // users validation
    const isValidUsers = usersSelectedOptions.length <= 0 ? false : true;

    // test accesses validation
    let isValidTestAccesses = false;
    for (let i = 0; i < testAccessesOptions.length; i++) {
      if (testAccessesOptions[i].checked) {
        isValidTestAccesses = true;
      }
    }

    // updating validation states
    this.setState(
      {
        triggerExpiryDateValidation: !this.state.triggerExpiryDateValidation
      },
      () => {
        this.setState(
          {
            isValidTestOrderNumber: isValidTestOrderNumber,
            isValidStaffingProcessNumber: isValidStaffingProcessNumber,
            isValidDepartmentMinistryCode: isValidDepartmentMinistryCode,
            isValidIsOrg: isValidIsOrg,
            isValidIsRef: isValidIsRef,
            isValidBillingContact: isValidBillingContact,
            isValidBillingContactInfo: isValidBillingContactInfo,
            isValidUsers: isValidUsers,
            isValidTestAccesses: isValidTestAccesses
          },
          () => {
            if (
              this.state.isValidTestOrderNumber &&
              this.state.isValidStaffingProcessNumber &&
              this.state.isValidDepartmentMinistryCode &&
              this.state.isValidIsOrg &&
              this.state.isValidIsRef &&
              this.state.isValidBillingContact &&
              this.state.isValidBillingContactInfo &&
              this.state.isValidUsers &&
              this.props.completeDateValidState &&
              this.state.isValidTestAccesses
            ) {
              // open confirmation popup
              this.openAssignTestPermissionsConfirmationPopup();
              // get selected test names
              this.getSelectedTestNamesArray();
            } else {
              this.focusOnHighestErrorField();
            }
          }
        );
      }
    );
  };

  // analyses field by field and focus on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.state.isValidTestOrderNumber) {
      document.getElementById("test-order-number").focus();
    } else if (this.state.financialDataFieldsVisible) {
      if (!this.state.isValidStaffingProcessNumber) {
        document.getElementById("staffing-process-number").focus();
      } else if (!this.state.isValidDepartmentMinistryCode) {
        document.getElementById("department-ministry-code").focus();
      } else if (!this.state.isValidIsOrg) {
        document.getElementById("is-org").focus();
      } else if (!this.state.isValidIsRef) {
        document.getElementById("is-ref").focus();
      } else if (!this.state.isValidBillingContact) {
        document.getElementById("billing-contact").focus();
      } else if (!this.state.isValidBillingContactInfo) {
        document.getElementById("billing-contact-info").focus();
      }
    } else if (!this.state.isValidUsers) {
      this.usersRef.current.focus();
    } else if (!this.props.completeDateValidState) {
      this.expiryDateRef.current.focus();
    } else if (!this.state.isValidTestAccesses) {
      document.getElementById("test-accesses-error").focus();
    }
  };

  handleSearch = () => {
    console.log("SEARCH CLICKED!");
  };

  handleManualEntry = () => {
    // displaying financial data fields + enabling them
    this.setState({ financialDataFieldsVisible: true, financialDataFieldsDisabled: false });
    // focusing on test order number field
    document.getElementById("test-order-number").focus();
  };

  render() {
    return (
      <div style={styles.container}>
        <div>
          <div style={styles.labelContainer}>
            <label id="test-order-number-label" htmlFor="test-order-number" style={styles.label}>
              {
                LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                  .testOrderNumberLabel
              }
            </label>
          </div>
          <div
            style={{
              ...activePermissionsStyles.searchBarContainer,
              ...styles.customSearchBarContainer
            }}
          >
            <input
              id="test-order-number"
              className={this.state.isValidTestOrderNumber ? "valid-field" : "invalid-field"}
              aria-invalid={!this.state.isValidTestOrderNumber}
              aria-required={true}
              type="text"
              style={{ ...styles.input, ...styles.searchEditBar }}
              value={this.state.testOrderNumber}
              onChange={this.getTestOrderNumberContent}
            ></input>
            <OverlayTrigger
              trigger="hover"
              placement="top"
              overlay={
                <Popover style={styles.popover}>
                  <div>
                    <p>
                      {
                        LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                          .searchButton
                      }
                    </p>
                  </div>
                </Popover>
              }
            >
              <button
                className="btn btn-secondary"
                onClick={this.handleSearch}
                style={{
                  ...activePermissionsStyles.searchIconButton,
                  ...styles.customSearchButton
                }}
              >
                <FontAwesomeIcon icon={faSearch} />
                <span style={styles.buttonSeparator} />
                <label style={styles.hiddenText}>
                  {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.searchButton}
                </label>
              </button>
            </OverlayTrigger>
            <OverlayTrigger
              trigger="hover"
              placement="top"
              overlay={
                <Popover style={styles.popover}>
                  <div>
                    <p>
                      {
                        LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                          .manuelEntryButton
                      }
                    </p>
                  </div>
                </Popover>
              }
            >
              <button
                className="btn btn-secondary"
                onClick={this.handleManualEntry}
                style={{ ...activePermissionsStyles.searchIconButton, ...styles.customEditButton }}
              >
                <FontAwesomeIcon icon={faEdit} />
                <label style={styles.hiddenText}>
                  {
                    LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                      .manuelEntryButton
                  }
                  }
                </label>
              </button>
            </OverlayTrigger>
            {!this.state.isValidTestOrderNumber && (
              <label
                id="test-order-number-error"
                htmlFor="test-order-number-dropdown"
                style={styles.errorMessage}
              >
                {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.emptyFieldError}
              </label>
            )}
          </div>
        </div>
        {this.state.financialDataFieldsVisible && (
          <div>
            <div style={styles.fieldSeparator}>
              <div style={styles.labelContainer}>
                <label
                  id="staffing-process-number-label"
                  htmlFor="staffing-process-number"
                  style={styles.label}
                >
                  {
                    LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                      .staffingProcessNumber
                  }
                </label>
              </div>
              <div style={styles.inputContainer}>
                <input
                  id="staffing-process-number"
                  className={
                    this.state.isValidStaffingProcessNumber ? "valid-field" : "invalid-field"
                  }
                  aria-invalid={!this.state.isValidStaffingProcessNumber}
                  aria-required={true}
                  disabled={this.state.financialDataFieldsDisabled}
                  type="text"
                  style={styles.input}
                  value={this.state.staffingProcessNumber}
                  onChange={this.getStaffingProcessNumberContent}
                ></input>
                {!this.state.isValidStaffingProcessNumber && (
                  <label
                    id="staffing-process-number-error"
                    htmlFor="staffing-process-number"
                    style={styles.errorMessage}
                  >
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .emptyFieldError
                    }
                  </label>
                )}
              </div>
            </div>
            <div style={styles.fieldSeparator}>
              <div style={styles.labelContainer}>
                <label
                  id="department-ministry-code-label"
                  htmlFor="department-ministry-code"
                  style={styles.label}
                >
                  {
                    LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                      .departmentMinistry
                  }
                </label>
              </div>
              <div style={styles.inputContainer}>
                <input
                  id="department-ministry-code"
                  className={
                    this.state.isValidDepartmentMinistryCode ? "valid-field" : "invalid-field"
                  }
                  aria-invalid={!this.state.isValidDepartmentMinistryCode}
                  aria-required={true}
                  disabled={this.state.financialDataFieldsDisabled}
                  type="text"
                  style={styles.input}
                  value={this.state.departmentMinistryCode}
                  onChange={this.getDepartmentMinistryCodeContent}
                ></input>
                {!this.state.isValidDepartmentMinistryCode && (
                  <label
                    id="department-ministry-code-error"
                    htmlFor="department-ministry-code"
                    style={styles.errorMessage}
                  >
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .emptyFieldError
                    }
                  </label>
                )}
              </div>
            </div>
            <div style={styles.fieldSeparator}>
              <div style={styles.labelContainer}>
                <label id="is-org-label" htmlFor="is-org" style={styles.label}>
                  {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.isOrg}
                </label>
              </div>
              <div style={styles.inputContainer}>
                <input
                  id="is-org"
                  className={this.state.isValidIsOrg ? "valid-field" : "invalid-field"}
                  aria-invalid={!this.state.isValidIsOrg}
                  aria-required={true}
                  disabled={this.state.financialDataFieldsDisabled}
                  type="text"
                  style={styles.input}
                  value={this.state.isOrg}
                  onChange={this.getIsOrgContent}
                ></input>
                {!this.state.isValidIsOrg && (
                  <label id="is-orgr-error" htmlFor="is-org" style={styles.errorMessage}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .emptyFieldError
                    }
                  </label>
                )}
              </div>
            </div>
            <div style={styles.fieldSeparator}>
              <div style={styles.labelContainer}>
                <label id="is-ref-label" htmlFor="is-ref" style={styles.label}>
                  {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.isRef}
                </label>
              </div>
              <div style={styles.inputContainer}>
                <input
                  id="is-ref"
                  className={this.state.isValidIsRef ? "valid-field" : "invalid-field"}
                  aria-invalid={!this.state.isValidIsRef}
                  aria-required={true}
                  disabled={this.state.financialDataFieldsDisabled}
                  type="text"
                  style={styles.input}
                  value={this.state.isRef}
                  onChange={this.getIsRefContent}
                ></input>
                {!this.state.isValidIsRef && (
                  <label id="is-ref-error" htmlFor="is-ref" style={styles.errorMessage}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .emptyFieldError
                    }
                  </label>
                )}
              </div>
            </div>
            <div style={styles.fieldSeparator}>
              <div style={styles.labelContainer}>
                <label id="billing-contact-label" htmlFor="billing-contact" style={styles.label}>
                  {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.billingContact}
                </label>
              </div>
              <div style={styles.inputContainer}>
                <input
                  id="billing-contact"
                  className={this.state.isValidBillingContact ? "valid-field" : "invalid-field"}
                  aria-invalid={!this.state.isValidBillingContact}
                  aria-required={true}
                  disabled={this.state.financialDataFieldsDisabled}
                  type="text"
                  style={styles.input}
                  value={this.state.billingContact}
                  onChange={this.getBillingContactContent}
                ></input>
                {!this.state.isValidBillingContact && (
                  <label
                    id="billing-contact-error"
                    htmlFor="billing-contact"
                    style={styles.errorMessage}
                  >
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .emptyFieldError
                    }
                  </label>
                )}
              </div>
            </div>
            <div style={styles.fieldSeparator}>
              <div style={styles.labelContainer}>
                <label
                  id="billing-contact-info-label"
                  htmlFor="billing-contact-info"
                  style={styles.label}
                >
                  {
                    LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                      .billingContactInfo
                  }
                </label>
              </div>
              <div style={styles.inputContainer}>
                <input
                  id="billing-contact-info"
                  className={this.state.isValidBillingContactInfo ? "valid-field" : "invalid-field"}
                  aria-invalid={!this.state.isValidBillingContactInfo}
                  aria-required={true}
                  disabled={this.state.financialDataFieldsDisabled}
                  type="text"
                  style={styles.input}
                  value={this.state.billingContactInfo}
                  onChange={this.getBillingContactInfoContent}
                ></input>
                {!this.state.isValidBillingContactInfo && (
                  <label
                    id="billing-contact-info-error"
                    htmlFor="billing-contact-info"
                    style={styles.errorMessage}
                  >
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .emptyFieldError
                    }
                  </label>
                )}
              </div>
            </div>
          </div>
        )}
        <div style={styles.usersFieldSeparator}>
          <div style={styles.labelContainer}>
            <label id="users-label" style={styles.label}>
              {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.users}
            </label>
          </div>
          <div style={styles.dropdown}>
            <Select
              ref={this.usersRef}
              id="users-dropdown"
              name="users-options"
              className={this.state.isValidUsers ? "valid-field" : "invalid-field"}
              aria-invalid={!this.state.isValidUsers}
              aria-required={true}
              aria-labelledby="users-label users-current-value-accessibility users-dropdown-error"
              placeholder=""
              options={this.state.usersOptions}
              onChange={this.getSelectedUsersOptions}
              clearable={false}
              value={this.state.usersSelectedOptions}
              isMulti={true}
            ></Select>
            <label
              id="users-current-value-accessibility"
              style={styles.hiddenText}
            >{`${LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.usersCurrentValueAccessibility} ${this.state.currentSelectedOptionsAccessibility}`}</label>
            {!this.state.isValidUsers && (
              <label id="users-dropdown-error" htmlFor="users-dropdown" style={styles.errorMessage}>
                {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.emptyFieldError}
              </label>
            )}
          </div>
        </div>
        <div style={styles.fieldSeparator}>
          <div style={styles.expiryDateLabelContainer}>
            <label id="expiry-date-label" style={styles.expiryDateLabel}>
              {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.expiryDate}
            </label>
          </div>
          <div style={styles.inputContainer}>
            <DatePicker
              dateDayFieldRef={this.expiryDateRef}
              dateLabelId={"expiry-date-label"}
              triggerValidation={this.state.triggerExpiryDateValidation}
              triggerResetFieldValues={this.state.triggerResetDateFieldValues}
              displayValidIcon={false}
              customYearOptions={populateCustomFiveYearsFutureDateOptions()}
              futureDateValidation={true}
            />
          </div>
        </div>
        <div>
          <label>
            {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.testAccesses}
          </label>
          {/* TODO (Future Feature): create API to get all different test types and map them in collapsing items */}
          <CollapsingItemContainer
            title={
              <div>
                <span style={styles.hiddenText}>
                  {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.testAccesses}
                </span>
                <span>
                  {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.eMIB}
                </span>
              </div>
            }
            body={
              <div style={styles.collapsingItemContainerBody}>
                {this.state.testAccessesOptions.map((test, id) => {
                  return (
                    <div key={id} style={styles.testAccessItemContainer}>
                      <input
                        id={`test-accesses-checkbox-${id}`}
                        type="checkbox"
                        checked={this.state.testAccessesOptions[id].checked}
                        style={styles.checkbox}
                        onChange={() => {
                          this.toggleTestAccessesCheckbox(id);
                        }}
                      />
                      <label
                        htmlFor={`test-accesses-checkbox-${id}`}
                        style={styles.collapsingItemLabel}
                      >
                        {this.props.currentLanguage === LANGUAGES.english
                          ? test.en_test_name
                          : test.fr_test_name}
                      </label>
                    </div>
                  );
                })}
              </div>
            }
          />
          {!this.state.isValidTestAccesses && (
            <label id="test-accesses-error" tabIndex={"0"} style={styles.errorMessage}>
              <span style={styles.hiddenText}>
                {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.testAccesses}
              </span>
              <span>
                {
                  LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                    .testAccessesError
                }
              </span>
            </label>
          )}
        </div>
        <div style={styles.buttonsContainer}>
          <OverlayTrigger
            trigger="hover"
            placement="top"
            overlay={
              <Popover style={styles.popover}>
                <div>
                  <p>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .refreshButton
                    }
                  </p>
                </div>
              </Popover>
            }
          >
            <button
              className="btn btn-secondary"
              type="button"
              style={styles.refreshButton}
              onClick={this.openClearAllFieldsPopup}
            >
              <FontAwesomeIcon icon={faUndo} style={styles.refreshButtonIcon} />
              <label id="refresh-button" style={styles.hiddenText}>
                {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.refreshButton}
              </label>
            </button>
          </OverlayTrigger>
          <OverlayTrigger
            trigger="hover"
            placement="top"
            overlay={
              <Popover style={styles.popover}>
                <div>
                  <p>
                    {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.saveButton}
                  </p>
                </div>
              </Popover>
            }
          >
            <button
              className="btn btn-primary"
              style={styles.saveButton}
              onClick={this.validateForm}
            >
              <FontAwesomeIcon icon={faSave} style={styles.saveButtonIcon} />
              <label id="save-button" style={styles.hiddenText}>
                {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.saveButton}
              </label>
            </button>
          </OverlayTrigger>
        </div>
        <PopupBox
          show={this.state.showClearFieldsPopup}
          handleClose={this.closeClearAllFieldsPopup}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
              .refreshConfirmationPopup.title
          }
          description={
            <div>
              <p>
                {
                  LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                    .refreshConfirmationPopup.description
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeClearAllFieldsPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faUndo}
          rightButtonIconCustomStyle={{ transform: "scale(1.5)", padding: "1.5px" }}
          rightButtonLabel={LOCALIZE.commons.confirm}
          rightButtonAction={this.handleRefreshAction}
        />
        <PopupBox
          show={this.state.showAssignTestPermissionsConfirmationPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.saveConfirmationPopup
              .title
          }
          description={
            <ConfirmTestAccessAssignmentPopup
              users={this.state.usersSelectedOptions}
              tests={this.state.selectedTestAccessesNames}
              testOrderNumber={this.state.testOrderNumber.value}
              staffingProcessNumber={this.state.staffingProcessNumber}
              departmentMinistryCode={this.state.departmentMinistryCode}
            />
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeAssignTestPermissionsConfirmationPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faSave}
          rightButtonLabel={LOCALIZE.commons.confirm}
          rightButtonAction={this.handleSubmitForm}
        />
        <PopupBox
          show={this.state.showTestAccessesGrantedPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.saveConfirmationPopup
              .testAccessesGrantedConfirmationPopup.title
          }
          description={
            <div>
              <p>
                {
                  LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                    .saveConfirmationPopup.testAccessesGrantedConfirmationPopup.description
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.closeTestAccessesGrantedConfirmationPopup}
        />
      </div>
    );
  }
}

export { AssignTestAccesses as unconnectedAssignTestAccesses };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    completeDatePicked: state.datePicker.completeDatePicked,
    completeDateValidState: state.datePicker.completeDateValidState,
    triggerPopulateTestPermissions: state.userPermissions.triggerPopulateTestPermissions
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getUsersBasedOnSpecifiedPermission,
      getNonPublicTests,
      resetDatePickedStates,
      grantTestPermission,
      updateTriggerPopulateTestPermissionsState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AssignTestAccesses);
