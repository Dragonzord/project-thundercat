import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { LANGUAGES } from "../../../modules/LocalizeRedux";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import PermissionRequests from "./PermissionRequests";
import ActivePermissions from "./ActivePermissions";
import {
  getPendingPermissions,
  getActivePermissions,
  getFoundActivePermissions,
  updateCurrentPermissionsPageState,
  updatePermissionsPageSizeState,
  updateSearchActivePermissionsStates
} from "../../../modules/PermissionsRedux";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tableContainer: {
    paddingRight: 48
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  }
};

class Permissions extends Component {
  static propTypes = {
    triggerPopulatePendingPermissions: PropTypes.bool.isRequired,
    // Props from Redux
    getPendingPermissions: PropTypes.func,
    getActivePermissions: PropTypes.func,
    getFoundActivePermissions: PropTypes.func,
    updateCurrentPermissionsPageState: PropTypes.func,
    updatePermissionsPageSizeState: PropTypes.func,
    updateSearchActivePermissionsStates: PropTypes.func
  };

  state = {
    pendingPermissions: [],
    activePermissions: [],
    activePermissionsCount: 0,
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    resultsFound: 0,
    clearSearchTriggered: false,
    currentlyLoading: false
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentPermissionsPageState(1);
    // initialize search permissionsKeyword, permissionsActiveSearch redux states
    this.props.updateSearchActivePermissionsStates("", false);
  };

  componentDidUpdate = prevProps => {
    // if language gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populatePendingPermissions();
      this.populateActivePermissionsBasedOnActiveSearch();
    }
    // if triggerPopulatePendingPermissions gets updated
    if (
      prevProps.triggerPopulatePendingPermissions !== this.props.triggerPopulatePendingPermissions
    ) {
      this.populatePendingPermissions();
      this.populateActivePermissionsBasedOnActiveSearch();
    }
    // if permissionsPaginationPage gets updated
    if (prevProps.permissionsPaginationPage !== this.props.permissionsPaginationPage) {
      this.populateActivePermissionsBasedOnActiveSearch();
    }
    // if permissionsPaginationPageSize get updated
    if (prevProps.permissionsPaginationPageSize !== this.props.permissionsPaginationPageSize) {
      this.populateActivePermissionsBasedOnActiveSearch();
    }
    // if search permissionsKeyword gets updated
    if (prevProps.permissionsKeyword !== this.props.permissionsKeyword) {
      // if permissionsKeyword redux state is empty
      if (this.props.permissionsKeyword !== "") {
        this.populateFoundActivePermissions();
      } else if (this.props.permissionsKeyword === "" && this.props.permissionsActiveSearch) {
        this.populateFoundActivePermissions();
      }
    }
    // if permissionsActiveSearch gets updated
    if (prevProps.permissionsActiveSearch !== this.props.permissionsActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.permissionsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateActivePermissions();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundActivePermissions();
      }
    }
  };

  // populating active permissions based on the permissionsActiveSearch redux state
  populateActivePermissionsBasedOnActiveSearch = () => {
    // current search
    if (this.props.permissionsActiveSearch) {
      this.populateFoundActivePermissions();
      // no current search
    } else {
      this.populateActivePermissions();
    }
  };

  //populating all existing pending permissions
  populatePendingPermissions = () => {
    let pendingPermissions = [];
    this.props.getPendingPermissions(localStorage.getItem("auth_token")).then(response => {
      // looping in all existing pending permissions
      for (let i = 0; i < response.length; i++) {
        // pushing needed data in pendingPermissions array
        pendingPermissions.push({
          username: response[i].username,
          permission_requested_id: response[i].permission_requested,
          permission_requested:
            this.props.currentLanguage === LANGUAGES.english
              ? response[i].en_name
              : response[i].fr_name,
          first_name: response[i].first_name,
          last_name: response[i].last_name,
          goc_email: response[i].goc_email,
          pri_or_military_nbr: response[i].pri_or_military_nbr,
          supervisor: response[i].supervisor,
          supervisor_email: response[i].supervisor_email,
          rationale: response[i].rationale,
          permission_request_id: response[i].permission_request_id
        });
      }
      // saving results in state
      this.setState({ pendingPermissions: pendingPermissions });
    });
  };

  //populating all existing active permissions
  populateActivePermissions = () => {
    // setting currentlyLoading to true as soon as this function is called
    this.setState({ currentlyLoading: true }, () => {
      let activePermissions = [];
      this.props
        .getActivePermissions(
          localStorage.getItem("auth_token"),
          this.props.permissionsPaginationPage,
          this.props.permissionsPaginationPageSize
        )
        .then(response => {
          this.populateActivePermissionsObject(activePermissions, response);
        })
        .then(() => {
          if (this.state.clearSearchTriggered) {
            // go back to the first page to avoid display bugs
            this.props.updateCurrentPermissionsPageState(1);
            this.setState({ clearSearchTriggered: false });
          }
        })
        .then(() => {
          // setting currentlyLoading to false as soon as all data has been provided to the interface
          this.setState({ currentlyLoading: false });
        });
    });
  };

  // populating all found active permissions based on a search
  populateFoundActivePermissions = () => {
    // setting currentlyLoading to true as soon as this function is called
    this.setState({ currentlyLoading: true }, () => {
      let activePermissions = [];
      this.props
        .getFoundActivePermissions(
          localStorage.getItem("auth_token"),
          this.props.currentLanguage,
          this.props.permissionsKeyword,
          this.props.permissionsPaginationPage,
          this.props.permissionsPaginationPageSize
        )
        .then(response => {
          // there are no results found
          if (response[0] === "no results found") {
            this.setState({
              activePermissions: [],
              numberOfPages: 1,
              nextPageNumber: 0,
              previousPageNumber: 0,
              resultsFound: 0
            });
            // there is at least one result found
          } else {
            this.populateActivePermissionsObject(activePermissions, response);
            this.setState({ resultsFound: response.count });
          }
        })
        .then(() => {
          // setting currentlyLoading to false as soon as all data has been provided to the interface
          this.setState({ currentlyLoading: false }, () => {
            // if there is at least one result found
            if (activePermissions.length > 0) {
              // focusing on results found label
              document.getElementById("results-found-label").focus();
              // no results found
            } else {
              // focusing on results found label
              document.getElementById("no-results-table-row").focus();
            }
          });
        });
    });
  };

  populateActivePermissionsObject = (activePermissions, response) => {
    // looping in response given
    for (let i = 0; i < response.results.length; i++) {
      // pushing needed data in activePermissions array
      activePermissions.push({
        username: response.results[i].user,
        user_permission_id: response.results[i].user_permission_id,
        permission:
          this.props.currentLanguage === LANGUAGES.english
            ? response.results[i].en_name
            : response.results[i].fr_name,
        first_name: response.results[i].first_name,
        last_name: response.results[i].last_name,
        permission_id: response.results[i].permission,
        goc_email: response.results[i].goc_email,
        pri_or_military_nbr: response.results[i].pri_or_military_nbr,
        supervisor: response.results[i].supervisor,
        supervisor_email: response.results[i].supervisor_email
      });
    }

    // saving results in state
    this.setState({
      activePermissions: activePermissions,
      numberOfPages: Math.ceil(response.count / this.props.permissionsPaginationPageSize),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  render() {
    const TABS = [
      {
        key: "permission-requests",
        tabName: LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.title,
        body: (
          <PermissionRequests
            pendingPermissions={this.state.pendingPermissions}
            populatePendingPermissions={this.populatePendingPermissions}
            populateActivePermissions={this.populateActivePermissions}
          />
        )
      },
      {
        key: "active-permissions",
        tabName: LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.title,
        body: (
          <ActivePermissions
            activePermissions={this.state.activePermissions}
            populateActivePermissions={this.populateActivePermissions}
            populateFoundActivePermissions={this.populateFoundActivePermissions}
            numberOfPages={this.state.numberOfPages}
            nextPageNumber={this.state.nextPageNumber}
            previousPageNumber={this.state.previousPageNumber}
            resultsFound={this.state.resultsFound}
            currentlyLoading={this.state.currentlyLoading}
          />
        )
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.systemAdministrator.sideNavItems.permissions}</h2>
          <p>{LOCALIZE.systemAdministrator.permissions.description}</p>
        </div>
        <div style={styles.tableContainer}>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey="permission-requests"
                id="permissions-tabs"
                style={styles.tabNavigation}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    permissionsPaginationPage: state.userPermissions.permissionsPaginationPage,
    permissionsPaginationPageSize: state.userPermissions.permissionsPaginationPageSize,
    permissionsKeyword: state.userPermissions.permissionsKeyword,
    permissionsActiveSearch: state.userPermissions.permissionsActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getPendingPermissions,
      getActivePermissions,
      getFoundActivePermissions,
      updateCurrentPermissionsPageState,
      updatePermissionsPageSizeState,
      updateSearchActivePermissionsStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Permissions);
