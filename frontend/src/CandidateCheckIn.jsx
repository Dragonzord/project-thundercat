import React, { Component } from "react";
import LOCALIZE from "./text_resources";
import PropTypes from "prop-types";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Button } from "react-bootstrap";
import PopupBox, { BUTTON_TYPE } from "./components/commons/PopupBox";
import { styles } from "./TestAdministration";
import updateCheckInRoom from "./modules/UpdateCheckInRoomRedux";

class CandidateCheckIn extends Component {
  static propTypes = {
    // Props from Redux
    updateCheckInRoom: PropTypes.func
  };

  state = {
    showCheckInPopup: false,
    testAccessCode: "",
    isInvalidTestAccessCode: false
  };

  handleCheckInPopup = () => {
    this.setState({ ...this.state, showCheckInPopup: true });
  };

  handleCheckin = () => {
    const { testAccessCode } = this.state;
    const { username } = this.props;

    this.props.updateCheckInRoom(testAccessCode, username).then(response => {
      if (response.ok) {
        this.setState({
          ...this.state,
          showCheckInPopup: false
        });
        this.props.handleCheckIn();
      } else {
        this.setState({ ...this.state, isInvalidTestAccessCode: true });
      }
    });
  };

  getTestAccessCode = event => {
    this.setState({ testAccessCode: event.target.value });
  };

  closeDialog = () => {
    this.setState({
      ...this.state,
      testAccessCode: "",
      showCheckInPopup: false,
      isInvalidTestAccessCode: false
    });
  };

  render() {
    return (
      <div id="unit-test-candidate-check-in">
        {this.props.isCheckedIn ? (
          <div id="unit-test-current-room-code">
            <p>
              {LOCALIZE.candidateCheckIn.checkedInText}
              <strong>{this.state.testAccessCode}</strong>
            </p>
          </div>
        ) : this.props.isLoaded ? (
          <div style={styles.checkInBtn} id="unit-test-check-in-button">
            <Button
              onClick={this.handleCheckInPopup}
              className="btn btn-primary btn-wide"
              style={styles.startTestBtn}
            >
              {LOCALIZE.candidateCheckIn.button}
            </Button>
          </div>
        ) : (
          <div style={styles.checkInBtn} id="unit-test-check-in-loading-button">
            <Button
              onClick={this.handleCheckInPopup}
              className="btn btn-primary btn-wide"
              style={styles.startTestBtn}
              disabled
            >
              {LOCALIZE.candidateCheckIn.loading}
            </Button>
          </div>
        )}
        <PopupBox
          show={this.state.showCheckInPopup}
          title={LOCALIZE.candidateCheckIn.popup.title}
          handleClose={() => {}}
          description={
            <div style={styles.popupWidth}>
              <p>{LOCALIZE.candidateCheckIn.popup.description}</p>

              <table style={styles.inputsTable}>
                <tbody>
                  <tr>
                    <th>
                      <div
                        style={
                          this.state.isInvalidTestAccessCode
                            ? styles.inputsTableTitlesWhenInvalid
                            : styles.inputsTableTitles
                        }
                      >
                        <label htmlFor="test-access-code-field">
                          {LOCALIZE.candidateCheckIn.popup.textLabel}
                        </label>
                      </div>
                    </th>
                    <th>
                      <div style={styles.inputsTableInputs}>
                        <input
                          aria-required={"false"}
                          id="test-access-field"
                          type="text"
                          style={
                            this.state.isInvalidTestAccessCode
                              ? { ...styles.textInput, ...styles.invalidInput }
                              : styles.textInput
                          }
                          value={this.state.testAccessCode}
                          onChange={this.getTestAccessCode}
                        />
                        <div>
                          {this.state.isInvalidTestAccessCode && (
                            <label id="invalid-test-access-error" style={styles.errorMessage}>
                              {LOCALIZE.candidateCheckIn.popup.textLabelError}
                            </label>
                          )}
                        </div>
                      </div>
                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeDialog}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.candidateCheckIn.button}
          rightButtonAction={this.handleCheckin}
        />
      </div>
    );
  }
}

export { CandidateCheckIn as UnconnectedCandidateCheckIn };

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({ updateCheckInRoom }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CandidateCheckIn);
