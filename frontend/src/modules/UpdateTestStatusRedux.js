//static api urls for updating test status
const START_STATUS = "/api/activate-test/"; //starts the test
const SUBMIT_STATUS = "/api/submit-test/";
const INACTIVE_STATUS = "/api/expire-test-inactivity/";
const TIMEOUT_STATUS = "/api/timeout-test/";
const QUIT_STATUS = "/api/quit-test/";

//pass in strict api urls to update the test status
function updateBackendTest(statusURL, test_id, startTime) {
  let url = statusURL + `?test_id=${test_id}&start_date=${new Date(startTime).getTime()}`;
  if (statusURL !== START_STATUS) {
    url = statusURL + `?test_id=${test_id}`;
  }

  return async function() {
    let tests = await fetch(url, {
      method: "GET",
      headers: {
        Authorization: "JWT " + localStorage.getItem("auth_token"),
        cache: "default"
      }
    });
    return await tests;
  };
}

export default updateBackendTest;
export { updateBackendTest };
export { START_STATUS, SUBMIT_STATUS, INACTIVE_STATUS, TIMEOUT_STATUS, QUIT_STATUS };
