// getting all tests where "is_public" is set to false
function getNonPublicTests(token) {
  return async function() {
    let response = await fetch("/api/get-non-public-tests", {
      method: "GET",
      headers: {
        Authorization: "JWT " + token,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// getting the test name based on the given test internal name
function getTestName(token, testInternalName) {
  return async function() {
    let response = await fetch(`/api/get-test-name/?test_name=${testInternalName}`, {
      method: "GET",
      headers: {
        Authorization: "JWT " + token,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// getting the test internal name based on the given test name (English or French name)
function getTestInternalName(token, testName) {
  return async function() {
    let response = await fetch(`/api/get-test-internal-name/?text_detail=${testName}`, {
      method: "GET",
      headers: {
        Authorization: "JWT " + token,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

export default getNonPublicTests;
export { getTestName, getTestInternalName };
