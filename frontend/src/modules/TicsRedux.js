// getting tics data
function getTicsData(token, orderNumber) {
  return async function() {
    let response = await fetch(`/api/get-tics-data/?order_number=${orderNumber}`, {
      method: "GET",
      headers: {
        Authorization: "JWT " + token,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    // no results found: return only response
    if (response.status === 204) {
      return response;
      //  there is a result found or there is an error: return json response
    } else {
      let responseJson = await response.json();
      return responseJson;
    }
  };
}

export default getTicsData;
