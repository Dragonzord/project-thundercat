//pass in strict api urls to update the test status
function updateCheckInRoom(testAccessCode, username) {
  let url = "/api/room-check-in?test_access_code=" + testAccessCode + "&username=" + username;

  return async function() {
    let tests = await fetch(url, {
      method: "GET",
      headers: {
        Authorization: "JWT " + localStorage.getItem("auth_token"),
        cache: "default"
      }
    });
    return await tests;
  };
}

export default updateCheckInRoom;
