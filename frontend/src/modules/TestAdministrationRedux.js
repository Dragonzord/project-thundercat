// gets randomly generated test access code
function getNewTestAccessCode(
  token,
  isTa,
  taUsername,
  staffingProcessNumber,
  testSessionLanguage,
  test
) {
  if (isTa) {
    return async function() {
      let response = await fetch(
        `/api/get-new-test-access-code/?ta_username_id=${taUsername}&staffing_process_number=${staffingProcessNumber}&test_session_language=${testSessionLanguage}&test_id=${test}`,
        {
          method: "GET",
          headers: {
            Authorization: "JWT " + token,
            Accept: "application/json",
            "Content-Type": "application/json",
            cache: "default"
          }
        }
      );
      let responseJson = await response.json();
      return responseJson;
    };
    // TODO: log the user out and redirect to login page, since it's not a TA
  } else {
    return;
  }
}

// delete test access code
function deleteTestAccessCode(token, isTa, testAccessCode, taUsername) {
  if (isTa) {
    return async function() {
      let response = await fetch(
        `/api/delete-test-access-code/?test_access_code=${testAccessCode}&ta_username_id=${taUsername}`,
        {
          method: "GET",
          headers: {
            Authorization: "JWT " + token,
            Accept: "application/json",
            "Content-Type": "application/json",
            cache: "default"
          }
        }
      );
      let responseJson = await response.json();
      return responseJson;
    };
    // TODO: log the user out and redirect to login page, since it's not a TA
  } else {
    return;
  }
}

// get active test access code data for a specified TA username
function getActiveTestAccessCodeData(token, isTa, taUsername) {
  if (isTa) {
    return async function() {
      let response = await fetch(`/api/get-active-test_access_code/?ta_username_id=${taUsername}`, {
        method: "GET",
        headers: {
          Authorization: "JWT " + token,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      });
      let responseJson = await response.json();
      return responseJson;
    };
    // TODO: log the user out and redirect to login page, since it's not a TA
  } else {
    return;
  }
}

export default getNewTestAccessCode;
export { deleteTestAccessCode, getActiveTestAccessCodeData };
