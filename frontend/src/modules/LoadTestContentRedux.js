// Action Types
const UPDATE_TEST_META_DATA = "emibInbox/UPDATE_TEST_META_DATA";
const UPDATE_TEST_BACKGROUND = "emibInbox/UPDATE_TEST_BACKGROUND";
const RESET_STATE = "testContent/RESET_STATE";

// Action Creators
const updateTestMetaDataState = testMetaData => ({ type: UPDATE_TEST_META_DATA, testMetaData });
const updateTestBackgroundState = testBackground => ({
  type: UPDATE_TEST_BACKGROUND,
  testBackground
});
const resetMetaDataState = () => ({ type: RESET_STATE });

// API Calls
const getTestMetaData = testName => {
  const AUTH_TOKEN = localStorage.getItem("auth_token");
  return async function() {
    let headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
      cache: "default"
    };
    if (AUTH_TOKEN) {
      headers.Authorization = "JWT " + AUTH_TOKEN;
    }
    let metaDataContent = await fetch(`/api/test-meta-data/?test_name=${testName}`, {
      method: "GET",
      headers: headers
    });
    const json = await metaDataContent.json();
    return json;
  };
};
const getTestContent = testName => {
  const AUTH_TOKEN = localStorage.getItem("auth_token");
  return async function() {
    let headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
      cache: "default"
    };
    if (AUTH_TOKEN) {
      headers.Authorization = "JWT " + AUTH_TOKEN;
    }
    let testContent = await fetch(`/api/test-questions/?test_name=${testName}`, {
      method: "GET",
      headers: headers
    });
    return await testContent.json();
  };
};

// Initial State
// isMetaLoading - boolean determining if the metadata has been initialized
// testMetaData - contains the name of the test and the overview page content
// testBackground - contains all the background information
// addressBook - an object containing arrays addressBookShapes
const initialState = {
  isMetaLoading: true,
  testMetaData: {},
  testBackground: {},
  addressBook: { en: [], fr: [] }
};

// Reducer
const loadTestContent = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_TEST_META_DATA:
      return {
        ...state,
        testMetaData: action.testMetaData,
        isMetaLoading: false
      };
    case UPDATE_TEST_BACKGROUND:
      return {
        ...state,
        testBackground: action.testBackground,
        addressBook: getAddressBook(action.testBackground)
      };
    case RESET_STATE:
      return {
        ...initialState
      };

    default:
      return state;
  }
};

// Small conversion of the addressbook
const getAddressBook = testBackground => {
  let enAddressBook = [];
  let frAddressBook = [];

  const enAddressArray = testBackground.en.address_book[0].contact;
  const frAddressArray = testBackground.fr.address_book[0].contact;

  for (let i = 0; i < enAddressArray.length; i++) {
    let enContact = enAddressArray[i];
    let frContact = frAddressArray[i];
    enAddressBook.push({ value: enContact.true_id, label: enContact.text });
    frAddressBook.push({ value: frContact.true_id, label: frContact.text });
  }

  return { en: enAddressBook, fr: frAddressBook };
};

// Filters - used to get a filtered version of the state in the
// mapStateToProps in the components.

// Returns the address book in the current language.
export const getAddressInCurrentLanguage = state => {
  const lang = state.localize.language;
  const addressBook = state.loadTestContent.addressBook;
  return addressBook[lang];
};

// Returns the background info in the current language.
export const getBackgroundInCurrentLanguage = state => {
  const lang = state.localize.language;
  const testBackground = state.loadTestContent.testBackground;
  return testBackground[lang].sections[0].section;
};

// Returns the time allotted for the test in minutes.
export const getTotalTestTime = state => {
  return state.loadTestContent.testMetaData.default_time;
};

export default loadTestContent;
export {
  initialState,
  updateTestMetaDataState,
  getTestMetaData,
  updateTestBackgroundState,
  getTestContent,
  resetMetaDataState
};
