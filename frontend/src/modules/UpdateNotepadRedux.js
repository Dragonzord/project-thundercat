//update the notepad in the backened
function updateNotepad(assigned_test_id, notepadContent) {
  let url =
    "/api/update_notepad" +
    `?assigned_test_id=${assigned_test_id}&notepad=${encodeURIComponent(notepadContent)}`;

  return async function() {
    let tests = await fetch(url, {
      method: "GET",
      headers: {
        Authorization: "JWT " + localStorage.getItem("auth_token"),
        cache: "default"
      }
    });
    return await tests;
  };
}

//update the notepad in the backened
function getNotepad(assigned_test_id) {
  let url = "/api/get_notepad?assigned_test_id=" + assigned_test_id;

  return async function() {
    let tests = await fetch(url, {
      method: "GET",
      headers: {
        Authorization: "JWT " + localStorage.getItem("auth_token"),
        cache: "default"
      }
    });
    let response = await tests.json();
    return response;
  };
}

export { updateNotepad, getNotepad };
