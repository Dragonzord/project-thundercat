// getting the assigned test of the specified user
const getAssignedTests = (username, token) => {
  return async function() {
    let tests = await fetch(`/api/assigned-tests/?username=${username}`, {
      method: "GET",
      headers: {
        Authorization: "JWT " + token,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let testsJson = await tests.json();
    return testsJson;
  };
};

// assigning a test to a specified user based on the test ID and the scheduled date
const assignTest = (token, test_id, username_id, scheduled_date) => {
  return async function() {
    let assignTest = await fetch(
      "/api/assign-test" +
        `/?test_id=${test_id}&username_id=${username_id}&scheduled_date=${scheduled_date}`,
      {
        method: "GET",
        headers: {
          Authorization: "JWT " + token,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return assignTest;
  };
};

// getting all the historical assigned tests of a specified user based on the test ID
const getHistoricalAssignedTests = (token, username, test_id) => {
  return async function() {
    let getHistoricalAssignedTests = await fetch(
      `/api/historical-assigned-tests/?username=${username}&test_id=${test_id}`,
      {
        method: "GET",
        headers: {
          Authorization: "JWT " + token,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let response = await getHistoricalAssignedTests.json();
    return response;
  };
};

export default getAssignedTests;
export { getAssignedTests, assignTest, getHistoricalAssignedTests };
