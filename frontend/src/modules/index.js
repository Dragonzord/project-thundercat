import { combineReducers } from "redux";
import localize from "./LocalizeRedux";
import login from "./LoginRedux";
import testStatus from "./TestStatusRedux";
import sampleTestStatus from "./SampleTestStatusRedux";
import emibInbox from "./EmibInboxRedux";
import loadTestContent from "./LoadTestContentRedux";
import userPermissions from "./PermissionsRedux";
import notepad from "./NotepadRedux";
import datePicker from "./DatePickerRedux";
import userProfile from "./UserProfileRedux";
import user from "./UserRedux";
import scorer from "./ScorerRedux";

export default combineReducers({
  localize,
  login,
  emibInbox,
  testStatus,
  sampleTestStatus,
  loadTestContent,
  userPermissions,
  notepad,
  datePicker,
  userProfile,
  user,
  scorer
});
