// Scorer Assigned Tests Pagination
export const SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE =
  "scorer/SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE";
export const SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE_SIZE =
  "scorer/SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE_SIZE";
const RESET_STATE = "scorer/REST_SCORER_STATE";

// calls
// update pagination page state (scorer assigned tests)
const updateCurrentScorerAssignedTestPageState = scorerPaginationPage => ({
  type: SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE,
  scorerPaginationPage
});
// update pagination pageSize state (scorer assigned tests)
const updateScorerAssignedTestPageSizeState = scorerPaginationPageSize => ({
  type: SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE_SIZE,
  scorerPaginationPageSize
});
const resetScorerState = () => ({
  type: RESET_STATE
});

// get scorer assigned tests (all tests assigned to this scorer)
function getAssignedTests(token, username, page, pageSize) {
  return async function() {
    let response = await fetch(
      "/api/get_scorer_assigned_tests" +
        `?scorer_username=${username}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: "JWT " + token,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
}

// Initial State
const initialState = {
  scorerPaginationPage: 1,
  scorerPaginationPageSize: 25
};

// Reducer
const scorer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE:
      return {
        ...state,
        scorerPaginationPage: action.scorerPaginationPage
      };
    case SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        scorerPaginationPageSize: action.scorerPaginationPageSize
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default scorer;
export {
  getAssignedTests,
  updateCurrentScorerAssignedTestPageState,
  updateScorerAssignedTestPageSizeState,
  resetScorerState
};
