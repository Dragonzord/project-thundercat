// Action Types
const SET_CURRENT_TEST = "testStatus/SET_CURRENT_TEST";
const SET_PAGE = "testStatus/SET_PAGE";
const ACTIVATE_TEST = "testStatus/ACTIVATE_TEST";
const START_TEST = "testStatus/START_TEST";
const DEACTIVATE_TEST = "testStatus/DEACTIVATE_TEST";
const QUIT_TEST = "testStatus/QUIT_TEST";
const TIMEOUT_TEST = "testStatus/TIMEOUT_TEST";
const RESET_STATE = "testStatus/RESET_STATE";

// Action Creators
const setCurrentTest = (currentAssignedTestId, testId, startTime) => ({
  type: SET_CURRENT_TEST,
  currentAssignedTestId,
  testId,
  startTime
});
const setCurrentPage = page => ({
  type: SET_PAGE,
  page
});
const activateTest = () => ({ type: ACTIVATE_TEST });
const startTest = (timeLimit, currentAssignedTestId, startTime) => ({
  type: START_TEST,
  timeLimit,
  currentAssignedTestId,
  startTime
});
const deactivateTest = () => ({ type: DEACTIVATE_TEST });
const quitTest = () => ({ type: QUIT_TEST });
const timeoutTest = () => ({ type: TIMEOUT_TEST });
const resetTestStatusState = () => ({ type: RESET_STATE });

const PAGES = {
  preTest: "preTest",
  instructions: "instructions",
  background: "background",
  inbox: "inbox",
  confirm: "confirm",
  quit: "quit",
  timeout: "timeout"
};

// Initial State
const initialState = {
  isTestActive: false,
  currentPage: PAGES.preTest,
  startTime: null,
  timeLimit: null,
  currentAssignedTestId: null,
  currentTestId: null,
  isTestStarted: false
};

// Reducer
const testStatus = (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_TEST:
      //set current test in local storage so that url change doesn't
      //remove the test type information
      return {
        ...state,
        currentAssignedTestId: action.currentAssignedTestId,
        currentTestId: action.testId,
        startTime: action.startTime
      };
    case SET_PAGE:
      return {
        ...state,
        currentPage: action.page
      };
    case ACTIVATE_TEST:
      return {
        ...state,
        isTestActive: true
      };
    case START_TEST:
      const limit = action.timeLimit === undefined ? null : action.timeLimit;

      return {
        ...state,
        isTestStarted: true,
        isTestActive: true,
        startTime: action.startTime,
        timeLimit: limit,
        currentPage: PAGES.background
      };
    case DEACTIVATE_TEST:
      // Ensure local storage is cleaned up once test is complete.
      return {
        ...state,
        isTestActive: false,
        isTestStarted: false,
        currentPage: PAGES.confirm,
        currentAssignedTestId: null,
        currentTestId: null,
        startTime: null
      };
    case QUIT_TEST:
      // Ensure local storage is cleaned up once the candidate quit the test.
      return {
        ...state,
        isTestActive: false,
        isTestStarted: false,
        currentPage: PAGES.quit,
        currentAssignedTestId: null,
        currentTestId: null,
        startTime: null
      };
    case TIMEOUT_TEST:
      // Ensure local storage is cleaned up once the candidate is timed out of the test.
      return {
        ...state,
        isTestActive: false,
        isTestStarted: false,
        currentPage: PAGES.timeout,
        currentTestId: null,
        currentAssignedTestId: null,
        startTime: null
      };
    case RESET_STATE:
      // Ensure local storage is cleaned up once the candidate is timed out of the test.
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default testStatus;
export {
  setCurrentTest,
  setCurrentPage,
  activateTest,
  startTest,
  initialState,
  deactivateTest,
  quitTest,
  timeoutTest,
  resetTestStatusState,
  PAGES
};
