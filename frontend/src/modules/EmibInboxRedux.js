import { SET_LANGUAGE } from "./LocalizeRedux";
import { ACTION_TYPE } from "../components/eMIB/constants";
import {
  addEmailResponse,
  addTaskResponse,
  updateEmailResponse,
  updateTaskResponse,
  deleteEmailResponse,
  deleteTaskResponse
} from "./UpdateResponseRedux";

// Initializers
export const initializeEmailSummaries = length => {
  let emailSummaries = [];
  for (let i = 0; i < length; i++) {
    emailSummaries.push({ isRead: false, emailCount: 0, taskCount: 0 });
  }
  return emailSummaries;
};

const initializeEmailActions = length => {
  let emailActions = [];
  for (let i = 0; i < length; i++) {
    emailActions.push([]);
  }
  return emailActions;
};

// Action Types
const READ_EMAIL = "emibInbox/READ_EMAIL";
const ADD_EMAIL = "emibInbox/ADD_EMAIL";
const ADD_TASK = "emibInbox/ADD_TASK";
const UPDATE_EMAIL = "emibInbox/UPDATE_EMAIL";
const UPDATE_TASK = "emibInbox/UPDATE_TASK";
const DELETE_EMAIL = "emibInbox/DELETE_EMAIL";
const DELETE_TASK = "emibInbox/DELETE_TASK";
const LOAD_RESPONSES = "emibInbox/LOAD_RESPONSES";
const CHANGE_CURRENT_EMAIL = "emibInbox/CHANGE_CURRENT_EMAIL";
const UPDATE_EMAILS_CONTENT = "emibInbox/UPDATE_EMAILS_CONTENT";
const SET_EN_EMAILS = "emibInbox/SET_EN_EMAILS";
const SET_FR_EMAILS = "emibInbox/SET_FR_EMAILS";
const SET_ASSIGNED_QUESTION_IDS = "emibInbox/SET_ASSIGNED_QUESTION_IDS";
const SET_RESPONSE_DB_ID = "emibInbox/SET_RESPONSE_DB_ID";
const RESET_STATE = "emibInbox/RESET_STATE";

// Action Creators
// updating email states (emails, emailsEN and emailsFR)
const updateEmailsEnState = emailsEN => ({ type: SET_EN_EMAILS, emailsEN });
const updateEmailsFrState = emailsFR => ({ type: SET_FR_EMAILS, emailsFR });
const updateEmailsState = emails => ({ type: UPDATE_EMAILS_CONTENT, emails });

const readEmail = emailIndex => ({ type: READ_EMAIL, emailIndex });
// emailIndex refers to the index of the original parent email and emailAction is an actionShape
const addEmail = (emailIndex, emailAction, dispatch) => ({
  type: ADD_EMAIL,
  emailIndex,
  emailAction,
  dispatch
});
// emailIndex refers to the index of the original parent email and taskAction is an actionShape
const addTask = (emailIndex, taskAction, dispatch) => ({
  type: ADD_TASK,
  emailIndex,
  taskAction,
  dispatch
});
// emailIndex refers to the index of the original parent email, responseId is the id of the response that is being edited and emailAction is an actionShape
const updateEmail = (emailIndex, responseId, emailAction) => ({
  type: UPDATE_EMAIL,
  emailIndex,
  responseId,
  emailAction
});
// emailIndex refers to the index of the original parent email, responseId is the id of the response that is being edited, and taskAction is an actionShape
const updateTask = (emailIndex, responseId, taskAction) => ({
  type: UPDATE_TASK,
  emailIndex,
  responseId,
  taskAction
});
// emailIndex refers to the index of the original parent email and responseId is the id of the response that is being deleted
const deleteEmail = (emailIndex, responseId) => ({
  type: DELETE_EMAIL,
  emailIndex,
  responseId
});
// emailIndex refers to the index of the original parent email and responseId is the id of the response that is being deleted
const deleteTask = (emailIndex, responseId) => ({
  type: DELETE_TASK,
  emailIndex,
  responseId
});

const loadResponses = responses => ({
  type: LOAD_RESPONSES,
  responses
});

// emailIndex refers to the index of the currently visible email
const changeCurrentEmail = emailIndex => ({
  type: CHANGE_CURRENT_EMAIL,
  emailIndex
});

// set the db ids for the assigned questions; needed for modifying responses
const setAssignedQuestionIds = assignedQuestionIds => ({
  type: SET_ASSIGNED_QUESTION_IDS,
  assignedQuestionIds
});

// set the id for a response, once the DB saves the resonse
const setResponseDbId = (emailIndex, responseId, trueId) => ({
  type: SET_RESPONSE_DB_ID,
  emailIndex,
  responseId,
  trueId
});

// reset to the initial state. used on end of emib
const resetInboxState = () => ({
  type: RESET_STATE
});

// Initial State
// emails - represents an array of emailShape objects in the currently selected language.
// emailSummaries - represents an array of objects indicating read state of each email.
// emailActions - represents an array of arrays, each array contains actionShape objects, representing an ACTION_TYPE.
const initialState = {
  // Loads emails from a static JSON file until an API exists.
  emails: {},
  emailsEN: {},
  emailsFR: {},
  emailSummaries: [],
  emailActions: [],
  currentEmail: 0,
  assignedQuestionIds: [],
  isTestLoadedFromDB: false
};

// Reducer
const emibInbox = (state = initialState, action) => {
  switch (action.type) {
    case SET_LANGUAGE:
      return {
        ...state,
        emails: action.language === "fr" ? state.emailsFR : state.emailsEN
      };
    case UPDATE_EMAILS_CONTENT:
      return {
        ...state,
        emails: action.emails,
        emailSummaries: initializeEmailSummaries(action.emails.length),
        emailActions: initializeEmailActions(action.emails.length)
      };
    case SET_EN_EMAILS:
      return {
        ...state,
        emailsEN: action.emailsEN
      };
    case SET_FR_EMAILS:
      return {
        ...state,
        emailsFR: action.emailsFR
      };
    case READ_EMAIL:
      let updatedEmailSummaries = Array.from(state.emailSummaries);
      updatedEmailSummaries[action.emailIndex].isRead = true;
      return {
        ...state,
        emailSummaries: updatedEmailSummaries
      };
    case ADD_EMAIL:
      let modifiedEmailSummaries = Array.from(state.emailSummaries);
      modifiedEmailSummaries[action.emailIndex].emailCount++;

      let modifiedEmailActions = Array.from(state.emailActions);
      modifiedEmailActions[action.emailIndex].push({
        ...action.emailAction,
        actionType: ACTION_TYPE.email
      });
      let newActionIndex = modifiedEmailActions[action.emailIndex].length - 1;
      addEmailResponse(
        state.assignedQuestionIds[action.emailIndex],
        action.emailIndex,
        newActionIndex,
        action.emailAction,
        action.dispatch
      );
      return {
        ...state,
        emailSummaries: modifiedEmailSummaries,
        emailActions: modifiedEmailActions
      };
    case ADD_TASK:
      let duplicatedEmailSummaries = Array.from(state.emailSummaries);
      duplicatedEmailSummaries[action.emailIndex].taskCount++;

      let duplicatedEmailActions = Array.from(state.emailActions);
      duplicatedEmailActions[action.emailIndex].push({
        ...action.taskAction,
        actionType: ACTION_TYPE.task
      });
      let nextActionIndex = duplicatedEmailActions[action.emailIndex].length - 1;
      addTaskResponse(
        state.assignedQuestionIds[action.emailIndex],
        action.emailIndex,
        nextActionIndex,
        action.taskAction,
        action.dispatch
      );
      return {
        ...state,
        emailSummaries: duplicatedEmailSummaries,
        emailActions: duplicatedEmailActions
      };
    case UPDATE_EMAIL:
      let updatedEmailActions = Array.from(state.emailActions);
      let trueId1 = updatedEmailActions[action.emailIndex][action.responseId].actionTrueId;
      updatedEmailActions[action.emailIndex][action.responseId] = {
        ...action.emailAction,
        actionType: ACTION_TYPE.email,
        actionTrueId: trueId1
      };
      updateEmailResponse(trueId1, action.emailAction);
      return {
        ...state,
        emailActions: updatedEmailActions
      };
    case UPDATE_TASK:
      let emailActionsUpdated = Array.from(state.emailActions);
      let trueId2 = emailActionsUpdated[action.emailIndex][action.responseId].actionTrueId;
      emailActionsUpdated[action.emailIndex][action.responseId] = {
        ...action.taskAction,
        actionType: ACTION_TYPE.task,
        actionTrueId: trueId2
      };
      updateTaskResponse(trueId2, action.taskAction);
      return {
        ...state,
        emailActions: emailActionsUpdated
      };
    case DELETE_EMAIL:
      let purgedEmailSummaries = Array.from(state.emailSummaries);
      purgedEmailSummaries[action.emailIndex].emailCount--;

      let purgedEmailActions = Array.from(state.emailActions);
      let trueId3 = purgedEmailActions[action.emailIndex][action.responseId].actionTrueId;
      deleteEmailResponse(trueId3);
      purgedEmailActions[action.emailIndex].splice(action.responseId, 1);
      return {
        ...state,
        emailSummaries: purgedEmailSummaries,
        emailActions: purgedEmailActions
      };
    case DELETE_TASK:
      let purifiedEmailSummaries = Array.from(state.emailSummaries);
      purifiedEmailSummaries[action.emailIndex].taskCount--;

      let purifiedEmailActions = Array.from(state.emailActions);
      let trueId4 = purifiedEmailActions[action.emailIndex][action.responseId].actionTrueId;
      deleteTaskResponse(trueId4);
      purifiedEmailActions[action.emailIndex].splice(action.responseId, 1);
      return {
        ...state,
        emailSummaries: purifiedEmailSummaries,
        emailActions: purifiedEmailActions
      };
    case LOAD_RESPONSES:
      // if the data was already loaded, then skip
      // this can get called multiple times
      if (state.isTestLoadedFromDB) {
        return state;
      }
      const responses = action.responses.test_responses;
      let assignedQuestionIds = [];
      let idx = 0;
      let loadingEmailActions = state.emailActions;
      let loadingEmailSummaries = state.emailSummaries;
      // itterate over the questions
      for (let key in responses) {
        assignedQuestionIds.push(parseInt(key, 10));
        let response = responses[key];
        loadingEmailActions[idx] = [];
        // itterate over the responses for a given question
        for (let inner_id in response) {
          let candidate_response = response[inner_id];
          let true_response = {};
          true_response["actionTrueId"] = candidate_response.candidate_response_id;
          // If there are responses, then the email was read
          loadingEmailSummaries[idx].isRead = true;
          if (candidate_response.response_type === "email") {
            //increment email count
            loadingEmailSummaries[idx].emailCount += 1;
            true_response["actionType"] = "email";
            true_response["emailBody"] = candidate_response.response;
            // TODO DO WE NEED TO STORE AND RETRIEVE THE TYPE OF EMAIL...?
            // It's pretty much cosmetic...
            true_response["emailType"] = "reply";
            true_response["emailTo"] = stringListToArray(candidate_response.to);
            true_response["emailCc"] = stringListToArray(candidate_response.cc);
            // TODO these may be artifacts that are no longer used... hard to say
            true_response["emailToSelectedValues"] = [];
            true_response["emailCcSelectedValues"] = [];
          }
          if (candidate_response.response_type === "task") {
            //increment task count
            loadingEmailSummaries[idx].taskCount += 1;
            true_response["actionType"] = "task";
            true_response["task"] = candidate_response.task;
          }
          true_response["reasonsForAction"] = candidate_response.reason;
          loadingEmailActions[idx].push(true_response);
        }
        idx += 1;
      }
      return {
        ...state,
        assignedQuestionIds: assignedQuestionIds,
        emailActions: loadingEmailActions,
        isTestLoadedFromDB: true,
        emailSummaries: loadingEmailSummaries
      };
    case CHANGE_CURRENT_EMAIL:
      return {
        ...state,
        currentEmail: action.emailIndex
      };
    case SET_ASSIGNED_QUESTION_IDS:
      if (
        action.assignedQuestionIds === undefined ||
        action.assignedQuestionIds["assigned_question_ids"] === undefined
      ) {
        // if there are no assigned questionids, then this is an unassigned/sample test
        return state;
      }
      return {
        ...state,
        assignedQuestionIds: action.assignedQuestionIds["assigned_question_ids"]
      };
    case SET_RESPONSE_DB_ID:
      let alteredEmailActions = Array.from(state.emailActions);
      alteredEmailActions[action.emailIndex][action.responseId]["actionTrueId"] = action.trueId;
      return {
        ...state,
        emailActions: alteredEmailActions
      };
    case RESET_STATE:
      return {
        ...initialState
      };

    default:
      return state;
  }
};

// Selector functions
const selectEmailActions = (actionState, emailId) => {
  return actionState[emailId];
};

// convert comma seperated string to array of numbers
const stringListToArray = value => {
  // if empty, then return an empty list
  if (value === "") {
    return [];
  }
  let retArr = [];
  let vals = value.split(",");
  for (let idx = 0; idx < vals.length; idx++) {
    retArr.push(parseInt(vals[idx], 10));
  }
  return retArr;
};

export default emibInbox;
export {
  initialState,
  readEmail,
  addEmail,
  addTask,
  updateEmail,
  updateTask,
  deleteEmail,
  deleteTask,
  loadResponses,
  selectEmailActions,
  changeCurrentEmail,
  updateEmailsEnState,
  updateEmailsFrState,
  updateEmailsState,
  setAssignedQuestionIds,
  setResponseDbId,
  resetInboxState
};
