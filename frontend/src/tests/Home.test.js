import React from "react";
import { shallow } from "enzyme";
import { UnconnectedHome as Home } from "../Home";
import LOCALIZE from "../text_resources";

it("renders home page title if not authenticated", () => {
  const wrapper = shallow(<Home authenticated={false} />, { disableLifecycleMethods: true });
  const homePageTitle = <h1>{LOCALIZE.homePage.welcomeMsg}</h1>;
  expect(wrapper.containsMatchingElement(homePageTitle)).toEqual(true);
});
