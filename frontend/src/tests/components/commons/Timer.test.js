import React from "react";
import { mount } from "enzyme";
import { UnconnectedTimer } from "../../../components/commons/Timer";

it("defaults to showing the time", () => {
  const wrapper = mount(<UnconnectedTimer timeout={() => {}} timeRemaining={() => {}} />);
  expect(wrapper.find("#unit-test-time-label").exists()).toEqual(true);
});

it("hides the time on click", () => {
  const wrapper = mount(<UnconnectedTimer timeout={() => {}} timeRemaining={() => {}} />);
  wrapper
    .find("#unit-test-toggle-timer")
    .first()
    .simulate("click");
  expect(wrapper.find("#unit-test-time-label").exists()).toEqual(false);
});
