import React from "react";
import { shallow } from "enzyme";
import TestFooter from "../../../components/commons/TestFooter";

describe("renders component content", () => {
  const submitMock1 = jest.fn();
  it("renders the submit button", () => {
    const wrapper = shallow(<TestFooter timeout={() => {}} submitTest={submitMock1} />);
    expect(wrapper.find("#unit-test-submit-btn").exists()).toEqual(true);
  });

  it("timer is displayed", () => {
    const wrapper = shallow(<TestFooter timeout={() => {}} submitTest={submitMock1} />);
    expect(wrapper.find("#unit-test-timer").exists()).toEqual(true);
  });
});
