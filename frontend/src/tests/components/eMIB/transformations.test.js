import { contactNameFromId, recursivelyProcessTree } from "../../../helpers/transformations";

const addressBook = [
  { label: "Joe (Developer)", value: 0 },
  { label: "Bob (Developer)", value: 1 },
  { label: "Smithers (Butler)", value: 2 },
  { label: "Arthur (King of Britain)", value: 3 },
  { label: "Richard (Lionheart)", value: 4 },
  { label: "Robert (The Bruce)", value: 5 }
];

describe("Check contactNameFromId", () => {
  it("returns the name when id is in the address book", () => {
    expect(contactNameFromId(addressBook, 3)).toEqual("Arthur (King of Britain)");
  });

  it("returns an empty string when the id is not in the address book", () => {
    expect(contactNameFromId(addressBook, 6)).toEqual("");
  });
});

describe("recursivelyProcessTree", () => {
  it("returns a processed array for single level tree", () => {
    const treeContent = [
      {
        id: 0,
        text: "Claude Huard (Quality Assurance Manager - You)",
        team_information_tree_child: []
      }
    ];
    const array = recursivelyProcessTree(treeContent, "team_information_tree_child", 1, 0);
    expect(array).toEqual([
      {
        id: 0,
        level: 1,
        name: "Claude Huard (Quality Assurance Manager - You)",
        groups: [],
        parent: undefined
      }
    ]);
  });

  it("returns a processed array for double level tree", () => {
    const treeContent = [
      {
        id: 0,
        text: "Claude Huard (Quality Assurance Manager - You)",
        team_information_tree_child: [
          {
            text: "Danny McBride (QA Analyst)",
            id: 0
          },
          {
            text: "Serge Duplessis (QA Analyst)",
            id: 1
          }
        ]
      }
    ];
    const array = recursivelyProcessTree(treeContent, "team_information_tree_child", 1, 0);
    expect(array).toEqual([
      { id: 0, level: 1, name: "Claude Huard (Quality Assurance Manager - You)", groups: [1, 2] },
      { id: 1, level: 2, name: "Danny McBride (QA Analyst)", parent: 0 },
      { id: 2, level: 2, name: "Serge Duplessis (QA Analyst)", parent: 0 }
    ]);
  });
});
