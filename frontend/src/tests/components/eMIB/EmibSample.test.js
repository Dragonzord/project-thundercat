import React from "react";
import { shallow } from "enzyme";
import EmibSample from "../../../components/eMIB/EmibSample";

it("renders the sample emib test", () => {
  const wrapper = shallow(<EmibSample updateBackendTest={() => {}} />);
  expect(wrapper.find("#unit-test-sample-test").exists()).toEqual(true);
});
