import React from "react";
import { shallow } from "enzyme";
import { unconnectedTestAccesses as TestAccesses } from "../../../components/etta/test_accesses/TestAccesses";
import LOCALIZE from "../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(<TestAccesses />, {
    disableLifecycleMethods: true
  });

  it("renders description", () => {
    const description = <p>{LOCALIZE.systemAdministrator.testAccesses.description}</p>;
    expect(wrapper.containsMatchingElement(description)).toEqual(true);
  });
});
