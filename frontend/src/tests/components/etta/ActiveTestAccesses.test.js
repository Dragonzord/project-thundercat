import React from "react";
import { shallow } from "enzyme";
import { unconnectedActiveTestAccesses as ActiveTestAccesses } from "../../../components/etta/test_accesses/ActiveTestAccesses";
import { LANGUAGES } from "../../../modules/LocalizeRedux";

const mockData = [
  {
    first_name: "John",
    last_name: "Smith",
    en_test_name: "Pizza Test",
    fr_test_name: "FR Pizza Test",
    test_order_number: "2020/123456",
    expiry_date: "2021/01/01",
    username: "john.smith@canada.ca",
    staffing_process_number: "ABC123DEF654",
    department_ministry_code: "1040",
    billing_contact: "Billing Contact One",
    billing_contact_info: "billing.contact.one@canada.ca",
    is_org: "ORG_ONE",
    is_ref: "REF_ONE"
  },
  {
    first_name: "John",
    last_name: "Doe",
    en_test_name: "Sample Test",
    fr_test_name: "FR Pizza Test",
    test_order_number: "2020/987654",
    expiry_date: "2021/06/06",
    username: "john.doe@canada.ca",
    staffing_process_number: "HIJ654KLM789",
    department_ministry_code: "1060",
    billing_contact: "Billing Contact Two",
    billing_contact_info: "billing.contact.two@canada.ca",
    is_org: "ORG_TWO",
    is_ref: "REF_TWO"
  }
];

describe("renders component content", () => {
  const wrapper = shallow(<ActiveTestAccesses />, {
    disableLifecycleMethods: true
  });
  wrapper.setState({ activeTestPermissions: mockData });
  wrapper.setProps({ currentLanguage: LANGUAGES.english });

  it("renders row content based on data", () => {
    // row 1
    const name1 = <td id="test-administrator-label-0">Smith, John</td>;
    expect(wrapper.containsMatchingElement(name1)).toEqual(true);
    const test1 = <td id="test-label-0">Pizza Test</td>;
    expect(wrapper.containsMatchingElement(test1)).toEqual(true);
    const testOrderNumber1 = <td id="test-order-number-label-0">2020/123456</td>;
    expect(wrapper.containsMatchingElement(testOrderNumber1)).toEqual(true);
    const expiryDate1 = <td id="expiry-date-label-0">2021/01/01</td>;
    expect(wrapper.containsMatchingElement(expiryDate1)).toEqual(true);
    // row 1
    const name2 = <td id="test-administrator-label-1">Doe, John</td>;
    expect(wrapper.containsMatchingElement(name2)).toEqual(true);
    const test2 = <td id="test-label-1">Sample Test</td>;
    expect(wrapper.containsMatchingElement(test2)).toEqual(true);
    const testOrderNumber2 = <td id="test-order-number-label-1">2020/987654</td>;
    expect(wrapper.containsMatchingElement(testOrderNumber2)).toEqual(true);
    const expiryDate2 = <td id="expiry-date-label-1">2021/06/06</td>;
    expect(wrapper.containsMatchingElement(expiryDate2)).toEqual(true);
  });
});
