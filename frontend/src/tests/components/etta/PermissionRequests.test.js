import React from "react";
import { shallow } from "enzyme";
import { unconnectedPermissionRequests as PermissionRequests } from "../../../components/etta/permissions/PermissionRequests";

const mockData = [
  {
    permission_requested: "Test Administrator",
    first_name: "John",
    last_name: "Smith",
    goc_email: "john.smith@canada.ca",
    pri_or_military_nbr: "A123456",
    rationale: "rationale",
    supervisor: "John Doe",
    supervisor_email: "john.doe@canada.ca"
  },
  {
    permission_requested: "System Administrator",
    first_name: "John",
    last_name: "Doe",
    goc_email: "john.doe@canada.ca",
    pri_or_military_nbr: "B123456",
    rationale: "rationale",
    supervisor: "John Smith",
    supervisor_email: "john.smith@canada.ca"
  }
];

describe("renders component content", () => {
  const wrapper = shallow(
    <PermissionRequests
      pendingPermissions={mockData}
      populatePendingPermissions={() => {}}
      populateActivePermissions={() => {}}
    />,
    {
      disableLifecycleMethods: true
    }
  );

  it("renders row content based on data", () => {
    const row1 = <label>Test Administrator [Smith, John]</label>;
    expect(wrapper.containsMatchingElement(row1)).toEqual(true);
    const row2 = <label>System Administrator [Doe, John]</label>;
    expect(wrapper.containsMatchingElement(row2)).toEqual(true);
    const row3 = <label>Test Administrator [Doe, John]</label>;
    expect(wrapper.containsMatchingElement(row3)).toEqual(false);
    const row4 = <label>System Administrator [Smith, John]</label>;
    expect(wrapper.containsMatchingElement(row4)).toEqual(false);
  });
});
