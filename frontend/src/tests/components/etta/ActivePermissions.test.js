import React from "react";
import { shallow } from "enzyme";
import { unconnectedActivePermissions as ActivePermissions } from "../../../components/etta/permissions/ActivePermissions";

const mockData = [
  {
    username: "john.smith@canada.ca",
    user_permission_id: "1",
    permission: "Test Administrator",
    first_name: "John",
    last_name: "Smith",
    permission_id: "1",
    goc_email: "jonh,smith@canada.ca",
    pri_or_military_nbr: "12345679",
    supervisor: "Supervisor Name",
    supervisor_email: "supervisor.name@canada.da"
  },
  {
    username: "john.doe@canada.ca",
    user_permission_id: "3",
    permission: "System Administrator",
    first_name: "John",
    last_name: "Doe",
    permission_id: "3",
    goc_email: "jonh,doe@canada.ca",
    pri_or_military_nbr: "A123456",
    supervisor: "Supervisor Name",
    supervisor_email: "supervisor.name@canada.da"
  }
];

describe("renders component content", () => {
  const wrapper = shallow(
    <ActivePermissions
      activePermissions={mockData}
      populateActivePermissions={() => {}}
      populateFoundActivePermissions={() => {}}
      numberOfPages={1}
      resultsFound={0}
    />,
    {
      disableLifecycleMethods: true
    }
  );
  wrapper.setState({ activePermissions: mockData });

  it("renders row content based on data", () => {
    // row 1
    const permission1 = <label>Test Administrator</label>;
    expect(wrapper.containsMatchingElement(permission1)).toEqual(true);
    const name1 = <label>Smith, John</label>;
    expect(wrapper.containsMatchingElement(name1)).toEqual(true);
    // row 2
    const permission2 = <label>System Administrator</label>;
    expect(wrapper.containsMatchingElement(permission2)).toEqual(true);
    const name2 = <label>Doe, John</label>;
    expect(wrapper.containsMatchingElement(name2)).toEqual(true);
  });
});
