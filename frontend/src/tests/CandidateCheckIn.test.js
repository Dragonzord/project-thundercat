import React from "react";
import { mount } from "enzyme";
import { UnconnectedCandidateCheckIn } from "../CandidateCheckIn";
import LOCALIZE from "../text_resources";
import { Button } from "react-bootstrap";
import styles from "../TestAdministration";

describe("renders right wording depending on the checkedIn state", () => {
  it("checkedIn is set to true)", () => {
    const wrapper = mount(<UnconnectedCandidateCheckIn isCheckedIn={true} />);
    wrapper.setState({ testAccessCode: "test" });
    const rawText = LOCALIZE.candidateCheckIn.checkedInText;
    expect(wrapper.contains(rawText)).toEqual(true);
  });

  it("checkedIn room code is displayed)", () => {
    const wrapper = mount(<UnconnectedCandidateCheckIn isCheckedIn={true} />);
    const testAccessCode = "test";
    wrapper.setState({ testAccessCode: testAccessCode });
    expect(wrapper.contains(testAccessCode)).toEqual(true);
  });
});

describe("renders right objects depending on the checkedIn state", () => {
  it("when checkedIn is set to false", () => {
    const wrapper = mount(<UnconnectedCandidateCheckIn isCheckedIn={false} isLoaded={true} />);
    wrapper.setState({ testAccessCode: "test" });
    const button = LOCALIZE.candidateCheckIn.button;

    expect(wrapper.contains(button)).toEqual(true);
  });

  it("when checkedIn is set to false and is not loaded", () => {
    const wrapper = mount(<UnconnectedCandidateCheckIn isCheckedIn={false} isLoaded={false} />);
    wrapper.setState({ testAccessCode: "test" });
    const button = LOCALIZE.candidateCheckIn.loading;

    expect(wrapper.contains(button)).toEqual(true);
  });
});
